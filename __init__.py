VERSION = '0.1'
TITLE = 'mmorpg'
TIER = 5
GOD_MODE = True
MAX_LEVEL = 50

import sys; HOST = 'localhost' if len(sys.argv)<2 else sys.argv[1]
PORT = 8007

BASE_FPS = 30.
FPS = 60.
UPDATE_FPS = 10. + TIER

WRITE_FREQUENCY = 60

ORIG_TILE_SIZE = 16
ORIG_TILE_BUFFER = 2
TILE_SIZE = 64
BLOCK_SIZE = 64
EMU_X, EMU_Y = (1024-64*2, 768-64*2)
SCREEN_X, SCREEN_Y = (EMU_X/TILE_SIZE,EMU_Y/TILE_SIZE)
SCALE = float(TILE_SIZE) / (ORIG_TILE_SIZE)
SCROLL_SPEED = SCALE*4
BUFFER = ORIG_TILE_BUFFER * SCALE
BUFFERED_TILE = TILE_SIZE + BUFFER
TILE_SCROLL = SCALE / 16.0
ANTIALIAS = True

FULLSCREEN = False

BASE_MOVE_SPEED = 0.075

move_directions = {'u': (0, -1),
                   'd': (0, 1),
                   'l': (-1, 0),
                   'r': (1, 0),}
                   
WALK_THRESHOLD = 300
REGEN_THRESHOLD = 250
POISON_THRESHOLD = 2
POISON_MOVE_PENALTY = 5
ATTACK_THRESHOLD = 6
ATTACK_ANIMATION_LENGTH = 1
ATTACK_ANIMATION_SPEED = 1./ATTACK_ANIMATION_LENGTH/BASE_FPS
INVERT_LENGTH = 0.25*BASE_FPS
RUN_THRESHOLD = 4
FLY_THRESHOLD = 3
SWIM_THRESHOLD = 3
TALK_DISTANCE = 1
USE_SKILL_THRESHOLD = 40

MAX_PENALTY = 1000
VISION = 8

MAX_HISTORY_LENGTH = 10
MSG_BOX_WIDTH = 9.5
MSG_BOX_HEIGHT = 2

COLORS_RED = (255,0,0)
COLORS_FADED_RED = (128, 32, 32)
COLORS_YELLOW = (255,255,0)
COLORS_FADED_YELLOW = (128,128,32)
COLORS_GREEN = (0,128,0)
COLORS_BRIGHT_GREEN = (0,255,0)
COLORS_FADED_GREEN = (32, 64, 32)
COLORS_BLUE = (0,0,255)
COLORS_WHITE = (255,255,255)
COLORS_GREY = COLORS_GRAY = (128,128,128)
COLORS_BLACK = (0,0,0)
COLORS_OFFBLACK = (24,24,24)

MARK_LENGTH = 600
RETHINK_TIME = 6
DAMAGE_NUMBER_DURATION = 1

MSG_NEW = '0'
MSG_CHAR = '1'
MSG_UPDATE = '2'
MSG_OBJS = '3'
MSG_ACT = '4'
MSG_WORLD = '5'
MSG_END = '\den\d'

MAX_MSG_LENGTH = 500000

HP_B = 0
HP_M = 5
HP_MIN = 10
STAM_B = 0
STAM_M = 2
STAM_MIN = 4
MAX_STATS = {
             'Swimming':50, 'Flying':50, 'Running':50, 'Regeneration':50,
             'Flurry':50, 'Quickcast':50,
             'Vitality':500, 'Stamina':200,
             }
if 'Vitality' in MAX_STATS: MAX_STATS['Toughness'] = (MAX_STATS['Vitality']-HP_B)/HP_M
if 'Endurance' in MAX_STATS: MAX_STATS['Endurance'] = (MAX_STATS['Stamina']-STAM_B)/STAM_M

MONO_FONT = 'assets/pixelated.ttf'#'assets/UbuntuMono-B.ttf'

HELP_MESSAGE = '''<b>Controls:</b><br>
WASD, arrows: move<br>
shift+move: run<br>
x: cancel attack<br>
space: show/hide message box<br>
tab/shift+tab: cycle through targets<br>
delete: attack target<br>
click: talk<br>
right click: attack<br>
ctrl+right click: attack friendly target<br>
f: fly
'''
