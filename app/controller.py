from mmorpg import *
from mmorpg.lib.models import *
#from mmorpg.app.controls import *
from mmorpg.app.map_window import MapWindow
from __init__ import PYGAME_FLAGS
import os
import pygame
import pygame.locals
import pygame.constants as pc
import pygame.mixer as mixer
import sys
import time


directions = {pc.K_UP: 'u',
              pc.K_DOWN: 'd',
              pc.K_LEFT: 'l',
              pc.K_RIGHT: 'r',
              ord('w'): 'u',
              ord('s'): 'd',
              ord('a'): 'l',
              ord('d'): 'r',
              ord('W'): 'u',
              ord('S'): 'd',
              ord('A'): 'l',
              ord('D'): 'r',
              }


class Controller:
    def __init__(self, settings):
        self.settings = settings
        
    def start(self):
        self.event_updates = self.draw_updates = 0
        self.done = False
        
        self.last_time = None
        
        self.buttons = {}
        
        self.f_pressed = False
        self.ins_pressed = False
        self.lctrl_pressed = False
        self.rctrl_pressed = False
        self.moving = ''
        self.last_moving_sent = ''
        
        pygame.display.init()
        #self.screen = pygame.display.set_mode(*PYGAME_FLAGS)
        #pygame.display.set_caption(TITLE)
        self.screen.set_alpha(None)
        self.width, self.height = self.screen.get_size()
        #pygame.event.set_allowed([pygame.MOUSEBUTTONDOWN, pygame.MOUSEBUTTONUP, pygame.KEYDOWN, pygame.KEYUP,
        #                          pygame.JOYAXISMOTION, pygame.JOYBUTTONDOWN, pygame.JOYBUTTONUP])
        pygame.key.set_repeat()
        
        pygame.joystick.init()
        try:
	        self.joystick = pygame.joystick.Joystick(0)
	        self.joystick.init()
        except pygame.error:
	        self.joystick = None
        
        self.draw_last_time = time.time()
        
        self.msg_lines = []
        self.msg_history = ""
        self.msg_scroll = 0
        self.message("<b>Welcome, %s</b>!<br>" % self.character.name)
        
        self.talk_shown = False
        self.talk_msg = ''
        
        self.ability_shown = True
        self.ability_scroll = 0

        self.skills_shown = False
        self.selected_skill = None
        self.last_upgrade = time.time()
        
        self.items_shown = False
        self.selected_item = None
        self.selitem_scroll = 0
        
        self.shop_shown = False
        self.selected_buy_item = None
        self.selected_sell_item = None
        
        self.map = MapWindow(self)
        self.map.load_tilesets(self.tilesets)
        
        self.pressing = None
        
        self.music = None
        

    def update(self):
        if self.done: return
        character = self.character
        if hasattr(character, 'music') and self.music != character.music:
            new_music = character.music
            if new_music:
                mixer.music.stop()#fadeout(2000)
                mixer.music.load('music/%s.ogg' % new_music)
                mixer.music.play(-1)
            self.music = new_music
        fps = 1. / (time.time() - self.draw_last_time)
        self.draw_updates += 1. / fps
        self.draw_fps = fps
        self.draw_last_time = time.time()

        if hasattr(self, 'map'):
            self.map.scroll -= TILE_SCROLL * (BASE_FPS / fps)
            if self.map.scroll <= 0:
                self.map.scroll += TILE_SIZE

        # check pygame for events
        for event in pygame.event.get():
            if event.type in (pygame.KEYDOWN, pygame.KEYUP):
                shift = event.mod&pygame.KMOD_SHIFT
                ctrl = event.mod&pygame.KMOD_CTRL
                alt = event.mod&pygame.KMOD_META
                if event.type == pygame.KEYDOWN:
                    try: char = event.unicode
                    except: char = ''
                    self.on_key_down(event.key, char, shift, ctrl, alt)
                elif event.type == pygame.KEYUP:
                    try: char = event.unicode
                    except: char = ''
                    self.on_key_up(event.key, char, shift, ctrl, alt)
            elif event.type == pygame.MOUSEBUTTONDOWN:
                self.on_click(event.pos, event.button)
            elif event.type == pygame.MOUSEBUTTONUP:
                self.pressing = None
                '''elif event.type == pygame.JOYAXISMOTION:
                if event.axis == 0:
                    if event.value == 0:
                        if self.moving:
                            self.moving = self.moving.replace('r','').replace('l','')
                    elif event.value > 0:
                        if not self.moving: self.moving = ''
                        self.moving += 'r'
                    elif event.value < 0:
                        if not self.moving: self.moving = ''
                        self.moving += 'l'
                elif event.axis == 1:
                    if event.value == 0:
                        if self.moving:
                            self.moving = self.moving.replace('d','').replace('u','')
                    elif event.value > 0:
                        if not self.moving: self.moving = ''
                        self.moving += 'd'
                    elif event.value < 0:
                        if not self.moving: self.moving = ''
                        self.moving += 'u'''
            elif event.type == pygame.JOYHATMOTION:
                dx, dy = event.value
                self.moving = ('r' if dx > 0 else ('l' if dx < 0 else '')) + ('u' if dy > 0 else ('d' if dy < 0 else ''))

            elif event.type == pygame.JOYBUTTONDOWN:
                if event.button in self.settings.joy_btns:
                    self.on_key_down(*self.settings.joy_btns[event.button])
            elif event.type == pygame.JOYBUTTONUP:
                if event.button in self.settings.joy_btns:
                    self.on_key_up(*self.settings.joy_btns[event.button])
                        
        self.map.draw(self.character)
        
        '''if self.draw_updates >= 5 or character.gui_update:
            self.main_window.Update()
            self.draw_updates = 0
            self.character.gui_update = False
            self.character.update_vitals = False
        elif character.update_vitals:
            self.main_window.update_vitals()
            self.character.update_vitals = False
            
        messages = self.character.read_messages()        
        if messages:
            self.main_window.Message(messages)'''
            
        if self.moving != self.last_moving_sent:
            self.do_to_character('set_moving', self.moving)
            self.last_moving_sent = self.moving
            
        if self.pressing:
            self.button_press(*self.pressing)
            
        pygame.display.flip()
        
        
    def on_key_down(self, key, char, shift=False, ctrl=False, alt=False):
        character = self.character
        
        new_move_dir = None
        
        if key <= 128 and self.talk_shown and not ctrl and not alt:
            if key in (pc.K_BACKSPACE, pc.K_DELETE):
                self.talk_msg = self.talk_msg[:-1]
            elif key == pc.K_RETURN:
                msg = self.talk_msg.strip()
                if msg:
                    if msg in ('!quit', '!q'):
                        self.quit()
                    elif GOD_MODE and msg in ('!god'):
                        self.do_to_character('god_mode')
                    elif GOD_MODE and msg[:2] == '!!':
                        me, do, set = self.character, self.do_to_character, self.set
                        try: exec(msg[2:])
                        except Exception as e: self.message(str(e))
                    elif GOD_MODE and msg[:5] == '!eval':
                        me, do, set = self.character, self.do_to_character, self.set
                        try: self.message(str(eval(msg[5:])))
                        except Exception as e: self.message(str(e))
                    else:
                        self.do_to_world('broadcast', self.character.uid, self.talk_msg)
                self.talk_msg = ''
                self.talk_shown = False
            elif char:
                self.talk_msg += str(char)
            return
            
        if key == pc.K_RETURN:
            self.talk_shown = True
        elif key == pc.K_F1:
            self.message(HELP_MESSAGE)
        elif key == pc.K_F5:
            self.map.screenshot = True
        elif key == pc.K_F10:
            pygame.display.toggle_fullscreen()
        elif key in directions.keys():
            # direction key; start moving character
            if not self.moving:
                new_move_dir = directions[key]
            else:
                if len(self.moving) == 1 and not directions[key] in self.moving:
                    new_move_dir = directions[key] + self.moving
        elif key in (pc.K_ESCAPE,):
            self.quit()
        elif key in (ord('E'), ord('e')):
            self.items_shown = not self.items_shown
            if self.items_shown: self.skills_shown = False
        elif key in (ord('R'), ord('r')):
            self.skills_shown = not self.skills_shown
            if self.skills_shown: self.items_shown = False
        elif key in (ord('X'), ord('x'), ord('Z'), ord('z')):
            if self.items_shown or self.skills_shown:
                self.items_shown = False
                self.skills_shown = False
            else:
                new_move_dir = False
                self.do_to_character('stop_actions')
        elif key in (pc.K_LSHIFT, pc.K_RSHIFT):
            # start character running
            self.do_to_character('run')
        elif key == pc.K_DELETE:
            looking_at = self.character.looking_at
            if looking_at and not looking_at == character.uid:
                self.do_to_character('attack', looking_at, False, ctrl)
        elif key in (pc.K_SPACE, pc.K_INSERT):
            self.toggle_msg_box()
        elif key == pc.K_TAB:
            self.character.next_selection(shift)
            self.do_to_character('look', self.character.looking_at)
        elif pc.K_0 <= key <= pc.K_9:
            n = key - 49 if key > pc.K_0 else 9
            if self.character.choices:
                self.do_to_character('make_choice', self.character.choices[n])
            else:
                abilities = self.character.sorted_abilities()
                if len(abilities) > n:
                    self.do_to_character('set_attacking_with', abilities[n][1])
        elif key in (ord('F'), ord('f')):
            if not self.f_pressed:
                if character.flying:
                    self.do_to_character('fly', False)
                else:
                    self.do_to_character('fly')
                self.f_pressed = True
        elif key == pc.K_LCTRL:
            self.lctrl_pressed = True
        elif key == pc.K_RCTRL:
            self.rctrl_pressed = True
        
        if not new_move_dir is None:
            self.moving = new_move_dir
        
                    
    def on_key_up(self, key, char, shift=False, ctrl=False, alt=False):
        character = self.character
        
        rm_move_dir = None

        if key in directions.keys():
            # stop moving in a particular direction
            try:
                rm_move_dir = directions[key]
            except Exception as e: print e; raise; pass
        elif key in (pc.K_LSHIFT, pc.K_RSHIFT):
            # stop running
            self.do_to_character('run', False)
        elif key == pc.K_INSERT:
            self.ins_pressed = False
        elif key in (ord('F'), ord('f')):
            self.f_pressed = False
        elif key == pc.K_LCTRL:
            self.lctrl_pressed = False
        elif key == pc.K_RCTRL:
            self.rctrl_pressed = False
        
        if not rm_move_dir is None and self.moving:
            self.moving = self.moving.replace(rm_move_dir, '')
            self.do_to_character('set_moving', rm_move_dir, True)
        
        
    def on_click(self, pos, btn):
        ctrl = self.lctrl_pressed or self.rctrl_pressed

        character = self.character
        characters = character.peers.values()
        
        x, y = pos
        x, y = (int(float(x)), 
                int(float(y)))
        
        for button_name, coords in sorted(self.buttons.items()):
            if not isinstance(coords, list):
                coords = [(None, coords, button_name)]
            else:
                coords = [this_btn + (button_name,) for this_btn in coords]
            for subname, coords, button in coords:
                if coords:
                    bx, by, width, height = coords
                    if bx <= x <= bx+width and by <= y <= by+height:
                        self.pressing = (button, btn, subname)
                        return
        
        if any((self.items_shown, self.skills_shown, self.shop_shown)): return
        
        # dictionary of distance of each character from click
        distances = {}
        for c in characters:
            notarget = btn == 3 and not ctrl and (not hasattr(c, 'hostile') or not c.hostile)
            if not hasattr(c, 'dead') or not c.dead and not notarget:
                px, py = onscreen(c, character, self.map.SCREEN_SIZE)
                px += self.map.global_offset_x + (TILE_SIZE/2)
                py += self.map.global_offset_y + (TILE_SIZE/2)

                #pygame.draw.circle(self.screen, COLORS_BLACK, (int(px), int(py)), int(TILE_SIZE/2))
                #pygame.display.flip()
                            
                distances[c] = abs(px-x) + abs(py-y)
            
        sorted_distances = sorted(distances.items(),
                                  key=lambda x: x[1])
                                  
        if sorted_distances:
            closest_char, distance = sorted_distances[0]
            max_distance = TILE_SIZE * 2
            if distance <= max_distance:
                if btn == 1:
                    # left click
                    self.look_at(closest_char.uid, True)
                    
                elif btn == 3:
                    # right click
                    if closest_char != character:
                        self.look_at(closest_char.uid)
                        self.do_to_character('attack', closest_char.uid, False, ctrl)
            else:
                # right click on nothing
                if btn == 3:
                    self.do_to_character('attack', False)
        else:
            # right click on nothing
            if btn == 3:
                self.do_to_character('attack', False)            
                
    def button_press(self, name, mouse_btn, subname=None):
        if not name in self.buttons or self.buttons[name] is None: return
    
        if name == 'msg_uparrow':
            self.msg_scroll += SCROLL_SPEED
        elif name == 'msg_downarrow':
            self.msg_scroll = max(self.msg_scroll - SCROLL_SPEED, 0)
        elif name in ('msg_close', 'msg_show'):
            self.toggle_msg_box()
        elif name == 'skills_close':
            self.skills_shown = False
            self.pressing = None
        elif name == 'skills_show':
            self.skills_shown = True
            self.pressing = None
        elif name == 'skill':
            self.selected_skill = subname
            self.pressing = None
        elif name == 'upgrade':
            self.do_to_character('upgrade', subname)
            self.pressing = None
        elif name == 'item':
            self.selected_item = subname
            self.selitem_scroll = 0
            self.pressing = None
        elif name == 'equip':
            self.do_to_character('equip', subname)
            self.pressing = None
        elif name == 'unequip':
            self.do_to_character('unequip', subname)
            self.pressing = None
        elif name == 'use':
            self.do_to_character('use_item', subname)
            self.pressing = None
        elif name == 'buyitem':
            self.selected_buy_item = subname
            self.selitem_scroll = 0
            self.pressing = None
        elif name == 'sellitem':
            self.selected_sell_item = subname
            self.pressing = None
        elif name == 'buy':
            self.do_to_character('buy', self.selected_buy_item)
            self.pressing = None
        elif name == 'sell':
            self.do_to_character('sell', self.selected_sell_item)
            self.pressing = None
        elif name.startswith('0scroll_up_'):
            subname = name[len('0scroll_up_'):]
            self.map.listboxes[subname].scroll = max(0, self.map.listboxes[subname].scroll - SCROLL_SPEED)
        elif name.startswith('0scroll_down_'):
            subname = name[len('0scroll_down_'):]
            self.map.listboxes[subname].scroll += SCROLL_SPEED
        elif name == 'selitem_uparrow':
            self.selitem_scroll = max(0, self.selitem_scroll - SCROLL_SPEED)
        elif name == 'selitem_downarrow':
            self.selitem_scroll += SCROLL_SPEED
        elif name == 'items_close':
            self.items_shown = False
            self.pressing = None
        elif name == 'items_show':
            self.items_shown = True
            self.pressing = None
        elif name == 'items_uparrow':
            self.items_scroll = max(self.items_scroll - SCROLL_SPEED, 0)
        elif name == 'items_downarrow':
            self.items_scroll += SCROLL_SPEED
        elif name == 'ability':
            self.do_to_character('set_attacking_with', subname)
            self.pressing = None
        elif name == 'choice':
            self.do_to_character('make_choice', subname)
            self.pressing = None

    '''def JoyMove(self, event):
        x,y = event.GetPosition()
        move_dir = ''
        
        if x: move_dir += 'r' if x > 0 else 'l'
        if y: move_dir += 'd' if y > 0 else 'u'
        
        self.moving = move_dir


    def JoyDown(self, event):
        btn = event.GetButtonChange()
        if btn in joy_btns:
            sim_event = wx.KeyEvent(wx.wxEVT_KEY_DOWN)
            sim_event.m_keyCode = joy_btns[btn]
            self.OnKeyDown(sim_event)
        elif btn == 4:
            self.do_to_character('next_selection', False)
        elif btn == 5:
            self.do_to_character('next_selection', True)
        

    def JoyUp(self, event):
        btn = event.GetButtonChange()
        if btn in joy_btns:
            sim_event = wx.KeyEvent(wx.wxEVT_KEY_UP)
            sim_event.m_keyCode = joy_btns[btn]
            self.OnKeyUp(sim_event)'''


    def do_to_character(self, *args):
        self.client.do(*args)
    def set(self, *args):
        self.do_to_character('setattr', *args)
    def do_to_world(self, *args):
        self.client.execute(*args)
        
        
    def upgrade(self, skill_name):
        if time.time() - self.last_upgrade >= 1:
            self.do_to_character('upgrade', skill_name)
            self.last_upgrade = time.time()
        
        
    def message(self, msgs):
        if not isinstance(msgs, list):
            msgs = [msgs]
        msgs.reverse()
        self.msg_lines += msgs
        self.msg_lines = self.msg_lines[-MAX_HISTORY_LENGTH:]
        self.msg_history = '<br>'.join(self.msg_lines)
        
    def shorten_msg_history(self):
        self.msg_lines = self.msg_lines[1:]
        self.msg_history = '<br>'.join(self.msg_lines)
        
        #self.msg_scroll = 0
        
    '''def update_vitals(self):
        self.character_label.Update()
        
    def toggle_sidebar(self):
        width, height = wx.GetDisplaySize()
        height = int(height * GAME_WINDOW_HEIGHT)
        if self.side_panel_visible:
            width = int(width)
            self.splitter.Detach(self.right_panel)
            [c.Show(False) for c in self.right_panel.GetChildren()]
            self.game_window.width = width
        else:
            width = int(width * GAME_WINDOW_WIDTH)
            [c.Show(True) for c in self.right_panel.GetChildren()]
            self.splitter.Add(self.right_panel, int((1-GAME_WINDOW_WIDTH)*100), wx.EXPAND | wx.ALL)            
            self.game_window.width = width
            
        self.splitter.Layout()
        self.game_window.screen = pygame.display.set_mode((width,height), PYGAME_FLAGS)
        self.game_window.map.set_size(4 if self.side_panel_visible else 0)
        self.side_panel_visible = not self.side_panel_visible
        self.message_box.history.ScrollToBottom()'''

        
    def look_at(self, uid, talk_to=False):
        self.do_to_character('look', uid, talk_to)
        self.character.looking_at = uid
        
    def toggle_msg_box(self):
        char = self.character
        shown = char.force_msg
        if shown: self.do_to_character('msg_done')
        else: self.do_to_character('msg_prolong', 10)
        
    def quit(self):
        self.do_to_world('logout', self.character.uid)
        self.done = True
