from mmorpg.app.map_window import make_label, render
import pgu.html as html
import pygame
import pygame.mixer as mixer
from mmorpg import *
from mmorpg.lib.models import Character, Settings
from mmorpg.scripts.parse_scripts import defs
from mmorpg.tiles.tiles import draw_char, get_tilesets, CHARSET
from __init__ import PYGAME_FLAGS
races = defs['playable_races']
titles = defs['playable_titles']
import sys
import threading
import time
import md5


class MainMenu:
    def __init__(self):
        pygame.font.init()
        self.font = self.big_font = pygame.font.Font(MONO_FONT, 16)
        pygame.display.init()
        self.screen = pygame.display.set_mode(*PYGAME_FLAGS)
        pygame.display.set_caption(TITLE)
        self.size = self.screen.get_size()
        self.buffer = pygame.Surface(self.size)
        self.done = False
        self.quit = False
        self.created = False
        self.loaded = False
        self.turn_page('main')
        
        self.name = ''
        self.password = 'abc'
        self.email = 'abc'
        self.race = 0
        self.title = 0
        self.titles = titles
        self.var = 0
        self.tutorial = False
        
        chars = 'tiles/chars.png'
        self.last_time = time.time()
        self.step = False
        
        self.invalidated = False
        
        self.logo = pygame.image.load('assets/logo.png').convert()
        
        self.settings = Settings()
        self.tilesets = None


    def load_tilesets(self):
        def f(self):
            self.tilesets = get_tilesets(self.settings)
        thread = threading.Thread(target=f,
                                  args=(self,))
        thread.start()
        #if not self.tilesets: self.tilesets = get_tilesets(self.settings)
        
        
    def run(self):
        sx, sy = self.size
        view = self.buffer
        self.btn_selected = None

        start_time = time.time()
        intro_done = False
        self.load_tilesets()
        while not intro_done:
            elapsed_time = time.time() - start_time

            if elapsed_time < 3:
                view.blit(self.logo, (sx/2-200, sy*0.5), (0,0,800,80))
            elif self.tilesets: intro_done = True

            self.screen.blit(view, (0,0), (0,0)+self.size)
            pygame.display.flip()

        mixer.init()
        mixer.music.load('music/theme.ogg')
        mixer.music.play()
    
        while not self.done:
            # draw screen
            texts = []
            btns = {}
            self.taborder = []
            
            if self.clear:
                self.buffer.fill((0,0,0))
                self.invalidated = False
            
            if self.page == 'main':            
                text = render('render', self.font, "LAST LEGEND", True, COLORS_WHITE)
                texts.append(make_label(text, sx/2, sy*0.15) + (None,))

                for txt, y in (('New Character', 0.55), ('Load Character', 0.6), ('Options', 0.65), ('Quit', 0.75)):
                    text = render('render', self.font, txt, True, COLORS_WHITE if self.btn_selected == txt else COLORS_GREY)
                    texts.append(make_label(text, sx/2, sy*y) + (txt,))
                    
                #view.blit(self.logo, (sx/2-200, sy*0.8), (0,0,800,80))
                
            elif self.page == 'newchar':
                rect = pygame.Rect(TILE_SIZE/2, TILE_SIZE/2, sx*0.3, sy-TILE_SIZE)
                instructions = '''\
Use the keyboard to enter your name, and use the arrow keys or mouse to select race, class, and appearance.
<br><br>
%s
<br><br>
%s''' % (defs['races_dict'][races[self.race]].description, defs['titles_dict'][self.titles[self.title]].description)
                instructions = render('render', html, self.font, rect, instructions, 1, COLORS_GREY)
                self.buffer.blit(instructions, (rect.left, rect.top), (0, 0, rect.width, rect.height))
                
                rect = pygame.Rect(sx*0.7-TILE_SIZE/2, TILE_SIZE/2, sx*0.3, sy-TILE_SIZE)
                description = 'HP: %s, Stamina:%s\n' % (self.char.hp, self.char.stamina) + self.char.skill_summary()
                description = render('render', html, self.font, rect, description, 1, COLORS_GREY)
                self.buffer.blit(description, (rect.left, rect.top), (0, 0, rect.width, rect.height))
            
                text = render('render', self.font, "NEW CHARACTER", True, COLORS_WHITE)
                texts.append(make_label(text, sx/2, sy*0.1) + (None,))
                
                text = render('render', self.font, 'Name: %s%s' % (self.name, '|' if len(self.name) < 10 else ''), True, COLORS_WHITE if self.btn_selected == 'Name' else COLORS_GREY)
                texts.append(make_label(text, sx/2, sy*0.18) + ('Name',))
                
                text = render('render', self.font, '<< %s >>' % races[self.race], True, COLORS_WHITE if self.btn_selected == 'Race' else COLORS_GREY)
                texts.append(make_label(text, sx/2, sy*0.26) + ('Race',))
                text = render('render', self.font, '<< %s >>' % self.titles[self.title], True, COLORS_WHITE if self.btn_selected == 'Title' else COLORS_GREY)
                texts.append(make_label(text, sx/2, sy*0.30) + ('Title',))
                
                
                texts.append((None, draw_char(view, self.tilesets.chars, (CHARSET[races[self.race]], self.var), 
                             (sx/2 - TILE_SIZE/2, sy * 0.4), 'd' if self.btn_selected == 'Var' else 'r', self.step), 'Var'))
                             
                
                text = render('render', self.font, 'E-mail:', True, COLORS_WHITE if self.btn_selected == 'Email' else COLORS_GREY)
                texts.append(make_label(text, sx/2, sy*0.6) + (None,))
                text = render('render', self.font, '%s%s' % (self.email, '|' if len(self.email) < 10 else ''), True, COLORS_WHITE if self.btn_selected == 'Email' else COLORS_GREY)
                texts.append(make_label(text, sx/2, sy*0.64) + ('Email',))
                
                text = render('render', self.font, 'Password: %s%s' % ('*'*len(self.password), '|' if len(self.password) < 10 else ''), True, COLORS_WHITE if self.btn_selected == 'Password' else COLORS_GREY)
                texts.append(make_label(text, sx/2, sy*0.72) + ('Password',))

                text = render('render', self.font, "Tutorial? %s" % ('Y' if self.tutorial else 'N'), True, COLORS_WHITE if self.btn_selected == 'Tutorial' else COLORS_GREY)
                texts.append(make_label(text, sx/2, sy*0.76) + ('Tutorial',))

                text = render('render', self.font, "[ Create Character ]", True, COLORS_WHITE if self.btn_selected == 'Create' else COLORS_GREY)
                texts.append(make_label(text, sx/2, sy*0.8) + ('Create',))

                
                text = render('render', self.font, "BACK TO MENU", True, COLORS_WHITE if self.btn_selected == 'Back' else COLORS_GREY)
                texts.append(make_label(text, sx/2, sy*0.88) + ('Back',))
                
                if time.time() - self.last_time >= 10*(1. / self.char['Speed']): 
                    self.clear()
                    self.step = not self.step
                    self.last_time = time.time()
                    
            elif self.page == 'loadchar':
                text = render('render', self.font, "LOAD CHARACTER", True, COLORS_WHITE)
                texts.append(make_label(text, sx/2, sy*0.1) + (None,))
                
                text = render('render', self.font, 'Name: %s%s' % (self.name, '|' if len(self.name) < 10 else ''), True, COLORS_WHITE if self.btn_selected == 'Name' else COLORS_GREY)
                texts.append(make_label(text, sx/2, sy*0.4) + ('Name',))
                text = render('render', self.font, 'Password: %s%s' % ('*'*len(self.password), '|' if len(self.password) < 10 else ''), True, COLORS_WHITE if self.btn_selected == 'Password' else COLORS_GREY)
                texts.append(make_label(text, sx/2, sy*0.45) + ('Password',))
                
                text = render('render', self.font, "Load", True, COLORS_WHITE if self.btn_selected == 'Load' else COLORS_GREY)
                texts.append(make_label(text, sx/2, sy*0.6) + ('Load',))
                
                text = render('render', self.font, "BACK TO MENU", True, COLORS_WHITE if self.btn_selected == 'Back' else COLORS_GREY)
                texts.append(make_label(text, sx/2, sy*0.9) + ('Back',))
                
            elif self.page == 'options':
                text = render('render', self.font, "OPTIONS", True, COLORS_WHITE)
                texts.append(make_label(text, sx/2, sy*0.1) + (None,))
                
                text = render('render', self.font, 'Graphics:' + {0:'retro', 1:'lores', 2:'hires'}[self.settings.GRAPHICS_QUALITY], True, COLORS_WHITE if self.btn_selected == 'Graphics' else COLORS_GREY)
                texts.append(make_label(text, sx/2, sy*0.4) + ('Graphics',))

                text = render('render', self.font, 'Music:' + ('ON' if self.settings.MUSIC else 'OFF'), True, COLORS_WHITE if self.btn_selected == 'Music' else COLORS_GREY)
                texts.append(make_label(text, sx/2, sy*0.7) + ('Music',))
                
                draw_char(view, self.tilesets.chars, 
                          (CHARSET[races[self.race]], self.var), 
                          (sx/2 - TILE_SIZE/2, sy * 0.5), 
                          'd', False)

                text = render('render', self.font, "BACK TO MENU", True, COLORS_WHITE if self.btn_selected == 'Back' else COLORS_GREY)
                texts.append(make_label(text, sx/2, sy*0.9) + ('Back',))                

            for text, rect, btn in texts:
                if text: view.blit(text, rect)
                if btn:
                    btns[btn] = rect
                    self.taborder.append(btn)
                    
            if not self.btn_selected: self.btn_selected = self.taborder[0]
            
            self.screen.blit(view, (0,0), (0,0)+self.size)
            pygame.display.flip()
            
            
            # events
            for event in pygame.event.get():
                if event.type == pygame.MOUSEMOTION:
                    btn = self.btn_at_pos(event.pos, btns)
                    if btn: self.btn_selected = btn
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    self.btn_press(self.btn_selected, event.button)
                elif event.type == pygame.KEYDOWN:
                    if event.key in (pygame.constants.K_LEFT, pygame.constants.K_RIGHT):
                        if self.btn_selected:
                            self.btn_press(self.btn_selected, 1 if event.key == pygame.constants.K_RIGHT else 3)
                    elif event.key in (pygame.constants.K_UP, pygame.constants.K_DOWN):
                        self.btn_tab(event.key == pygame.constants.K_UP)
                    elif event.key == pygame.constants.K_TAB:
                        self.btn_tab(event.mod&pygame.KMOD_LSHIFT or event.mod&pygame.KMOD_RSHIFT)
                    elif event.key == pygame.constants.K_RETURN:
                        self.btn_press(self.btn_selected, 1)
                    elif event.key == pygame.constants.K_ESCAPE:
                        self.btn_press('Back', 1)
                    else:
                        prop = None
                        max_lengths = {'name': 10, 'password': 10, 'email': 30}
                        if self.btn_selected == 'Password': prop = 'password'
                        elif self.btn_selected == 'Email': prop = 'email'
                        elif self.btn_selected == 'Name': prop = 'name'                        
                        if prop:
                            if event.key in (pygame.constants.K_BACKSPACE, pygame.constants.K_DELETE):
                                setattr(self, prop, getattr(self, prop)[:-1])
                            elif (not event.key in (pygame.constants.K_SPACE,)) and event.unicode:
                                if len(getattr(self, prop)) < max_lengths[prop]: setattr(self, prop, getattr(self, prop) + str(event.unicode))
        

        mixer.music.fadeout(2000)



    def btn_tab(self, back):
        if not self.btn_selected or not self.btn_selected in self.taborder:
            self.btn_selected = self.taborder[0]
        else:
            if self.btn_selected:
                starting_point = self.taborder.index(self.btn_selected)
            direction = -1 if back else 1
            try: self.btn_selected = self.taborder[starting_point + direction]
            except IndexError: self.btn_selected = self.taborder[0]

    def btn_at_pos(self, pos, btns):
        mx, my = pos
        for btn, rect in btns.items():
            if rect.left <= mx <= rect.right and rect.top <= my <= rect.bottom:
                return btn
        return None
        
    def set_char(self):
        self.char = Character('', 0, races[self.race], self.titles[self.title], defs)
        
    def set_possible_titles(self):
        old_title = self.titles[self.title]
        self.titles = []
        race = defs['races_dict'][races[self.race]]
        for var in defs['playable_titles']:
            title = defs['titles_dict'][var]
            if (not [s for s in title.start_skills if s in race.disabled_skills]
                and not [s for s in title.starting_items if defs['items_dict'][s].types().intersection(race.disabled_skills)]):
                self.titles.append(title.name)
        #try: self.title = self.titles.index(old_title)
        #except: self.title = 0
        self.title = 0
        
    def btn_press(self, btn, mouse_btn):
        if btn == 'New Character':
            self.set_possible_titles()
            self.set_char()
            self.turn_page('newchar')
        elif btn == 'Load Character':
            self.turn_page('loadchar')
        elif btn == 'Options':
            self.turn_page('options')
        elif btn == 'Race':
            if mouse_btn == 1: self.race = self.race + 1 if self.race < len(races)-1 else 0
            else: self.race = self.race - 1 if self.race > 0 else len(races)-1
            self.set_possible_titles()
            self.set_char()
            self.clear()
        elif btn == 'Title':
            if mouse_btn == 1: self.title = self.title + 1 if self.title < len(self.titles)-1 else 0
            else: self.title = self.title - 1 if self.title > 0 else len(self.titles)-1
            self.set_char()
            self.clear()
        elif btn == 'Var':
            if mouse_btn == 1: self.var = self.var + 1 if self.var < 5 else 0
            else: self.var = self.var - 1 if self.var > 0 else 5
        elif btn == 'Tutorial':
            self.tutorial = not self.tutorial
        elif btn == 'Create':
            self.name = self.name.replace(' ','')
            self.password = self.password.replace(' ', '')
            self.email = self.email.replace(' ', '')
            if self.name and self.password and self.email:
                self.created = True
                self.done = True
        elif btn == 'Load':
            self.name = self.name.replace(' ','')
            self.password = self.password.replace(' ', '')
            if self.name and self.password:
                self.loaded = True
                self.done = True
        elif btn == 'Graphics':
            d = 1 if mouse_btn == 1 else -1
            self.settings.GRAPHICS_QUALITY += d
            if self.settings.GRAPHICS_QUALITY < 0: self.settings.GRAPHICS_QUALITY = 2
            if self.settings.GRAPHICS_QUALITY > 2: self.settings.GRAPHICS_QUALITY = 0
            
            self.loading_screen()

            self.tilesets = get_tilesets(self.settings)
        elif btn == 'Music':
            self.settings.MUSIC = not self.settings.MUSIC
        elif btn == 'Back':
            self.turn_page('main')
        elif btn == 'Quit':
            self.quit = True
            self.done = True

    def loading_screen(self):
        sx, sy = self.size
        view = self.buffer
        self.clear()
        text = render('render', self.font, "Loading...", True, COLORS_WHITE)
        text, rect = make_label(text, sx/2, sy/2)
        view.blit(text, rect)
        self.screen.blit(view, (0,0), (0,0)+self.size)
        pygame.display.flip()
            
    def clear(self):
        self.invalidated = True
            
    def turn_page(self, page):
        self.clear()        
        self.page = page
        self.btn_selected = None
            

def main():
    menu = MainMenu()
    menu.run()
    if menu.quit:
        sys.exit()
    menu.password = md5.md5(menu.password).hexdigest()
    if menu.created:
        return ','.join(('1', menu.email, menu.password, menu.name, str(menu.var), races[menu.race], menu.titles[menu.title], '1' if menu.tutorial else '0')), menu
    elif menu.loaded:
        return ','.join(('0', menu.name, menu.password)), menu
    

if __name__=='__main__':
    main()
