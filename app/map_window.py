from mmorpg.tiles.tiles import CHARSET, TILESET, OBJSET, get_tilesets
from mmorpg import *
from mmorpg.lib.models import *
from mmorpg.scripts.parse_scripts import get_maps
maps = get_maps()
import numpy as np
import pygame
import time
import pgu.html as html


renders = {}
darknesses = {}
def render(render, font, *args):
    key = (str((font,) + args))
    try:
        result = renders[key]
    except KeyError:
        renders[key] = getattr(font, render)(*args)
        result = renders[key]
    return result


def make_label(text, cx, cy):
    rect = text.get_rect()
    rect.centerx = cx
    rect.centery = cy
    return (text, rect)
    

class MapWindow:
    def __init__(self, parent):
        self.parent = parent
        self.main_window = parent
        self.show_fps = 1
        
        self.listboxes = {}
        
        pygame.font.init()

        font_file = open(MONO_FONT, 'r')
        self.fixed_font = pygame.font.Font(MONO_FONT, 14)
        self.fixed_font.set_bold(True)
        font_file.close()

        font_file = open(MONO_FONT, 'r')
        self.med_font = self.big_font = pygame.font.Font(MONO_FONT, 20)
        font_file.close()

        font_file = open(MONO_FONT, 'r')
        self.big_font = pygame.font.Font(MONO_FONT, 32)
        font_file.close()
        
        self.font = self.fixed_font

        font_file = open(MONO_FONT, 'r')
        self.html_font = pygame.font.Font(MONO_FONT, 14)
        font_file.close()
    
        self.set_size()

        self.scroll = 0
        self.last_position = True

        self.screenshot = False
        

    def load_tilesets(self, tilesets=None):
        if not tilesets: tilesets = get_tilesets(self.parent.settings)
        for key, item in tilesets.__dict__.items():
            setattr(self, key, item)
        

    def set_size(self, expand=0):
        self.width, self.height = self.parent.width, self.parent.height
        self.SCREEN_SIZE = (SCREEN_X + expand, SCREEN_Y)
        self.center_x, self.center_y = [n / 2 - TILE_SIZE / 2 for n in (EMU_X, EMU_Y)]
        (self.global_offset_x, 
         self.global_offset_y) = [n1 - (n2 / 2 * TILE_SIZE)
                                  for n1, n2 in zip([self.center_x, self.center_y], self.SCREEN_SIZE)]
        self.buffer_surface = pygame.Surface((self.SCREEN_SIZE[0]*TILE_SIZE, self.SCREEN_SIZE[1]*TILE_SIZE))
        
        self.msg_box_rect = pygame.Rect(BLOCK_SIZE/2, EMU_Y - (MSG_BOX_HEIGHT+0.5)*BLOCK_SIZE, MSG_BOX_WIDTH*BLOCK_SIZE, MSG_BOX_HEIGHT*BLOCK_SIZE)
        self.msg_render_rect = self.msg_box_rect.copy()
        self.msg_render_rect.height = EMU_Y*2
        self.ability_box_rect = pygame.Rect(BLOCK_SIZE + self.msg_box_rect.width, self.msg_box_rect.top, EMU_X-(BLOCK_SIZE*1.5 + self.msg_box_rect.width), self.msg_box_rect.height)
        self.skills_box_rect = pygame.Rect(BLOCK_SIZE/2, BLOCK_SIZE, EMU_X-BLOCK_SIZE, EMU_Y - (self.msg_box_rect.height + BLOCK_SIZE*2))
        sx, sy = self.html_font.size(' '*30)
        self.skill_label_size = (self.skills_box_rect.width/3, sy + SCALE)
        self.talk_box_rect = pygame.Rect(self.msg_box_rect.left, self.height - sy - BUFFER, MSG_BOX_WIDTH*BLOCK_SIZE, sy)
        


    def draw(self, main_character):
        drawn = []
        
        main_character.resolve_offset_p(self.parent.draw_fps)

        view = self.parent.screen
        map_tiles = maps[main_character.map_name].real_tiles
        map_objs = maps[main_character.map_name].objs
        characters = [main_character] + main_character.peers.values()
        x, y = main_character.position
        main_offset = real_offset(*main_character.offset_p)
        SCREEN_SIZE = self.SCREEN_SIZE
        
        global_offset_x, global_offset_y = self.global_offset_x, self.global_offset_y
        
        # draw tiles
        i_range = [x - SCREEN_SIZE[0] / 2 + wx for wx in xrange(-2, SCREEN_SIZE[0] + 2)]
        j_range = [y - SCREEN_SIZE[1] / 2 + wy for wy in xrange(-2, SCREEN_SIZE[1] + 3)]
        
        tiles, objs = ((self.inv_tiles, self.inv_objs) 
                       if (main_character.invert and float(main_character.hp)/main_character['Vitality'] < 0.33)
                       or main_character.dead
                       else (self.tiles, self.objs))

        if True:#self.last_position and (x, y, main_offset) != self.last_position:
            wx = -2
            for i in i_range:
                wy = -2
                for j in j_range:        
                    try: tile = map_tiles[j][i] if i >= 0 and j >= 0 else '0'
                    except IndexError: tile = '0'
                    tile_def = TILESET[tile]
                    if tile_def.scroll:                        
                        speed = tile_def.scroll_spd
                        scroll = (self.scroll * speed if tile_def.scroll else 0) % TILE_SIZE
                        cx, cy = tile_def.x, (tile_def.y + scroll)
                        gx, gy = (SCALE + cx * BUFFERED_TILE + (ORIG_TILE_BUFFER * SCALE), 
                                  SCALE + cy * BUFFERED_TILE + (ORIG_TILE_BUFFER * SCALE))
                    else:
                        gx, gy = (SCALE + tile_def.x * BUFFERED_TILE, SCALE + tile_def.y * BUFFERED_TILE)
                                        
                    drawn.append(view.blit(tiles, (wx * TILE_SIZE + main_offset[0] + global_offset_x, 
                                           wy * TILE_SIZE + main_offset[1] + global_offset_y), 
                                          (gx, gy, TILE_SIZE, TILE_SIZE)))
                    #if distance(x, y, i, j) > main_character.vision():
                    #    view.blit(self.objs, (wx * TILE_SIZE + main_offset[0], 
                    #                          wy * TILE_SIZE + main_offset[1]), 
                    #                         (0, BUFFERED_TILE, TILE_SIZE, TILE_SIZE))

                    wy += 1
                    
                wx += 1
            self.last_position = (x, y, main_offset)
            
        # draw chars
        texts = []

        chars_to_draw = []
        target_lines = []
        wpns_to_draw = []
        rects_to_draw = []
        for character in characters:
            if isinstance(character, Character):
                old_character = character
                character = character.sprite()
                character.offset = real_offset(*old_character.offset_p)
                character.fly_offset = old_character.fly_offset
                character.damage_numbers = old_character.damage_numbers
            elif isinstance(character, Sprite):
                character.resolve_offset_p(self.parent.draw_fps)
            else:
                continue
                
            px,py = character.position
            if i_range[0] <= px <= i_range[-1] and j_range[0] <= py <= j_range[-1]:
                if px == np.inf or py == np.inf: break
                dx, dy = onscreen(character, main_character, SCREEN_SIZE)
                dx += global_offset_x
                dy += global_offset_y
                direction = 'dead' if hasattr(character, 'dead') and character.dead else character.direction[0]
                step = character.step if hasattr(character, 'step') else False
                
                race, colors = character.pic
                try:
                    char = (CHARSET[race], colors)
                except:
                    char = (CHARSET['Human'], colors)
                                
                flying = character.flying if hasattr(character, 'flying') else False
                if direction == 'dead': flying = False
                if flying:
                    drawn.append(draw_obj(view, self.objs, 'shadow', (dx, dy + (1 + smoothstep(character.fly_offset)) * TILE_SIZE)))
                
                if hasattr(character, 'damage_numbers') and character.damage_numbers:
                    for n, dur in character.damage_numbers:
                        if isinstance(n, int):
                            desc, color = (str(abs(n)), COLORS_WHITE) if n < 0 else ('+%s' % n, COLORS_GREEN)
                        elif isinstance(n, str) and n.endswith('!'):
                            try:
                                n = abs(int(n[:-1]))
                                desc, color = '%s!' % n, COLORS_RED
                            except:
                                desc, color = n, COLORS_BLUE
                        else:
                            desc, color = str(n), COLORS_WHITE
                            #desc, color = (n, COLORS_BLUE if 'XP' in n else COLORS_WHITE)
                        text = render('render', self.med_font, desc, ANTIALIAS, color)
                        texts.append(make_label(text, dx + (TILE_SIZE / 2), dy - (TILE_SIZE * (1-dur) * 2)))
                
                if hasattr(character, 'attack_animation') and character.attack_animation:
                    for a in character.attack_animation:
                        self.draw_wpn_animation(view, wpns_to_draw, main_character, dx, dy, global_offset_x, global_offset_y, character, a)
                
                if hasattr(character, 'invert') and character.invert:
                    chars = self.inv_chars
                elif hasattr(character, 'poison') and character.poison:
                    chars = self.chars if int(self.scroll)%4 else self.psn_chars
                else: 
                    chars = self.chars
                
                chars_to_draw.append((flying, direction=='dead', dy, (view, chars, char, (dx, dy), direction, step)))
                
                if ((not main_character.uid ==  character.uid)
                    and main_character.looking_at == character.uid 
                    and (not int(self.scroll) % 3)):
                    draw_obj(view, self.objs, 'attacking' if main_character.attacking == character.uid else 'selection', (dx, dy))

                length = TILE_SIZE * 3 if hasattr(character, 'boss') else TILE_SIZE
                if not direction=='dead':
                    if hasattr(character, 'attacking') and character.attacking:
                        target = character.current_target(main_character.peers.values())

                        if target:
                            tx, ty = onscreen(target, main_character, SCREEN_SIZE)
                        
                        #target_lines.append((view, 
                        #                    COLORS_RED, 
                        #                    (dx+TILE_SIZE/2, dy+TILE_SIZE*3/4), 
                        #                    (tx+TILE_SIZE/2, ty+TILE_SIZE/2), 2))
                    
                        #if self.character.attacking and target == character:
                        #    color = COLORS_RED
                        #else: color = COLORS_WHITE
                        
                        left = dx + TILE_SIZE/2 - length/2
                        top = dy - SCALE*2
                        rects_to_draw.append((view, COLORS_WHITE, (left, top, length*character.attack_wait/ATTACK_THRESHOLD, SCALE/2)))

                    if (hasattr(character, 'attack_progress') and character.attack_progress) and (hasattr(character, 'attacking_with') and character.attacking_with):
                        summary, color = character.attacking_with, COLORS_WHITE
                    else:
                        summary, color = character.short_summary, character.rgb
                    text = render('render', self.font, summary, ANTIALIAS, color)
                    texts.append(make_label(text, dx + (TILE_SIZE / 2), dy - TILE_SIZE / 4))
                    
                    if hasattr(character, 'hp'):
                        left = dx + TILE_SIZE/2 - length/2
                        top = dy - SCALE*2 + SCALE/2
                        rects_to_draw.append((view, COLORS_RED, (left, top, length, SCALE/2)))
                        rects_to_draw.append((view, COLORS_GREEN, (left, top, length*character.hp, SCALE/2)))
                        
                    if hasattr(character, 'stamina'):
                        left = dx + TILE_SIZE/2 - length/2
                        top = dy - SCALE
                        rects_to_draw.append((view, COLORS_BLACK, (left, top, length, SCALE/2)))
                        rects_to_draw.append((view, COLORS_YELLOW, (left, top, length*character.stamina, SCALE/2)))

        chars_to_draw.sort(key = lambda c: (1 if c[0] else 0, c[2]))
        #for line in target_lines:
        #    pygame.draw.line(*line)
        
        for _, _, _, args in [c for c in chars_to_draw if c[1]]:
            drawn.append(draw_char(*args))
        for _, _, _, args in [c for c in chars_to_draw if not c[0] and not c[1]]:
            drawn.append(draw_char(*args))
            

        # draw objs
        wx = -2
        for i in i_range:
            wy = -2
            for j in j_range:
                if (i,j) in map_objs:
                    for obj in map_objs[(i,j)]:
                        obj_def = OBJSET[obj.type]
                        speed = obj_def.scroll_spd
                        scroll = (self.scroll * speed if obj_def.scroll else 0) % TILE_SIZE

                        ta, tb, h = obj_def.x, obj_def.y, obj_def.height

                        cx, cy = (SCALE + ta * BUFFERED_TILE, SCALE + tb * BUFFERED_TILE - (h * TILE_SIZE))
                        drawn.append(view.blit(objs, 
                                               (wx * TILE_SIZE + main_offset[0] + global_offset_x, 
                                                wy * TILE_SIZE + main_offset[1] - (h * TILE_SIZE) + global_offset_y),
                                     (cx, cy+scroll, TILE_SIZE, TILE_SIZE * (1 + h))))
                wy += 1
            wx += 1


        if not main_character.dead:
            summary = main_character.short_summary()
            sum_len = len(summary)
            hp_len = len(str(main_character['Vitality']))
            stam_len = len(str(main_character['Stamina']))
            hp_color = color_from_amt(float(main_character.hp) / main_character['Vitality'], html=False)
            stamina_color = color_from_amt(float(main_character.stamina) / main_character['Stamina'], html=False)
            vital_label = '  %s  HP %s/%s  Stamina %s/%s  %s XP  %sg  ' % (summary,
                                                                           ' '*hp_len, 
                                                                           main_character['Vitality'],
                                                                           ' '*stam_len, 
                                                                           main_character['Stamina'],
                                                                           main_character.xp,
                                                                           main_character.gold)
            hp_label = ' '*(sum_len + 7 + (hp_len-len(str(main_character.hp)))) + '%s' % (main_character.hp)
            hp_label += ' '*(len(vital_label)-len(hp_label))
            stamina_label = ' '*(sum_len+18+hp_len*2 + (stam_len-len(str(main_character.stamina)))) + '%s' % (main_character.stamina)
            stamina_label += ' '*(len(vital_label)-len(stamina_label))

            for (label, color) in [(vital_label, (COLORS_WHITE, COLORS_BLACK)),
                                    (hp_label, (hp_color,)),
                                    (stamina_label, (stamina_color,)),
                                    ]:
                text = render('render', self.fixed_font, label, ANTIALIAS, *color)
                rect = text.get_rect()
                rect.centerx = (self.center_x)
                rect.top = 0
                texts.append((text, rect))
        else:
            time_left = int(main_character.dead)
            text = render('render', self.big_font, 'Respawn in: %3.f seconds' % time_left, ANTIALIAS, COLORS_WHITE)
            rect = text.get_rect()
            rect.centerx = (self.center_x)
            rect.top = TILE_SIZE
            texts.append((text, rect))

        if self.show_fps:
            text = render('render', self.fixed_font, 'FPS=%s' % (int(self.parent.draw_fps)), ANTIALIAS, COLORS_WHITE)
            texts.append(make_label(text, 100, 50))

        # draw flying chars and character labels
        for _, _, _, args in [c for c in chars_to_draw if c[0] and not c[1]]:
            drawn.append(draw_char(*args))
        for text, rect in texts:
            drawn.append(view.blit(text, rect))
        for args in wpns_to_draw:
            drawn.append(draw_wpn(*args))
        for args in rects_to_draw:
            pygame.draw.rect(*args)
            
        for btn in (
                    'item',
                    'skill',
                    'ability'
                    ):
            self.parent.buttons[btn] = []
        self.draw_message_box(main_character, view)
        self.draw_shop(main_character, view)
        self.draw_ability_box(main_character, view)
        self.draw_choice_box(main_character, view)
        self.draw_skills_box(main_character, view)
        self.draw_items_box(main_character, view)
        self.draw_talk_box(main_character, view)
        
        if self.parent.skills_shown or self.parent.items_shown or self.parent.shop_shown:
            self.parent.buttons['skills_show'] = None
            self.parent.buttons['items_show'] = None
        else:
            rect = self.skills_box_rect

            coords = (rect.right - BLOCK_SIZE, rect.top, BLOCK_SIZE, BLOCK_SIZE)
            self.parent.buttons['skills_show'] = coords
            draw_icon(view, self.icons, 'skillsbtn', coords)
            
            coords = (rect.right - BLOCK_SIZE, rect.top + BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE)
            self.parent.buttons['items_show'] = coords
            draw_icon(view, self.icons, 'equipbtn', coords)
            
        sx, sy = self.width, self.height
        #scale_function = pygame.transform.smoothscale if self.parent.draw_fps > 24 else pygame.transform.scale
        #scale_function = pygame.transform.scale
        #view = scale_function(view, (sx,sy))
        #sx, sy = self.SCREEN_SIZE[0]*TILE_SIZE, self.SCREEN_SIZE[1]*TILE_SIZE
        #dest.blit(view, (0,0), (0,0,sx,sy))

        if self.screenshot:
            pygame.image.save(view, time.strftime('%Y %h %d %H:%M:%S.png'))
            self.screenshot = False



    def draw_wpn_animation(self, view, wpns_to_draw, main_character, dx, dy, global_offset_x, global_offset_y, character, animation, orig_progress=None, trace=False):
        if not trace is False and trace <= 0: return
        if 'image' in animation:
            image, animation_type, direction = [animation[x] if x in animation else 0
                                               for x in 
                                               ['image', 'animation_type', 'direction']]
            progress = orig_progress
            if not progress: 
                progress = character.attack_progress if hasattr(character, 'attack_progress') else 0
                orig_progress = progress
            if trace: image = animation['trace']                
            mx, my = move_directions[direction]
            if animation_type == 'projectile':
                progress = max((progress - 0.4) / 0.6, 0)
                (src_x, src_y), (dest_x, dest_y) = animation['src_pos'], animation['dest_pos']
                src_fly, dest_fly = animation['src_fly'], animation['dest_fly']
                move_vector = (dest_x - src_x, dest_y - src_y)
                move_dist = (move_vector[0]**2 + move_vector[1]**2)**0.5
                proj_z = move_dist / 2
                proj_z *= TILE_SIZE
                elevate = 1 if not 'elevate' in animation else animation['elevate']
                proj_z *= elevate
                proj_x, proj_y = (src_x + move_vector[0]*(1-progress), src_y + move_vector[1]*(1-progress))
                proj_x, proj_y = onscreen((proj_x, proj_y), main_character, self.SCREEN_SIZE)
                proj_x += global_offset_x
                proj_y += global_offset_y
                draw_obj(view, self.objs, 'smshadow', (proj_x, proj_y))
                if src_fly or dest_fly:
                    src_z = 1.5 if src_fly else 0
                    dest_z = 1.5 if dest_fly else 0
                    proj_y -= (progress*src_z + (1-progress)*dest_z) * TILE_SIZE
                if progress <= 0.5: proj_y -= smoothprojectile(progress * 2) * proj_z
                else: proj_y -= smoothprojectile((1-(progress - 0.5)*2)) * proj_z
                
                if 'trace' in animation and animation['trace'] and orig_progress > 0.2:
                    progress = round(orig_progress, 2) + 0.05
                    if progress < 1:
                        self.draw_wpn_animation(view, wpns_to_draw, main_character, dx, dy, global_offset_x, global_offset_y, character, animation, orig_progress=progress, trace=(trace-1 if trace else 4))
                    
                wpns_to_draw.append((view, self.objs, image, (proj_x, proj_y), direction))
            else:
                dist = smoothstep(1-max((progress-.8)*4, 0))
                if image and not image=='0': 
                    wpns_to_draw.append((view, self.objs, image, (dx + mx*TILE_SIZE*dist, dy + my*TILE_SIZE*dist), direction))

        
        
    def draw_talk_box(self, main_character, view):
        if self.parent.talk_shown:
            talk = 'Say: %s|' % self.parent.talk_msg[-40:]
            rect = self.talk_box_rect
            talk_label = render('render', self.font, talk, ANTIALIAS, COLORS_WHITE)
            w, h = talk_label.get_size()
            view.blit(talk_label, (rect.left, rect.top), (0, 0, w, h))



    def draw_window(self, rect, view):
        margin = SCALE*2
        rect = pygame.Rect(rect.left-margin,rect.top-margin,rect.width+margin*2, rect.height+margin*2)
        key = str(rect)
        if key in darknesses: darkness = darknesses[key]
        else:
            darkness = pygame.transform.scale(self.darkness, (rect.width, rect.height))
            darknesses[key] = darkness
        view.blit(darkness, (rect.left, rect.top),
                  (0, 0, rect.width, rect.height))
    

    def draw_message_box(self, main_character, view):
        if main_character.force_msg or main_character.choices:
            rect, rect2 = self.msg_box_rect, self.msg_render_rect
            self.draw_window(rect, view)
                    
            error = True
            while error:
                try:
                    html_label = render('rendertrim', html, self.html_font, rect2, self.parent.msg_history, ANTIALIAS, COLORS_WHITE)
                    error = False
                except:
                    error=True
                if error: self.parent.shorten_msg_history()
            w, h = html_label.get_size()
            if h > rect.height: 
                if self.parent.msg_scroll > h - rect.height: self.parent.msg_scroll = h - rect.height
                html_label = html_label.subsurface((0, h - rect.height - self.parent.msg_scroll, w, rect.height))
            #view.blit(html_label, (0, 0), (0, 0) + html_label.get_size())
            view.blit(html_label, (rect.left, rect.top), (0, 0, rect.width, rect.height))
            
            #coords = (self.msg_box_rect.right - BLOCK_SIZE, self.msg_box_rect.top - BLOCK_SIZE/2, BLOCK_SIZE, BLOCK_SIZE)
            #draw_icon(view, self.icons, 'close', coords)
            #self.parent.buttons['msg_close'] = coords
            self.parent.buttons['msg_show'] = None
            if self.parent.msg_scroll < h-rect.height: 
                coords = (self.msg_box_rect.right - BLOCK_SIZE*3/4, self.msg_box_rect.top, BLOCK_SIZE, BLOCK_SIZE)
                draw_icon(view, self.icons, 'uparrow', coords)
                self.parent.buttons['msg_uparrow'] = coords
            else: self.parent.buttons['msg_uparrow'] = None
            if self.parent.msg_scroll > 0: 
                coords = (self.msg_box_rect.right - BLOCK_SIZE*3/4, self.msg_box_rect.top + BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE)
                draw_icon(view, self.icons, 'downarrow', coords)
                self.parent.buttons['msg_downarrow'] = coords
            else: self.parent.buttons['msg_downarrow'] = None
        else:
            coords = (self.msg_box_rect.left, self.msg_box_rect.bottom - BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE)
            draw_icon(view, self.icons, 'msgbtn', coords)
            self.parent.buttons['msg_show'] = coords
            self.parent.buttons['msg_close'] = None



    def draw_ability_box(self, main_character, view):
        self.parent.buttons['ability'] = []
        
        if self.parent.ability_shown and not main_character.choices:
            rect = self.ability_box_rect
            self.draw_window(rect, view)
                    
            sx, sy = self.skill_label_size
            
            abilities = main_character.sorted_abilities()
            
            scroll = self.parent.ability_scroll
            scroll_up = scroll>0
            scroll_down = False
            
            for n, (skill, btn, cost) in enumerate(abilities):
                b = n
                
                rect2 = pygame.Rect(rect.left, rect.top + b*sy - scroll, sx, sy)
                if rect2.bottom < rect.top: continue 
                if rect2.top > rect.bottom: scroll_down = True; continue
                
                label = '%s<b>%s</b>' % (n+1, skill)
                if cost: label += ' %s' % cost
                html_label = render('render', html, self.html_font, rect2, label, ANTIALIAS, 
                                    COLORS_WHITE if main_character.attacking_with == btn else COLORS_GREY)
                
                if rect2.top < rect.top:
                    overlap = rect.top - rect2.top
                    width, height = html_label.get_size()
                    html_label = html_label.subsurface((0, overlap, width, height-overlap))
                    rect2 = pygame.Rect((rect2.left, rect2.top + overlap, rect2.width, rect2.height - overlap))
                if rect2.bottom > rect.bottom: 
                    scroll_down = True
                    overlap = rect2.top - rect.top
                    width, height = html_label.get_size()
                    html_label = html_label.subsurface((0, 0, width, height-overlap))
                    rect2 = pygame.Rect((rect2.left, rect2.top, rect2.width, rect2.height - overlap))
                    
                result = view.blit(html_label, (rect2.left, rect2.top), (0, 0, rect2.width, rect2.height))
                
                width, height = self.html_font.size(label)
                self.parent.buttons['ability'].append((btn, (result.left, result.top, width, height)))
                
                
                
    def draw_choice_box(self, main_character, view):
        if main_character.choices:
            rect = self.ability_box_rect
            self.draw_window(rect, view)
                    
            sx, sy = self.skill_label_size
            
            self.parent.buttons['choice'] = []

            scroll = self.parent.ability_scroll
            scroll_up = scroll>0
            scroll_down = False
            
            for n, choice in enumerate(main_character.choices):
                b = n
                
                rect2 = pygame.Rect(rect.left, rect.top + b*sy - scroll, sx, sy)
                if rect2.bottom < rect.top: continue 
                if rect2.top > rect.bottom: scroll_down = True; continue
                
                label = '%s<b>%s</b>' % (n+1, choice)
                html_label = render('render', html, self.html_font, rect2, label, ANTIALIAS, COLORS_WHITE)
                
                if rect2.top < rect.top:
                    overlap = rect.top - rect2.top
                    width, height = html_label.get_size()
                    html_label = html_label.subsurface((0, overlap, width, height-overlap))
                    rect2 = pygame.Rect((rect2.left, rect2.top + overlap, rect2.width, rect2.height - overlap))
                if rect2.bottom > rect.bottom: 
                    scroll_down = True
                    overlap = rect2.top - rect.top
                    width, height = html_label.get_size()
                    html_label = html_label.subsurface((0, 0, width, height-overlap))
                    rect2 = pygame.Rect((rect2.left, rect2.top, rect2.width, rect2.height - overlap))
                    
                result = view.blit(html_label, (rect2.left, rect2.top), (0, 0, rect2.width, rect2.height))
                
                width, height = self.html_font.size(label)
                self.parent.buttons['choice'].append((choice, (result.left, result.top, width, height)))



    def draw_skills_box(self, main_character, view):
        if self.parent.skills_shown:

            self.parent.buttons['skill'] = []
            self.parent.buttons['upgrade'] = []

            rect = self.skills_box_rect
            self.draw_window(rect, view)

            sx, sy = self.skill_label_size
            rows_per_column = int(rect.height / sy)

            sorted_skills = main_character.sorted_skills()
            learnable_skills = main_character.learnable_skills()

            # left part of panel: learned skills
            items = []
            for skill, level, exp in sorted_skills:
                cost = main_character.upgrade_cost(skill)
                can_upgrade = cost and main_character.xp >= cost and not main_character.penalty
                if level in (None, False):
                    label = '<b>%s</b>' % skill
                    color = COLORS_WHITE
                elif level is True:
                    label = '<i>%s</i>' % skill
                    if 'Resist' in skill: color = COLORS_GREEN
                    elif 'Weak' in skill: color = COLORS_RED
                    else: color = COLORS_WHITE
                else:
                    spaces = 3-len(str(level))
                    label = ('<b>%s%s</b> %s' % ('.'*spaces, level, skill))
                    if skill == 'Goodness': color = main_character.rgb()
                    color = COLORS_WHITE if (can_upgrade) else COLORS_GREY
                    if main_character.equipment_attr(skill) > 0:
                        color = COLORS_GREEN if color == COLORS_WHITE else COLORS_FADED_GREEN
                    elif main_character.equipment_attr(skill) < 0:
                        color = COLORS_RED if color == COLORS_WHITE else COLORS_FADED_RED

                if cost: label += ' %sXP' % cost
                if exp: 
                    label += '<i>%s%%</i>' % (100*exp / USE_SKILL_THRESHOLD)
                    color = COLORS_WHITE
                if self.parent.selected_skill == skill: color = COLORS_YELLOW if (can_upgrade) else COLORS_FADED_YELLOW
                
                items.append((label, color, skill))
            
            if not 'Skills' in self.listboxes:
                self.listboxes['Skills'] = Listbox('Skills', 'skill', self, rect, 'left')
            self.listboxes['Skills'].draw(items, view)

            # right part of panel: learnable skills
            items = []
            for skill, cost, natural in learnable_skills:
                label = (skill) if not (cost is None) else ('<b>%s</b>' % (skill))
                can_upgrade = cost and main_character.xp >= cost and not main_character.penalty
                if natural: color = COLORS_GREEN if (can_upgrade or level is None) else COLORS_FADED_GREEN
                else: color = COLORS_WHITE if (can_upgrade or cost is None) else COLORS_GREY
                if self.parent.selected_skill == skill: color = COLORS_YELLOW if (can_upgrade) else COLORS_FADED_YELLOW
                items.append((label, color, skill))
                
            
            if not 'Learn' in self.listboxes:
                self.listboxes['Learn'] = Listbox('Learn', 'skill', self, rect, 'right')
            self.listboxes['Learn'].draw(items, view)
            
            
            rect2 = pygame.Rect((rect.left + rect.width/3, rect.top, rect.width/3, rect.height/3))
            label = '%s<br><br>Total XP: <b>%s</b><br>' % (main_character.fancy_summary(), main_character.spent_xp)
            if main_character.next_level():
                label += 'Next Level: <b>%s</b><br>' % main_character.next_level()
            label += 'Available XP: <b>%s</b>' % main_character.xp
            html_label = render('render', html, self.html_font, rect2, label, ANTIALIAS, COLORS_WHITE)
            result = view.blit(html_label, (rect2.left, rect2.top), (0, 0, rect2.width, rect2.height))
                

            # lower middle part of panel: currently selected skill
            if self.parent.selected_skill and self.parent.selected_skill in main_character.defs['skills_dict']:
                rect2 = pygame.Rect((rect.left + rect.width/3, rect.top + rect.height*2/5, rect.width/3, rect.height*3/5))
                this_skill = main_character.defs['skills_dict'][self.parent.selected_skill]
                label = '<b>%s</b>' % this_skill.name
                if this_skill.name in main_character.skill_upgrades: label += str(main_character[this_skill.name])
                elif main_character.upgrade_cost(this_skill.name): label += '%s XP' % main_character.upgrade_cost(this_skill.name)
                label += '<br><br>'
                if this_skill.formula: label += '<i>%s</i><br><br>' % html_format(this_skill.formula)
                label += this_skill.description
                html_label = render('render', html, self.html_font, rect2, label, ANTIALIAS, COLORS_WHITE)
                result = view.blit(html_label, (rect2.left, rect2.top), (0, 0, rect2.width, rect2.height))
                
                cost = main_character.upgrade_cost(self.parent.selected_skill)
                if cost and main_character.xp >= cost and not main_character.penalty:
                    verb = 'upgrade' if self.parent.selected_skill in main_character.skills else 'learn'
                    text = ' Click to %s (%s XP) ' % (verb, cost)
                    label = render('render', self.font, text, ANTIALIAS, COLORS_WHITE, COLORS_BLACK)
                    rect2 = label.get_rect()
                    rect2.centerx = rect.centerx
                    rect2.bottom = rect.bottom
                    result = view.blit(label, (rect2.left, rect2.top), (0, 0, rect2.width, rect2.height))
                    self.parent.buttons['upgrade'].append((self.parent.selected_skill, (result.left, result.top, result.width, result.height)))
                    
            if main_character.penalty:
                text = 'Penalty: %s XP' % (main_character.penalty)
                label = render('render', self.font, text, ANTIALIAS, COLORS_RED)
                rect2 = label.get_rect()
                rect2.centerx = rect.centerx
                rect2.bottom = rect.bottom
                result = view.blit(label, (rect2.left, rect2.top), (0, 0, rect2.width, rect2.height))

            coords = (rect.right - BLOCK_SIZE, rect.top, BLOCK_SIZE, BLOCK_SIZE)
            draw_icon(view, self.icons, 'close', coords)
            self.parent.buttons['skills_close'] = coords
            self.parent.buttons['skills_show'] = None
        else:
            self.parent.buttons['skills_close'] = None
            self.parent.buttons['skill'] = None
            self.parent.buttons['upgrade'] = None
            self.parent.buttons['skills_uparrow'] = None
            self.parent.buttons['skills_downarrow'] = None
            self.parent.buttons['learn_uparrow'] = None
            self.parent.buttons['learn_downarrow'] = None
            
            

    def draw_items_box(self, main_character, view):
        if self.parent.items_shown:
            self.parent.buttons['item'] = []
            self.parent.buttons['use'] = []
            self.parent.buttons['equip'] = []
            self.parent.buttons['unequip'] = []
            self.parent.buttons['drop'] = []
            
            rect = self.skills_box_rect
            self.draw_window(rect, view)
            
            sx, sy = self.skill_label_size
            rows_per_column = int(rect.height / sy)
            
            equip_slots = main_character.equipment_slots()
            
            # equipment (left) panel
            items = []
            for slot in equip_slots:
                equipped = main_character.equipped(slot)
                
                color = COLORS_WHITE if not self.parent.selected_item == equipped else COLORS_YELLOW
                
                items.append(('<i>%s</i>' % slot, color, equipped))
                items.append(('  <b>%s</b>' % equipped, color, equipped))
            
            if not 'Equipment' in self.listboxes:
                self.listboxes['Equipment'] = Listbox('Equipment', 'item', self, rect, 'left')
            self.listboxes['Equipment'].draw(items, view)
            
            # inventory (right) panel
            items = []
            for n, (item, qty, equipped) in enumerate([(item.item.name, item.quantity, item.equipped) for item in main_character.items]):
                label = '%s%s' % (item, (' x%s' % qty) if qty > 1 else '')
                if equipped: label = '<b>%s</b>' % label
                color = COLORS_WHITE if not self.parent.selected_item == item else COLORS_YELLOW
                items.append((label, color, item))
                
            if not 'Items' in self.listboxes:
                self.listboxes['Items'] = Listbox('Items', 'item', self, rect, 'right')
            self.listboxes['Items'].draw(items, view)
            
            # info at top center
            rect2 = pygame.Rect((rect.left + rect.width/3, rect.top, rect.width/3, rect.height/3))
            label = 'Dmg/sec <b>%s</b><br>Range <b>%s</b>%s<br>Defense <b>%s</b><br><br>Gold <b>%s</b>' % (
                     main_character.dps(), main_character.range(),
                     (', Splash <b>%s</b>' % main_character.splash()) if main_character.splash() else '',
                     main_character['Defense'], main_character.gold)
            html_label = render('render', html, self.html_font, rect2, label, ANTIALIAS, COLORS_WHITE)
            result = view.blit(html_label, (rect2.left, rect2.top), (0, 0, rect2.width, rect2.height))
            
            
            # item description 
            if self.parent.selected_item and self.parent.selected_item in main_character.defs['items_dict']:
                this_item = main_character.find_item(self.parent.selected_item)
                if not this_item: self.parent.selected_item = None
                else:
                    self.draw_item_description(self.parent.selected_item, main_character, view)
                    
                    verbs = this_item.verbs(main_character)
                
                    if 'equip' in verbs or 'unequip' in verbs:
                        verb = 'Equip' if 'equip' in verbs else 'Unequip'
                        label = render('render', self.font, ' %s ' % verb, ANTIALIAS, COLORS_GREEN if verb == 'Equip' else COLORS_RED, COLORS_BLACK)
                        rect2 = label.get_rect()
                        rect2.centerx = rect.centerx - sx
                        rect2.bottom = rect.bottom
                        result = view.blit(label, (rect2.left, rect2.top), (0, 0, rect2.width, rect2.height))
                        self.parent.buttons[verb.lower()].append((self.parent.selected_item, (result.left, result.top, result.width, result.height)))

                    if 'use' in verbs:
                        verb = 'Use'
                        label = render('render', self.font, ' %s ' % verb, ANTIALIAS, COLORS_GREEN, COLORS_BLACK)
                        rect2 = label.get_rect()
                        rect2.centerx = rect.centerx
                        rect2.bottom = rect.bottom
                        result = view.blit(label, (rect2.left, rect2.top), (0, 0, rect2.width, rect2.height))
                        self.parent.buttons[verb.lower()].append((self.parent.selected_item, (result.left, result.top, result.width, result.height)))
    
                    if 'drop' in verbs:
                        verb = 'Drop'
                        label = render('render', self.font, ' %s ' % verb, ANTIALIAS, COLORS_RED, COLORS_BLACK)
                        rect2 = label.get_rect()
                        rect2.centerx = rect.centerx + sx
                        rect2.bottom = rect.bottom
                        result = view.blit(label, (rect2.left, rect2.top), (0, 0, rect2.width, rect2.height))
                        self.parent.buttons[verb.lower()].append((self.parent.selected_item, (result.left, result.top, result.width, result.height)))
                        
            coords = (rect.right - BLOCK_SIZE, rect.top, BLOCK_SIZE, BLOCK_SIZE)
            draw_icon(view, self.icons, 'close', coords)
            self.parent.buttons['items_close'] = coords
            self.parent.buttons['items_show'] = None
            self.parent.buttons['skills_show'] = None
        else:
            self.parent.buttons['items_close'] = None
            self.parent.buttons['item'] = None
            self.parent.buttons['use'] = None
            self.parent.buttons['drop'] = None
            self.parent.buttons['equip'] = None
            
            
            
    def draw_shop(self, main_character, view):
        if not self.parent.shop_shown and not self.parent.items_shown:
            self.parent.selitem_scroll = 0
        shop = self.parent.shop_shown = main_character.shopping
        
        self.parent.buttons['buyitem'] = []
        self.parent.buttons['sellitem'] = []
        self.parent.buttons['buy'] = []
        self.parent.buttons['sell'] = []
        
        if self.parent.shop_shown:
            self.parent.skills_shown = False
            self.parent.items_shown = False
            
            rect = self.skills_box_rect
            self.draw_window(rect, view)
            
            sx, sy = self.skill_label_size
            rows_per_column = int(rect.height / sy)
            
            for_purchase = shop.buy_summary(main_character)
            
            # purchase (left) panel
            items = []
            length = max(3, max([len(str(price)) for item, price in for_purchase]))
            for item, price in for_purchase:
                item_name = item.name
                if self.parent.selected_buy_item == item_name: 
                    color = COLORS_YELLOW if main_character.gold >= price else COLORS_FADED_YELLOW
                else:
                    if 'Equippable' in item.types():
                        if main_character.can_equip(item):
                            color = COLORS_WHITE if main_character.gold >= price else COLORS_GREY
                        else:
                            color = COLORS_RED if main_character.gold >= price else COLORS_FADED_RED
                    else:
                        color = COLORS_WHITE if main_character.gold >= price else COLORS_GREY
                
                items.append(('<b>%s</b>%s' % (str(price).zfill(length), item_name), color, item_name))
            
            if not 'Buy' in self.listboxes:
                self.listboxes['Buy'] = Listbox('Buy', 'buyitem', self, rect, 'left')
            self.listboxes['Buy'].draw(items, view)
            
            if self.parent.selected_buy_item and main_character.gold >= shop.purchase_price(main_character.defs['items_dict'][self.parent.selected_buy_item], main_character):
                verb = 'Buy'
                label = render('render', self.font, ' %s ' % verb, ANTIALIAS, COLORS_GREEN, COLORS_BLACK)
                rect2 = label.get_rect()
                rect2.centerx = rect.centerx - sx
                rect2.bottom = rect.bottom
                result = view.blit(label, (rect2.left, rect2.top), (0, 0, rect2.width, rect2.height))
                self.parent.buttons[verb.lower()].append((self.parent.selected_item, (result.left, result.top, result.width, result.height)))
            
            
            # inventory (right) panel
            items = []
            length = max(3, max([len(str(shop.buyback_price(item.item, main_character))) for item in main_character.items]))
            for n, item in enumerate(main_character.items):
                item_name = item.item.name
                qty = item.quantity
                equipped = item.equipped
                label = '<b>%s</b>%s%s' % (str(shop.buyback_price(item.item, main_character)).zfill(length), item_name, (' x%s' % qty) if qty > 1 else '')
                if equipped: label = '<b>%s</b>' % label
                color = COLORS_WHITE if not self.parent.selected_sell_item == item_name else COLORS_YELLOW
                items.append((label, color, item_name))
                
            if not 'Sell' in self.listboxes:
                self.listboxes['Sell'] = Listbox('Sell', 'sellitem', self, rect, 'right')
            self.listboxes['Sell'].draw(items, view)
            
            if self.parent.selected_sell_item:
                verb = 'Sell'
                label = render('render', self.font, ' %s ' % verb, ANTIALIAS, COLORS_GREEN, COLORS_BLACK)
                rect2 = label.get_rect()
                rect2.centerx = rect.centerx + sx
                rect2.bottom = rect.bottom
                result = view.blit(label, (rect2.left, rect2.top), (0, 0, rect2.width, rect2.height))
                self.parent.buttons[verb.lower()].append((self.parent.selected_item, (result.left, result.top, result.width, result.height)))
            
            
            # info at top center
            rect2 = pygame.Rect((rect.left + rect.width/3, rect.top, rect.width/3, rect.height/3))
            label = '<b>%s</b><br><br>Gold <b>%s</b>' % (
                     main_character.shopping.name, main_character.gold
                     )
            html_label = render('render', html, self.html_font, rect2, label, ANTIALIAS, COLORS_WHITE)
            result = view.blit(html_label, (rect2.left, rect2.top), (0, 0, rect2.width, rect2.height))
            
            
            # item description 
            if self.parent.selected_buy_item and self.parent.selected_buy_item in main_character.defs['items_dict']:
                self.draw_item_description(self.parent.selected_buy_item, main_character, view)
            
            
            self.parent.buttons['items_show'] = None
            self.parent.buttons['skills_show'] = None
            
    
    def draw_item_description(self, this_item, main_character, view):
        scroll = self.parent.selitem_scroll
        scroll_up = scroll>0
        scroll_down = False
        rect = self.skills_box_rect

        item = main_character.defs['items_dict'][this_item]
        rect2 = pygame.Rect((rect.left + rect.width/3, rect.top + rect.height/3, rect.width/3, rect.height*2/3))
        label = '<b>%s</b><br>%s' % (item.name, ('<br><i>%s</i><br>' % html_format(item.description) if item.description else ''))
        for name, value in item.attributes:
            if name[0] == name[0].upper():
                label += '<br><i>%s</i><br> <b>%s</b>' % (name, html_format(value))
        render_rect = pygame.Rect((rect2.left, rect2.top, rect2.width, EMU_Y*2))
        html_label = render('rendertrim', html, self.html_font, render_rect, label, ANTIALIAS, COLORS_WHITE)
        w, h = html_label.get_size()
        max_scroll = h - rect2.height
        if h > rect2.height: 
            scroll_down = True
        if scroll >= max_scroll: 
            self.parent.selitem_scroll = max_scroll
            scroll = self.parent.selitem_scroll
            scroll_down = False
        if h > rect2.height:
            html_label = html_label.subsurface((0, scroll, w, rect2.height))
            
        result = view.blit(html_label, (rect2.left, rect2.top), (0, 0, rect2.width, rect2.height))

        if scroll_up:
            coords = (rect2.right - BLOCK_SIZE*3/4, rect2.bottom - rect2.height/2 - BLOCK_SIZE/2, BLOCK_SIZE, BLOCK_SIZE)
            draw_icon(view, self.icons, 'uparrow', coords)
            self.parent.buttons['selitem_uparrow'] = coords
        else: self.parent.buttons['selitem_uparrow'] = None
        if scroll_down: 
            coords = (rect2.right - BLOCK_SIZE*3/4, rect2.bottom - rect2.height/2 + BLOCK_SIZE/2, BLOCK_SIZE, BLOCK_SIZE)
            draw_icon(view, self.icons, 'downarrow', coords)
            self.parent.buttons['selitem_downarrow'] = coords
        else: self.parent.buttons['selitem_downarrow'] = None

            
            
class Listbox(object):
    def __init__(self, name, button, parent, rect, area):
        self.name = name
        self.button = button
        self.rect = rect
        self.scroll = 0
        self.parent = parent
        self.rect = rect
        self.area = area
        
    def draw(self, items, view):
        parent = self.parent
        scroll = self.scroll
        scroll_up = scroll > 0
        scroll_down = False
        b = 0
        sx, sy = parent.skill_label_size
        buttons = parent.parent.buttons
        
        area = self.area
        if area == 'left':
            ax = 0
        elif area == 'right':
            ax = sx*2
        
        rect = self.rect
        left = rect.left + ax
        top = rect.top
        
        title_rect = pygame.Rect(left, top, sx, sy)
        title_label = render('render', html, parent.html_font, title_rect, '<b>%s</b>' % self.name, ANTIALIAS, COLORS_WHITE)
        view.blit(title_label, (title_rect.left, title_rect.top), (0, 0, title_rect.width, title_rect.height))
        
        for n, (text, color, click) in enumerate(items):
            b = n+1
            
            rect2 = pygame.Rect(left, top + b*sy - scroll, sx, sy)
            if rect2.bottom < rect.top: continue
            if rect2.top > rect.bottom: scroll_down = True; continue
            
            label = text
            html_label = render('render', html, parent.html_font, rect2, label, ANTIALIAS, color)
            
            if rect2.top < top+sy:
                overlap = top+sy - rect2.top
                width, height = html_label.get_size()
                html_label = html_label.subsurface((0, overlap, width, height-overlap))
                rect2 = pygame.Rect((rect2.left, rect2.top + overlap, rect2.width, rect2.height - overlap))
            if rect2.bottom > rect.bottom: 
                scroll_down = True
                overlap = rect2.top - rect.top+sy
                width, height = html_label.get_size()
                html_label = html_label.subsurface((0, 0, width, height-overlap))
                rect2 = pygame.Rect((rect2.left, rect2.top, rect2.width, rect2.height - overlap))
                
            result = view.blit(html_label, (rect2.left, rect2.top), (0, 0, rect2.width, rect2.height))
            
            width, height = parent.html_font.size(label)
            if click:
                if not self.button in buttons:
                    buttons[self.button] = []
                buttons[self.button].append((click, (result.left, result.top, width, height)))
            
        if scroll_up:
            coords = (left + sx - BLOCK_SIZE*3/4, top + rect.height/2 - BLOCK_SIZE/2, BLOCK_SIZE, BLOCK_SIZE)
            draw_icon(view, parent.icons, 'uparrow', coords)
            buttons['0scroll_up_%s' % self.name] = coords
        else: buttons['0scroll_up_%s' % self.name] = None
        if scroll_down:
            coords = (left + sx - BLOCK_SIZE*3/4, top + rect.height/2 + BLOCK_SIZE/2, BLOCK_SIZE, BLOCK_SIZE)
            draw_icon(view, parent.icons, 'downarrow', coords)
            buttons['0scroll_down_%s' % self.name] = coords
        else: buttons['0scroll_down_%s' % self.name] = None
