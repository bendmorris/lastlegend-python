import wx
import wx.html
from mmorpg.app.controls import *
from mmorpg.lib.models import *
from mmorpg.assets import upgrade
from game_window import *
import os
import sys

GAME_WINDOW_WIDTH = 0.7
GAME_WINDOW_HEIGHT = 0.8


class App(wx.App):
    def __init__(self):
        wx.App.__init__(self, redirect=False)
        
        mfs = wx.MemoryFSHandler()
        wx.FileSystem_AddHandler(mfs)
        #mfs.AddFile("upgrade.png", upgrade.upgrade.GetImage(), wx.BITMAP_TYPE_PNG)
        for img_file in [f for f in os.listdir("assets/") if f[-4:] == '.png']:
            img = wx.Image('assets/%s' % img_file)
            img.LoadFile('assets/%s' % img_file)
            mfs.AddFile(img_file, img, wx.BITMAP_TYPE_PNG)
        

    def start(self):
        self.frame = Frame(None, -1, "MMORPG Frame", self.character, self.client)
        self.frame.Show()
        self.frame.game_window.SetFocus()
        self.frame.game_window.start_pygame()
        self.frame.Update()
        self.frame.ShowFullScreen(True, style=wx.FULLSCREEN_ALL)
        self.frame.started = True


class Frame(wx.Frame):
    def __init__(self, parent, ID, title, character, client):
        wx.Frame.__init__(self, parent, ID, title,
                          size=wx.GetDisplaySize())
        
        self.DRAW_FPS = DRAW_FPS
        self.EVENT_FPS = EVENT_FPS

        self.character = character
        self.client = client
        self.main_window = self
        self.side_panel_visible = True

        self.SetBackgroundColour(wx.BLACK)
        
        self.vsizer = wx.BoxSizer(wx.VERTICAL)
        self.splitter = wx.BoxSizer(wx.HORIZONTAL)
                        

        width, height = wx.GetDisplaySize()
        self.left_panel = wx.BoxSizer(wx.VERTICAL)
        
        self.game_window = GameWindow(self, int(width*0.7), int(height*0.8))

        self.message_box = MessageBox(self)
        self.history = self.message_box.history
        
        self.left_panel.Add(self.game_window, int(GAME_WINDOW_HEIGHT*100), wx.EXPAND | wx.ALL)
        self.left_panel.Add(self.message_box, int((1-GAME_WINDOW_HEIGHT)*100), wx.EXPAND | wx.ALL)
        
        self.right_panel = wx.BoxSizer(wx.VERTICAL)
        self.character_label = CharacterLabel(self)
        
        self.description_label = HtmlWindow(self)

        self.notebook = Notebook(self)
        
        self.right_panel.Add(self.character_label, 10, wx.EXPAND | wx.ALL)
        self.right_panel.Add(self.notebook, 70, wx.EXPAND | wx.ALL)
        self.right_panel.Add(self.description_label, 20, wx.EXPAND | wx.ALL)
        
        self.splitter.Add(self.left_panel, int(GAME_WINDOW_WIDTH*100), wx.EXPAND | wx.ALL)
        self.splitter.Add(self.right_panel, int((1-GAME_WINDOW_WIDTH)*100), wx.EXPAND | wx.ALL)
        
        def make_transparent(control):
            if hasattr(control, 'SetTransparent'): control.SetTransparent(200)
            if hasattr(control, 'GetChildren'):
                for child in control.GetChildren():
                    make_transparent(child)

        make_transparent(self.left_panel)
            
        
        self.vsizer.Add(self.splitter, 3, wx.EXPAND | wx.ALL, 2)
        
        self.SetSizer(self.vsizer)
        self.vsizer.Layout()
        
        self.Bind(wx.EVT_CLOSE, self.OnClose)
        
        
    def OnClose(self, evt):
        self.draw_timer.Stop()
        wx.Exit()
        
        
    def Upgrade(self, skill_name):
        success = self.do_to_character('upgrade', skill_name)
        
        self.Update()
        
        
    def Update(self):
        self.notebook.Update()
        self.character_label.Update()
        self.description_label.Update()
        self.get_look_summary()
        
        
    def UpdateVitals(self):
        self.character_label.Update()
        
        
    def Message(self, txt):
        self.history.Update(txt)

        
    def StartTimer(self):
        self.DRAW_TIMESPACING = 1000.0 / self.DRAW_FPS
        self.EVENT_TIMESPACING = 1000.0 / self.EVENT_FPS
        
        timer_id = wx.NewId()
        wx.RegisterId(timer_id)
        self.draw_timer = wx.Timer(self, timer_id)
        self.draw_timer.Start(self.DRAW_TIMESPACING, False)
        self.Bind(wx.EVT_TIMER, self.game_window.Update, self.draw_timer, timer_id)
        
        
    def update_vitals(self):
        self.character_label.Update()
        
    def do_to_character(self, *args):
        if self.started:
            self.client.do(*args)
    def do_to_world(self, *args):
        if self.started:
            self.client.execute(*args)
        
    def toggle_sidebar(self):
        width, height = wx.GetDisplaySize()
        height = int(height * GAME_WINDOW_HEIGHT)
        if self.side_panel_visible:
            width = int(width)
            self.splitter.Detach(self.right_panel)
            [c.Show(False) for c in self.right_panel.GetChildren()]
            self.game_window.width = width
        else:
            width = int(width * GAME_WINDOW_WIDTH)
            [c.Show(True) for c in self.right_panel.GetChildren()]
            self.splitter.Add(self.right_panel, int((1-GAME_WINDOW_WIDTH)*100), wx.EXPAND | wx.ALL)            
            self.game_window.width = width
            
        self.splitter.Layout()
        self.game_window.screen = pygame.display.set_mode((width,height), PYGAME_FLAGS)
        self.game_window.map.set_size(4 if self.side_panel_visible else 0)
        self.side_panel_visible = not self.side_panel_visible
        self.message_box.history.ScrollToBottom()
        
    def get_look_summary(self):
        if self.character.looking_at and not self.character.looking_at == self.character.uid:
            self.client.get_look_summary()
        else:
            self.notebook.look_panel.look_window.SetPage('Click on a character to look at them.')
        
    def look_at(self, uid):
        self.do_to_character('look', uid)
        self.character.looking_at = uid
        self.get_look_summary()
        
        self.notebook.SetSelection(1)
        
        #if not self.side_panel_visible:
        #    self.toggle_sidebar()
