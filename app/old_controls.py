import wx
import wx.html
import time
import re
from mmorpg import *
from mmorpg.lib.models import *
from mmorpg.lib.map import *


class Control:
    def setup(self, nofocus=True):
        self.main_window = self.parent.main_window
        self.character = self.main_window.character
        if nofocus: self.Bind(wx.EVT_SET_FOCUS, self.OnGetFocus)
    
    def OnGetFocus(self, event):
        self.main_window.game_window.SetFocus()
        event.Skip()
        
    def Update(self):
        pass


class HtmlWindow(wx.html.HtmlWindow, Control):
    def __init__(self, parent, style=0):
        wx.html.HtmlWindow.__init__(self, parent, id=wx.ID_ANY, style=style|wx.BORDER_SIMPLE)
        self.parent = parent
        self.setup()
        self.SetBackgroundColour(wx.BLACK)
        self.text = ""
        
    def SetPage(self, html):
        self.text = html
        wx.html.HtmlWindow.SetPage(self, "<font color=WHITE>%s</font>" % html)
        self.SetBackgroundColour(wx.BLACK)
        
        
class CharacterLabel(HtmlWindow):
    def __init__(self, parent):
        HtmlWindow.__init__(self, parent, style=wx.html.HW_SCROLLBAR_NEVER)
        self.Update()

    def Update(self):    
        character_text = "<font color=WHITE>%s</font>" % self.character.vitals_summary(main=True)
        self.SetPage(character_text)


class SkillList(wx.HtmlListBox, Control):
    def __init__(self, parent):
        wx.HtmlListBox.__init__(self, parent, -1, style=wx.BORDER_SIMPLE | wx.LB_SINGLE)
        self.parent = parent
        self.setup()
                
        self.SetBackgroundColour(wx.BLACK)
        
        self.Bind(wx.EVT_LISTBOX, self.RefreshMe)
        
        self.Update()
        
    def Update(self):
        count = len([s for s in self.character.defs['all_skills'] 
                     if not s in self.character.race().disabled_skills])
        if self.GetItemCount() != count:
            self.SetItemCount(count)
        self.RefreshAll()
        self.SetBackgroundColour(wx.BLACK)
    
    def OnGetItem(self, index):
        return self.HtmlSummary(index)
        
    def RefreshMe(self, evt):
        self.main_window.description_label.SetPage(self.parent.sel_text)
        self.RefreshAll()        
            
    def HtmlSummary(self, index):
        selected = self.GetSelection() == index
        skill = [s for s in self.character.defs['all_skills']
                 if not s in self.character.race().disabled_skills][index]
        skill = self.character.defs['skills_dict'][skill]
        has_skill = skill.name in self.character.skills
        cost = self.character.upgrade_cost(skill.name)
        desc = ["<font color=%s>" % ('white' if has_skill else 'gray')]
        desc += ["<table border=0 cellpadding=0 cellspacing=0><tr>"]
        desc += ["<td align='center' width='100'>"]
        if cost and self.character.available_xp() >= cost and not skill.name == 'Goodness':
            desc += ["<a href='%s'><img src='memory:upgrade.png' width=25 height=25></a>" % skill.name]
        desc += ["<font size='8'>%s</font>" % self.character[skill.name]]
        desc += ['</td><td>']
        desc += ['<b>']
        desc += [skill.name]
        desc += ['</b>']
        if selected:
            desc2 = ['<b>%s</b>' % skill.name]
            if skill.formula:
                desc2 += ['<br>']
                desc2 += ['<br><i>' + skill.formula.replace('<', '').replace('>', '')]
            desc2 += ['</i><br><br>' + skill.description]
            desc2 += ['<br>']
            self.parent.sel_text = ''.join(desc2)
        if skill.name != 'Goodness':
            if cost:
                desc += [' <i>%s XP to %s</i>' % (cost, 'upgrade' if has_skill else 'learn')]
                if not has_skill:
                    desc += ['<i>, starts at %s</i>' % (self.character.solve_formula(skill.formula) + 1)]
        else:            
            desc += ["<br>" + self.character.goodness_summary()]
        desc += ['</td></tr></table></font>']
        return ''.join(desc)
        
    def OnLinkClicked(self, n, link):
        skill_name = link.GetHref()
        self.main_window.Upgrade(skill_name)
        
        
class XPLabel(HtmlWindow):
    def __init__(self, parent):
        HtmlWindow.__init__(self, parent, style=wx.html.HW_SCROLLBAR_NEVER)
        self.Update()

    def Update(self):
        text = "<font color=WHITE>"
        text += self.character.fancy_summary()
        text += "<br><br>Total XP: %s" % self.character.spent_xp
        next = self.character.next_level()
        if next:
            text += "; Next Level: %s" % next
        if not self.character.penalty:
            text += "<br><b>Available XP:</b> %s" % self.character.xp
        else:
            text += "<br><font color=RED><b>XP Penalty:</b> %s (movement -%s%%)</font>" % (
                self.character.penalty, int(100. * self.character.penalty / self.character.max_penalty() / 4))
        text += "<br><br>Damage: %s, Range: %s" % (self.character['Damage'], self.character['Range'])
        text += "</font>"
        self.SetPage(text)
        
    def OnLinkClicked(self, link):
        character = self.character
        shapeshift(self, character, link, self.main_window.Update)
        
        
class ItemLabel(HtmlWindow):
    def __init__(self, parent):
        HtmlWindow.__init__(self, parent, style=wx.html.HW_SCROLLBAR_NEVER)
        self.Update()

    def Update(self):
        character = self.character
        text = "<font color=WHITE><b>Equipment</b><br><br><table border=0>"
        for slot in character.equipment_slots():
            slot_name = slot
            if slot == 'Main Hand':
                if ((character['Two-Handed'] and not character.equipped('Off Hand'))
                    or (character.equipped('Off Hand') == character.equipped('Main Hand'))):
                    slot_name = 'Both Hands'
            elif slot == 'Off Hand' and character.equipped('Off Hand') == character.equipped('Main Hand'):
                continue
            text += "<tr><td><i>%s</i></td><td>%s</td></tr>" % (slot_name, character.equipped(slot))
        text += "</table>"
        text += "<br><br>Damage: %s, Range: %s" % (character['Damage'], character['Range'])
        text += "<br><br>Gold: %s<br>" % character.gold
        
        burden = self.character.burden()
        carrying = self.character['Carrying']
        if burden > carrying: text += '<font color=RED>'
        text += "Burden: %s/%s" % (self.character.burden() , self.character['Carrying'])
        if burden > carrying: 
            text += ' (movement -%s%%)</font>' % int(100 - 100./min(float(burden)/carrying, 2))
        text += "</font>"
        self.SetPage(text)


class ItemList(wx.HtmlListBox, Control):
    def __init__(self, parent):
        wx.HtmlListBox.__init__(self, parent, -1, style=wx.BORDER_SIMPLE | wx.LB_SINGLE)
        self.parent = parent
        self.setup()
                
        self.SetBackgroundColour(wx.BLACK)
        
        self.Bind(wx.EVT_LISTBOX, self.RefreshMe)
        
        self.Update()
        
    def Update(self):
        count = len(self.character.items)
        if self.GetItemCount() != count:
            self.SetItemCount(count)
        self.RefreshAll()
    
    def OnGetItem(self, index):
        return self.HtmlSummary(index)
        
    def RefreshMe(self, evt):
        self.main_window.description_label.SetPage(self.parent.sel_text)
        self.RefreshAll()
            
    def HtmlSummary(self, index):
        selected = self.GetSelection() == index
        item = self.character.items[index]
        equipped = item.equipped
        for_sale = item.for_sale
        qty = item.quantity
        item = item.item
        desc = ["<font color=white>%s" % item.name]
        if qty > 1:
            desc += [" (%s)" % qty]
        desc += ["</font>"]
        if equipped:
            desc = ["<b>%s</b>" % ''.join(desc)]
        if selected:
            droppable = True
            if 'Usable' in item['Type'].split(','):
                if self.character.solve_formula(item['Requires']):
                    desc += [" (<a href='Use'>Use</a>)"]
                else:
                    desc += [" (Can't use)"]
            if 'Equippable' in item['Type'].split(','):
                if equipped:
                    desc += [" (<a href='Unequip'>Unequip</a>)"]
                    droppable = False
                else:
                    if self.character.can_equip(item):
                        desc += [" (<a href='Equip'>Equip</a>)"]
                    else:
                        desc += [" (Can't equip)"]
            if droppable: desc += [" (<a href='Drop'>Drop</a>)"]
            desc2 = ['<b>%s</b><br>%s<br><table border=0>' % (item.name, item.description)]
            for name, value in item.attributes:
                if name[0] == name[0].upper():
                    formatted_value = value
                    replacements = re.findall("<[^<>=]*>", formatted_value)
                    for replacement in replacements:
                        formatted_value = formatted_value.replace(replacement, replacement[1:-1])
                    desc2 += ['<tr><td><i>%s</i></td><td>%s</td></tr>' % (name, formatted_value)]
            desc2 += ["</table>"]
            self.parent.sel_text = ''.join(desc2)
        return ''.join(desc)
        
    def OnLinkClicked(self, n, link):
        character = self.character
        action = link.GetHref()
        item = character.items[self.GetSelection()]
        
        if action == 'Equip':
            self.main_window.do_to_character('equip', item.item.name)
        elif action == 'Unequip':
            self.main_window.do_to_character('unequip', item.item.name)
        elif action == 'Drop':            
            #current_map = character.world.maps_dict[character.location.map]
            #position = character.location.position
            #if not [o for o in current_map.get_objs(position[0], position[1]) if o.type == item.item['drop_img']]:
            #    new_obj = Obj(item.item['drop_img'], respawn=False)
            #    current_map.add_obj(position, new_obj)
            #    character.lose_item(item.item.name)
            pass
            
        elif action == 'Use':
            self.main_window.do_to_character('use_item', item.item)
            
        self.main_window.Update()


class Panel(wx.Panel, Control):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent=parent, id=wx.ID_ANY, style=wx.BORDER_SIMPLE)
        self.parent = parent
        self.setup()
        
        self.sel_text = ""
        
        
class HistoryWindow(HtmlWindow):
    def __init__(self, parent):
        HtmlWindow.__init__(self, parent)
        self.history = ""
        self.Update("<b>Welcome, %s</b>!" % self.character.name)
        self.Update("Type <b>!help</b> for help or <b>!quit</b> to quit.<br>")
        
    def Update(self, msgs):
        if not isinstance(msgs, list):
            msgs = [msgs]
        self.history += ''.join(["<br>&lt;%s&gt; %s" % (time.strftime('%I:%M:%S %p'), text) for text in msgs])
        lines = self.history.split('<br>')[-MAX_HISTORY_LENGTH:]
        self.history = '<br>'.join(lines)
        self.SetPage(self.history)        
        self.ScrollToBottom()
        
    def ScrollToBottom(self):
        wx.Sleep(0.1)
        wx.Yield()
        self.Scroll(-1, self.GetClientSize()[0])
        
        
class TalkCtrl(wx.TextCtrl, Control):
    def __init__(self, parent):
        wx.TextCtrl.__init__(self, parent, style=wx.TE_PROCESS_ENTER)
        self.parent = parent
        self.setup(nofocus=False)
        self.SetBackgroundColour(wx.BLACK)
        self.SetForegroundColour(wx.WHITE)
        self.input_history = []
        self.history_position = -1
        self.Bind(wx.EVT_SET_FOCUS, self.OnFocus)
        self.Bind(wx.EVT_KEY_DOWN, self.OnKeyDown)
        
    def OnFocus(self, e):
        self.main_window.do_to_character('set_moving', False)
        self.main_window.do_to_character('run', False)
        
    def OnKeyDown(self, e):
        me = self.character
        do = self.main_window.do_to_character
        #chars = self.world.get_characters[me.location.map].copy()
        key = e.GetKeyCode()
        
        if key == wx.WXK_RETURN:
            message = self.GetValue().strip()
            if message:
                self.input_history = [message] + self.input_history
                self.input_history = self.input_history[:50]
                self.history_position = -1
                if message[0] == '@':
                    send_to = message.split(' ')[0][1:]
                    if send_to.lower() == self.character.name.lower():
                        self.main_window.Message("<i>Talking to ourselves, are we?</i>")
                    else:
                        msg = ' '.join(message.split(' ')[1:])
                        self.main_window.do_to_world('tell', self.character.uid, send_to, msg)
                elif message[0] == '!':
                    command = message.split(' ')[0]
                    arg = ' '.join(message.split(' ')[1:])
                    if command in ('!upgrade', '!u'):
                        if arg:
                            self.main_window.Upgrade(arg)
                        else:
                            self.main_window.Message("Usage: !upgrade (skill name)")
                    elif command in ('!quit', '!q'):
                        self.main_window.OnClose(None)
                    elif command in ('!help', '!h'):
                        msg = """!help<br><b>Controls</b>:<br><br><ul>
<li><i>Arrows</i>: Move</li>
<li><i>Shift+Arrow</i>: Run (requires stamina)</li>
<li><i>Click</i>: Look</li>
<li><i>Right Click</i>: Attack</li>
<li><i>Tab</i>: Select next target</li>
<li><i>Delete</i>: Attack target</i>
<li><i>Escape</i>: Stop moving/attacking</li>
<li><i>f</i>: Fly or stop flying</li>
</ul><br><br>
<b>Commands:</b><br><br><ul>
<li><i>!upgrade skill_name</i>: Spend XP to increase a skill.</li>
<li><i>!quit</i>: Log out.</li>
</ul>
                        """
                        self.parent.history.Update(msg)
                    elif command in ('!eval', 'v'):
                        self.main_window.Message('Result: %s' % (eval(arg),))
                    elif command in ('!exec', '!!'):
                        exec(arg)
                    elif command in ('!x', '!xp'):
                        self.character.xp += 100000
                        self.main_window.character_label.Update()
                    elif command in ('!god',):
                        self.main_window.do_to_character('god_mode')
                    elif command in ('!fps',):
                        self.main_window.game_window.map.show_fps = not self.main_window.game_window.map.show_fps
                    else:
                        self.main_window.Message("Unknown command: %s" % command)
                else:
                    self.main_window.do_to_world('broadcast', self.character.uid, message)
            self.SetValue("")
            self.main_window.game_window.SetFocus()
        elif key == wx.WXK_ESCAPE:
            self.main_window.game_window.SetFocus()
        elif key in (wx.WXK_DOWN, wx.WXK_UP):
            direction = 1 if key == wx.WXK_UP else -1
            if 0 <= (self.history_position + direction) < len(self.input_history):
                self.history_position += direction
                self.SetValue(self.input_history[self.history_position])
                self.SetInsertionPoint(len(self.GetValue()))
            elif 0 <= self.history_position < len(self.input_history):
                self.history_position += direction
                self.SetValue("")
        else:
            e.Skip()


class MessageBox(Panel):
    def __init__(self, parent):
        Panel.__init__(self, parent)
        
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        
        self.history = HistoryWindow(self)
        self.talk_ctrl = TalkCtrl(self)
        
        self.sizer.Add(self.history, 80, wx.EXPAND | wx.ALL)
        self.sizer.Add(self.talk_ctrl, 20, wx.EXPAND | wx.ALL)
        
        self.SetSizer(self.sizer)
        
        
def shapeshift(self, character, link, update):
    command = link.GetHref()
    if command[:11] == 'Shapeshift:':
        new_race = command[11:]
        self.main_window.do_to_character('shapeshift', new_race)
        update()
    

class LookHtmlWindow(HtmlWindow):
    def OnLinkClicked(self, link):
        character = self.character
        shapeshift(self, character, link, self.main_window.Update)
                

class LookPanel(Panel):
    def __init__(self, parent):
        Panel.__init__(self, parent)
        
        self.sizer = wx.BoxSizer(wx.VERTICAL)        
        self.look_window = LookHtmlWindow(self)
        
        self.sizer.Add(self.look_window, 1, wx.EXPAND | wx.ALL)
        self.SetSizer(self.sizer)        
        self.looking_at = None
            
    def Update(self):
        pass


class SkillsPanel(Panel):
    def __init__(self, parent):
        Panel.__init__(self, parent)

        self.sizer = wx.BoxSizer(wx.VERTICAL)
 
        #self.ability_list = AbilityList(self)
        self.xp_label = XPLabel(self)
        self.skill_list = SkillList(self)
        
        self.sizer.Add(self.xp_label, 40, wx.EXPAND | wx.ALL)
        #self.sizer.Add(self.ability_list, 20, wx.EXPAND | wx.ALL)
        self.sizer.Add(self.skill_list, 60, wx.EXPAND | wx.ALL)
        
        self.SetSizer(self.sizer)
        
    def Update(self):
        #self.ability_list.Update()
        self.xp_label.Update()
        self.skill_list.Update()
        
        
class BuySellPanel(Panel):
    def __init__(self, parent):
        Panel.__init__(self, parent)
    
    
class ItemsPanel(Panel):
    def __init__(self, parent):
        Panel.__init__(self, parent)

        self.sizer = wx.BoxSizer(wx.VERTICAL)
 
        self.items_label = ItemLabel(self)
        self.items_list = ItemList(self)
        
        self.sizer.Add(self.items_label, 50, wx.EXPAND | wx.ALL)
        self.sizer.Add(self.items_list, 50, wx.EXPAND | wx.ALL)
        self.SetSizer(self.sizer)
        
    def Update(self):
        self.items_label.Update()
        self.items_list.Update()
        
        
class MapPanel(Panel):
    def __init__(self, parent):
        Panel.__init__(self, parent)
        
        
class Notebook(wx.Notebook, Control):
    def __init__(self, parent):    
        wx.Notebook.__init__(self, parent, id=wx.ID_ANY, style=
                             wx.NB_TOP | wx.BORDER_SIMPLE
                             #wx.BK_TOP 
                             #wx.BK_BOTTOM
                             #wx.BK_LEFT
                             #wx.BK_RIGHT
                             )
                             
        self.SetBackgroundColour((50,50,50))
        self.SetForegroundColour(wx.WHITE)

        self.parent = parent
        self.setup()

        self.look_panel = LookPanel(self)
        self.skills_panel = SkillsPanel(self)
        self.items_panel = ItemsPanel(self)
        self.buysell_panel = BuySellPanel(self)
        self.map_panel = MapPanel(self)
        
        self.AddPage(self.skills_panel, 'Me')
        self.AddPage(self.look_panel, 'Look')
        self.AddPage(self.items_panel, 'Items')
        self.AddPage(self.buysell_panel, 'Trade')
        self.AddPage(self.map_panel, 'Map')
        
        self.Bind(wx.EVT_NOTEBOOK_PAGE_CHANGED, self.TurnPage)

    def TurnPage(self, evt):
        self.Update(evt.GetSelection())
        evt.Skip()
        
    def Update(self, page_num=None):
        if not page_num is None:
            page = self.GetPage(page_num)
        else:
            page = self.GetCurrentPage()
            
        page.Update()
        
        sel_text = page.sel_text
        if not page_num is None or sel_text != self.main_window.description_label.text:
            self.main_window.description_label.SetPage(sel_text)
        
        
class NewCharacterDialog(wx.Dialog):    
    def __init__(self, parent, title, defs):
        wx.Dialog.__init__(self,
                           parent=parent, 
                           title=title, size=(250, 200))

        panel = wx.Panel(self)
        vbox = wx.BoxSizer(wx.VERTICAL)

        sb = wx.StaticBox(panel, label='New Character')
        sbs = wx.StaticBoxSizer(sb, orient=wx.VERTICAL)
        
        self.defs = defs
        
        self.name = wx.TextCtrl(panel, -1, "Ben")
        self.race_selection = wx.Choice(panel, choices=defs['playable_races'])
        self.race_selection.SetSelection(0)
        self.var_selection = wx.Choice(panel, choices=defs['all_titles'])
        self.var_selection.SetSelection(0)
        
        sbs.Add(self.name)
        sbs.Add(self.race_selection)
        sbs.Add(self.var_selection)
                
        panel.SetSizer(sbs)
       
        hbox2 = wx.BoxSizer(wx.HORIZONTAL)
        okButton = wx.Button(self, label='Ok')
        closeButton = wx.Button(self, label='Close')
        hbox2.Add(okButton)
        hbox2.Add(closeButton, flag=wx.LEFT, border=5)

        vbox.Add(panel, proportion=1, flag=wx.ALL|wx.EXPAND, border=5)
        vbox.Add(hbox2, flag= wx.ALIGN_CENTER|wx.TOP|wx.BOTTOM, border=10)

        self.SetSizer(vbox)
        vbox.Layout()
        
        okButton.Bind(wx.EVT_BUTTON, self.OnClose)
        closeButton.Bind(wx.EVT_BUTTON, self.OnClose)
        self.race_selection.Bind(wx.EVT_CHOICE, self.ValidateVars)
        

    def OnClose(self, e):
        self.Close()
        
    def ValidateVars(self, e):
        self.var_selection.Clear()
        race = self.defs['races_dict'][self.race_selection.GetStringSelection()]
        for var in self.defs['all_titles']:
            name, skills, items = self.defs['titles_dict'][var]
            if not [s for s in skills if s in race.disabled_skills]:
                self.var_selection.Append(name)
        self.var_selection.SetSelection(0)
