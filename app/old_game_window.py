from mmorpg import *
from mmorpg.lib.models import *
from mmorpg.app.controls import *
from mmorpg.app.map_window import MapWindow
import os
import pygame
import pygame.locals
import pygame.constants as pc
import sys
import time


directions = {wx.WXK_UP: 'u',
              wx.WXK_DOWN: 'd',
              wx.WXK_LEFT: 'l',
              wx.WXK_RIGHT: 'r',
              ord('w'): 'u',
              ord('s'): 'd',
              ord('a'): 'l',
              ord('d'): 'r',
              ord('W'): 'u',
              ord('S'): 'd',
              ord('A'): 'l',
              ord('D'): 'r',
              }

pygame_keys = {i:i for i in range(64, 123)}
pygame_keys.update({
                    pc.K_UP: wx.WXK_UP,
                    pc.K_DOWN: wx.WXK_DOWN,
                    pc.K_RIGHT: wx.WXK_RIGHT,
                    pc.K_LEFT: wx.WXK_LEFT,
                    pc.K_LSHIFT: wx.WXK_SHIFT,
                    pc.K_RSHIFT: wx.WXK_SHIFT,
                    pc.K_DELETE: wx.WXK_DELETE,
                    pc.K_INSERT: wx.WXK_INSERT,
                    pc.K_TAB: wx.WXK_TAB,
               })
joy_btns = {
            0: wx.WXK_ESCAPE,
            1: wx.WXK_SHIFT,
            2: wx.WXK_DELETE,
            7: ord('F'),
            8: wx.WXK_INSERT,
            }

class GameWindow(wx.Window):
    def __init__(self, parent, width, height):
        wx.Window.__init__(self, parent, wx.ID_ANY, style=wx.WANTS_CHARS)
        self.parent = parent
        self.event_updates = self.draw_updates = 0
        self.main_window = parent.main_window
        self.started = False
        
        self.character = parent.character
        self.client = parent.client
        
        self.last_time = None
        
        self.width, self.height = width, height
        self.size = self.GetSizeTuple()
        
        self.joy = wx.Joystick()
        self.joy.SetCapture(self)
                
        self.Bind(wx.EVT_PAINT, self.OnPaint)
        self.Bind(wx.EVT_SIZE, self.OnSize)
        #for evt in [wx.EVT_KEY_UP, wx.EVT_CHAR, wx.EVT_KEY_DOWN]:
            #self.Bind(evt, self.SkipEvent)
        self.Bind(wx.EVT_KEY_UP, self.OnKeyUp)
        self.Bind(wx.EVT_CHAR, self.OnKeyDown)
        self.Bind(wx.EVT_KEY_DOWN, self.OnKeyDown)
        self.Bind(wx.EVT_LEFT_DOWN, self.OnClick)
        self.Bind(wx.EVT_RIGHT_DOWN, self.OnClick)
        self.Bind(wx.EVT_JOY_MOVE, self.JoyMove)
        self.Bind(wx.EVT_JOY_BUTTON_DOWN, self.JoyDown)
        self.Bind(wx.EVT_JOY_BUTTON_UP, self.JoyUp)
        
        self.f_pressed = False
        self.ins_pressed = False
        self.moving = ''
        self.last_moving_sent = ''
        

    #def CommandToKey(self, event):
        #evt = wx.KeyEvent(wx.wxEVT_KEY_DOWN)
        #evt.m_keyCode = event.GetId()
        #self.OnKeyDown(evt)
        
        
    def start_pygame(self):
        self.hwnd = self.GetHandle()
        if sys.platform == 'win32':
            os.environ['SDL_VIDEODRIVER'] = 'windib'
        os.environ['SDL_WINDOWID'] = str(self.hwnd)
        
        import pygame
        
        pygame.display.init()
        self.screen = pygame.display.set_mode((self.width,self.height), PYGAME_FLAGS)
        self.screen.set_alpha(None)
        pygame.event.set_allowed([pygame.MOUSEBUTTONDOWN, pygame.KEYDOWN, pygame.KEYUP])
                
        self.draw_last_time = time.time()
        self.main_window.StartTimer()

        self.map = MapWindow(self)

        self.started = True
        

    def SkipEvent(self, evt):
        evt.Skip()
        
        
    def Update(self, event):
        character = self.character
        fps = 1. / (time.time() - self.draw_last_time)
        self.draw_updates += 1. / fps
        self.draw_fps = fps
        self.draw_last_time = time.time()

        if hasattr(self, 'map'):
            self.map.scroll -= TILE_SCROLL * (BASE_FPS / fps)
            if self.map.scroll <= 0:
                self.map.scroll += TILE_SIZE

        # check pygame for events
        pygame_evt = pygame.event.poll()
        while pygame_evt:
            print pygame_evt
            if pygame_evt.type == pygame.KEYDOWN: 
                if pygame_evt.key in pygame_keys.keys():
                    sim_event = wx.KeyEvent(wx.wxEVT_KEY_DOWN)
                    sim_event.m_keyCode = pygame_keys[pygame_evt.key]
                    self.OnKeyDown(sim_event)
            elif pygame_evt.type == pygame.KEYUP:
                if pygame_evt.key in pygame_keys.keys():
                    sim_event = wx.KeyEvent(wx.wxEVT_KEY_UP)
                    sim_event.m_keyCode = pygame_keys[pygame_evt.key]
                    self.OnKeyUp(sim_event)
            elif pygame_evt.type == pygame.MOUSEBUTTONDOWN:
                if pygame_evt.button == 1: btn = wx.wxEVT_LEFT_DOWN
                else: btn = wx.wxEVT_RIGHT_DOWN
                sim_event = wx.MouseEvent(btn)
                #sim_event.Button = wx.MOUSE_BTN_LEFT
                sim_event.m_x, sim_event.m_y = pygame_evt.pos
                self.OnClick(sim_event)
            pygame_evt = pygame.event.poll()
                        
        self.Redraw()
        
        if self.draw_updates >= 5 or character.gui_update:
            self.main_window.Update()
            self.draw_updates = 0
            self.character.gui_update = False
            self.character.update_vitals = False
        elif character.update_vitals:
            self.main_window.update_vitals()
            self.character.update_vitals = False
            
        messages = self.character.read_messages()        
        if messages:
            self.main_window.Message(messages)
            
        if self.moving != self.last_moving_sent:
            self.do_to_character('set_moving', self.moving)
            self.last_moving_sent = self.moving
            
        #if looking_at and self.character.abs_distance(looking_at) > self.map.character.vision() * 2:
        #    self.character.look(False)
            
        #wx.Yield()
        
        
    def OnKeyDown(self, event):
        key = event.GetKeyCode()
        character = self.character
        
        new_move_dir = None
        
        if key == wx.WXK_RETURN:
            # put cursor in message window
            self.main_window.message_box.talk_ctrl.SetFocus()
            new_move_dir = False
        elif key in directions.keys():
            # direction key; start moving character
            if not self.moving:
                new_move_dir = directions[key]
            else:
                if len(self.moving) == 1 and not directions[key] in self.moving:
                    new_move_dir = directions[key] + self.moving
        elif key in (wx.WXK_ESCAPE, ord('X'), ord('x'), ord('Z'), ord('z')):
            new_move_dir = False
            self.do_to_character('run', False)
            self.do_to_character('attack', False)
        elif key == wx.WXK_SHIFT:
            # start character running
            self.do_to_character('run')
        elif key == wx.WXK_DELETE:
            looking_at = self.character.looking_at
            if looking_at and not looking_at == character.uid:
                self.do_to_character('attack', looking_at)
        elif key == wx.WXK_INSERT:
            if not self.ins_pressed:
                self.main_window.toggle_sidebar()
                self.ins_pressed = True
        elif key == wx.WXK_TAB:
            self.do_to_character('next_selection', character.running)
        elif key in (ord('F'), ord('f')):
            if not self.f_pressed:
                if character.flying:
                    self.do_to_character('fly', False)
                else:
                    self.do_to_character('fly')
                self.f_pressed = True
        elif key <= 128:
            # if an unprocessed key event has an ASCII key code, send it to the message window
            self.main_window.message_box.talk_ctrl.SetFocus()
            self.main_window.message_box.talk_ctrl.SetInsertionPoint(len(self.main_window.message_box.talk_ctrl.GetValue()))
            if event.GetEventType() == wx.EVT_KEY_DOWN.typeId: 
                event.Skip()
            else:
                #wx.PostEvent(self.main_window.message_box.talk_ctrl, event)
                if 32 <= key <= 126:
                    self.main_window.message_box.talk_ctrl.WriteText(chr(key))
                elif key in (8,):
                    self.main_window.message_box.talk_ctrl.EmulateKeyPress(event)
                character.moving = False
        else: event.Skip()
        
        if not new_move_dir is None:
            self.moving = new_move_dir
        
                    
    def OnKeyUp(self, event):
        key = event.GetKeyCode()
        character = self.character
        
        rm_move_dir = None

        if key in directions.keys():
            # stop moving in a particular direction
            try:
                rm_move_dir = directions[key]
            except Exception as e: print e; raise; pass
        elif key == wx.WXK_SHIFT:
            # stop running
            self.do_to_character('run', False)
        elif key == wx.WXK_INSERT:
            self.ins_pressed = False
        elif key == ord('F'):
            self.f_pressed = False
            
        try:
            event.skip()
        except: pass
        
        if not rm_move_dir is None and self.moving:
            self.moving = self.moving.replace(rm_move_dir, '')
            self.do_to_character('set_moving', rm_move_dir, True)
        
        
    def OnClick(self, event):
        character = self.character
        characters = self.character.peers
        
        x, y = event.GetPositionTuple()
        x, y = (int(float(x) / self.width * self.map.SCREEN_SIZE[0]*TILE_SIZE), 
                int(float(y) / self.height * self.map.SCREEN_SIZE[1]*TILE_SIZE))
        btn = event.GetButton()
        
        main_x, main_y = character.position        
        main_ox, main_oy = character.offset
        cx, cy = [n / 2 for n in self.map.SCREEN_SIZE]

        # dictionary of squared distances of each character from click
        distances = {}
        for c in characters:
            if not hasattr(c, 'dead') or not c.dead:
                px, py = c.position
                ox, oy = c.abs_offset()
                px = (cx + px - main_x) * TILE_SIZE + (ox - main_ox) + (TILE_SIZE / 2)
                py = (cy + py - main_y) * TILE_SIZE + (oy - main_oy) + (TILE_SIZE / 2)
                            
                distances[c] = (px-x)**2 + (py-y)**2
            
        sorted_distances = sorted(distances.items(),
                                  key=lambda x: x[1])
                                  
        if sorted_distances:
            closest_char, distance = sorted_distances[0]
            max_distance = TILE_SIZE * 2
            if distance <= max_distance ** 2:
                if btn == wx.MOUSE_BTN_LEFT:
                    # left click
                    self.main_window.look_at(closest_char.uid)
                    
                elif btn == wx.MOUSE_BTN_RIGHT:
                    # right click
                    if closest_char != character:
                        self.main_window.look_at(closest_char.uid)
                        self.do_to_character('attack', closest_char.uid)
            else:
                # right click on nothing
                if btn == wx.MOUSE_BTN_RIGHT:
                    self.do_to_character('attack', False)
        else:
            # right click on nothing
            if btn == wx.MOUSE_BTN_RIGHT:
                self.do_to_character('attack', False)            
                

    def JoyMove(self, event):
        x,y = event.GetPosition()
        move_dir = ''
        
        if x: move_dir += 'r' if x > 0 else 'l'
        if y: move_dir += 'd' if y > 0 else 'u'
        
        self.moving = move_dir


    def JoyDown(self, event):
        btn = event.GetButtonChange()
        if btn in joy_btns:
            sim_event = wx.KeyEvent(wx.wxEVT_KEY_DOWN)
            sim_event.m_keyCode = joy_btns[btn]
            self.OnKeyDown(sim_event)
        elif btn == 4:
            self.do_to_character('next_selection', False)
        elif btn == 5:
            self.do_to_character('next_selection', True)
        

    def JoyUp(self, event):
        btn = event.GetButtonChange()
        if btn in joy_btns:
            sim_event = wx.KeyEvent(wx.wxEVT_KEY_UP)
            sim_event.m_keyCode = joy_btns[btn]
            self.OnKeyUp(sim_event)


    def Redraw(self):
        if self.started:
            drawn = self.map.draw(self.character)
            pygame.display.update()
            

    def OnPaint(self, event):
        self.Redraw()
        event.Skip()
        

    def OnSize(self, event):
        self.size = self.GetSizeTuple()
        event.Skip()
        

    def Kill(self, event):
        # Make sure Pygame can't be asked to redraw /before/ quitting by unbinding all methods which
        # call the Redraw() method
        # (Otherwise wx seems to call Draw between quitting Pygame and destroying the frame)
        self.Unbind(event = wx.EVT_PAINT, handler = self.OnPaint)
        self.main_window.Unbind(event = wx.EVT_TIMER, handler = self.Update, source = self.main_window.timer)
        event.Skip()
        

    def do_to_character(self, *args):
        self.client.do(*args)
