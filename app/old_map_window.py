from mmorpg.tiles.tiles import CHARSET, TILESET, OBJSET
from mmorpg import *
from mmorpg.lib.models import *
import numpy as np
import pygame
import time



def invert(src, c=0):
    src = src.copy()
    arr = pygame.surfarray.pixels3d(src)
    #vmax = np.vectorize(lambda n: min(n, 255))
    r,g,b = arr[:,:,0], arr[:,:,1], arr[:,:,2]
    abs_c = (r + g + b) + 128
    arr[:,:,c] = abs_c
    if not c == 0: arr[:,:,0] = 24
    if not c == 1: arr[:,:,1] = 24
    if not c == 2: arr[:,:,2] = 24
    return src
    
#def should_flash(uid, chars):
#    try: return [c for c in chars
#                 if hasattr(c, 'attacking')
#                 and c.attacking == uid 
#                 and hasattr(c, 'attack_animation')
#                 and c.attack_animation
#                 and 0.1 < c.attack_animation['progress'] < 0.2]
#    except: return False
    
class MapWindow:
    def __init__(self, parent):
        self.parent = parent
        self.main_window = parent.main_window
        self.show_fps = False
        
        self.set_size()
        
        pygame.font.init()
        
        #self.font = pygame.font.Font(None, 24)
        #self.font.set_bold(True)
        
        self.fixed_font = pygame.font.Font(MONO_FONT, 15)
        self.fixed_font.set_bold(True)
        self.med_font = self.big_font = pygame.font.Font(MONO_FONT, 24)
        self.big_font = pygame.font.Font(MONO_FONT, 36)
        self.font = self.fixed_font
    
        tiles = 'tiles/tiles.png'
        chars = 'tiles/chars.png'
        objs = 'tiles/objs.png'
        x2 = pygame.transform.scale2x
        def smoothscale(surface, scale): 
            t = pygame.transform.smoothscale if SMOOTH else pygame.transform.scale
            return pygame.transform.scale(surface, 
                    (int(surface.get_width() * scale), int(surface.get_height() * scale)))
        def smooth(surface):
            return smoothscale(x2(surface), 0.5)
                    
        self.tiles = pygame.image.load(tiles).convert()
        self.chars = pygame.image.load(chars).convert_alpha()
        self.objs = pygame.image.load(objs).convert_alpha()

        self.inv_tiles = invert(self.tiles)
        self.inv_chars = invert(self.chars)
        self.inv_objs = invert(self.objs)
        self.psn_chars = invert(self.chars, c=1)

        tilesets = ['tiles', 'chars', 'objs', 
                    'inv_tiles', 'inv_chars', 'inv_objs',
                    'psn_chars',]
        
        need_to_scale = float(SCALE)
        smoothed = False
        doubles = DOUBLES
        while need_to_scale != 1:
            if need_to_scale > 1 and doubles > 0:
                for t in tilesets:
                    setattr(self, t, x2(getattr(self, t)))
                need_to_scale /= 2
                doubles -= 1
            else:
                for t in tilesets:
                    setattr(self, t, smoothscale(getattr(self, t), need_to_scale))
                need_to_scale = 1
        
        while doubles > 0: 
            for t in tilesets:
                setattr(self, t, smooth(getattr(self, t)))
            doubles -= 1
            
        
        for s in ['chars', 'inv_chars', 'psn_chars']:
            setattr(self, s, (s, getattr(self, s)))

        self.scroll = 0
        self.last_position = True
        self.font_renders = {}
        

    def set_size(self, expand=0):
        self.width, self.height = self.parent.width, self.parent.height
        self.SCREEN_SIZE = (SCREEN_X + expand, SCREEN_Y)
        self.center_x, self.center_y = [n / 2 - TILE_SIZE / 2 for n in (self.SCREEN_SIZE[0]*TILE_SIZE, self.SCREEN_SIZE[1]*TILE_SIZE)]
        (self.global_offset_x, 
         self.global_offset_y) = [n1 - (n2 / 2 * TILE_SIZE)
                                  for n1, n2 in zip([self.center_x, self.center_y], self.SCREEN_SIZE)]
        self.buffer_surface = pygame.Surface((self.SCREEN_SIZE[0]*TILE_SIZE, self.SCREEN_SIZE[1]*TILE_SIZE))


    def draw(self, main_character):
        drawn = []
        
        view = self.buffer_surface
        dest = self.parent.screen
        current_map = main_character.map
        characters = [main_character.sprite()] + main_character.peers
        x, y = main_character.position
        main_offset = main_character.real_offset()
        
        global_offset_x, global_offset_y = self.global_offset_x, self.global_offset_y
        
        # draw tiles
        i_range = [x - self.SCREEN_SIZE[0] / 2 + wx for wx in xrange(-2, self.SCREEN_SIZE[0] + 2)]
        j_range = [y - self.SCREEN_SIZE[1] / 2 + wy for wy in xrange(-2, self.SCREEN_SIZE[1] + 2)]
        
        tiles, objs = ((self.inv_tiles, self.inv_objs) 
                       if (main_character.invert and float(main_character.hp)/main_character['Vitality'] < 0.33)
                       or main_character.dead
                       else (self.tiles, self.objs))

        if True:#self.last_position and (x, y, main_offset) != self.last_position:
            wx = -2
            for i in i_range:
                wy = -2
                for j in j_range:                
                    tile = current_map.get_tile(i, j)
                    tile_def = TILESET[tile]
                    if tile_def.scroll:                        
                        speed = tile_def.scroll_spd
                        scroll = (self.scroll * speed if tile_def.scroll else 0) % TILE_SIZE
                        cx, cy = tile_def.x, (tile_def.y + scroll)
                        gx, gy = (SCALE + cx * BUFFERED_TILE + (ORIG_TILE_BUFFER * SCALE), 
                                  SCALE + cy * BUFFERED_TILE + (ORIG_TILE_BUFFER * SCALE))
                    else:
                        gx, gy = (SCALE + tile_def.x * BUFFERED_TILE, SCALE + tile_def.y * BUFFERED_TILE)
                                        
                    drawn.append(view.blit(tiles, (wx * TILE_SIZE + main_offset[0] + global_offset_x, 
                                           wy * TILE_SIZE + main_offset[1] + global_offset_y), 
                                          (gx, gy, TILE_SIZE, TILE_SIZE)))
                    #if distance(x, y, i, j) > main_character.vision():
                    #    view.blit(self.objs, (wx * TILE_SIZE + main_offset[0], 
                    #                          wy * TILE_SIZE + main_offset[1]), 
                    #                         (0, BUFFERED_TILE, TILE_SIZE, TILE_SIZE))

                    wy += 1
                    
                wx += 1
            self.last_position = (x, y, main_offset)
            
        # draw chars
        texts = []

        chars_to_draw = []
        target_lines = []
        wpns_to_draw = []
        for character in characters:
            if isinstance(character, Character):
                character = character.sprite()
            if not isinstance(character, Sprite):
                break
                
            px,py = character.position
            if i_range[0] <= px <= i_range[-1] and j_range[0] <= py <= j_range[-1]:
                if px == np.inf or py == np.inf: break
                dx, dy = onscreen(character, main_character, self.SCREEN_SIZE)
                dx += global_offset_x
                dy += global_offset_y
                direction = 'dead' if hasattr(character, 'dead') and character.dead else character.direction[0]
                step = character.step if hasattr(character, 'step') else False
                offset = character.offset if hasattr(character, 'offset') else (0,0)
                
                race, colors = character.pic
                try:
                    char = (CHARSET[race], colors)
                except:
                    char = (CHARSET['Human'], colors)
                                
                flying = character.flying if hasattr(character, 'flying') else False
                if direction == 'dead': flying = False
                if flying:
                    drawn.append(draw_obj(view, self.objs, 'shadow', (dx, dy + (1 + smoothstep(character.fly_offset)) * TILE_SIZE)))
                
                if hasattr(character, 'damage_numbers') and character.damage_numbers:
                    for n, dur in character.damage_numbers:
                        if isinstance(n, int):
                            desc, color = (str(abs(n)), COLORS_WHITE) if n < 0 else ('+%s' % n, COLORS_GREEN)
                        else:
                            desc, color = n, COLORS_WHITE
                            #desc, color = (n, COLORS_BLUE if 'XP' in n else COLORS_WHITE)
                        text = self.med_font.render(desc, True, color)
                        rect = text.get_rect()
                        rect.centerx = dx + (TILE_SIZE / 2)
                        rect.centery = dy - (TILE_SIZE * (1-dur) * 2)
                        texts.append((text, rect))
                
                if hasattr(character, 'attack_animation') and character.attack_animation:
                    if 'image' in character.attack_animation:
                        animation = character.attack_animation
                        image, animation_type, progress, direction = [animation[x] if x in animation else 0
                                                           for x in 
                                                           ['image', 'animation_type', 'progress', 'direction']]
                        mx, my = move_directions[direction]
                        if animation_type == 'projectile':
                            progress = max((progress - 0.4) / 0.6, 0)
                            (src_x, src_y), (dest_x, dest_y) = animation['src_pos'], animation['dest_pos']
                            src_fly, dest_fly = animation['src_fly'], animation['dest_fly']
                            move_vector = (dest_x - src_x, dest_y - src_y)
                            move_dist = (move_vector[0]**2 + move_vector[1]**2)**0.5
                            proj_z = move_dist / 2
                            proj_z *= TILE_SIZE
                            proj_x, proj_y = (src_x + move_vector[0]*(1-progress), src_y + move_vector[1]*(1-progress))
                            proj_x, proj_y = onscreen((proj_x, proj_y), main_character, self.SCREEN_SIZE)
                            proj_x += global_offset_x
                            proj_y += global_offset_y
                            drawn.append(draw_obj(view, self.objs, 'smshadow', (proj_x, proj_y)))
                            if src_fly or dest_fly:
                                src_z = 1.5 if src_fly else 0
                                dest_z = 1.5 if dest_fly else 0
                                proj_y -= (progress*src_z + (1-progress)*dest_z) * TILE_SIZE
                            if progress <= 0.5: proj_y -= smoothprojectile(progress * 2) * proj_z
                            else: proj_y -= smoothprojectile((1-(progress - 0.5)*2)) * proj_z
                            wpns_to_draw.append((view, self.objs, image, (proj_x, proj_y), direction))
                        else:
                            dist = smoothstep(1-max((progress-.9)*4, 0))
                            wpns_to_draw.append((view, self.objs, image, (dx + mx*TILE_SIZE*dist, dy + my*TILE_SIZE*dist), direction))
                
                if hasattr(character, 'invert') and character.invert:
                    chars = self.inv_chars
                elif hasattr(character, 'poison') and character.poison:
                    chars = self.chars if int(self.scroll)%4 else self.psn_chars
                else: 
                    chars = self.chars
                
                chars_to_draw.append((flying, direction=='dead', dy, (view, chars, char, (dx, dy), direction, step)))

                if not direction=='dead':
                    if hasattr(character, 'attacking') and character.attacking:
                        target = character.current_target(main_character.peers)

                        if target:
                            tx, ty = onscreen(target, main_character, self.SCREEN_SIZE)
                        
                        #target_lines.append((view, 
                        #                    COLORS_RED, 
                        #                    (dx+TILE_SIZE/2, dy+TILE_SIZE*3/4), 
                        #                    (tx+TILE_SIZE/2, ty+TILE_SIZE/2), 2))
                    
                        #if self.character.attacking and target == character:
                        #    color = COLORS_RED
                        #else: color = COLORS_WHITE
                        summary = character.attack_meter
                        if main_character.looking_at == character.uid:
                            summary = '*%s*' % summary
                        text = self.fixed_font.render(summary, True, COLORS_WHITE)
                        rect = text.get_rect()
                        rect.centerx = dx + (TILE_SIZE / 2)
                        rect.centery = dy - TILE_SIZE / 4
                    else:
                        summary = character.short_summary
                        if main_character.looking_at == character.uid:
                            summary = '*%s*' % summary
                        text = self.font.render(summary, True, character.rgb)
                        rect = text.get_rect()
                        rect.centerx = dx + (TILE_SIZE / 2)
                        rect.centery = dy - TILE_SIZE / 4
                        
                    texts.append((text, rect))

        chars_to_draw.sort(key = lambda c: (1 if c[0] else 0, c[2]))
        #for line in target_lines:
        #    pygame.draw.line(*line)
        
        for _, _, _, args in [c for c in chars_to_draw if c[1]]:
            drawn.append(draw_char(*args))
        for _, _, _, args in [c for c in chars_to_draw if not c[0] and not c[1]]:
            drawn.append(draw_char(*args))
            

        # draw objs
        wx = -2
        for i in i_range:
            wy = -2
            for j in j_range:
                if (i,j) in current_map.objs:                
                    for obj in current_map.objs[(i,j)]:
                        obj_def = OBJSET[obj.type]
                        speed = obj_def.scroll_spd
                        scroll = (self.scroll * speed if obj_def.scroll else 0) % TILE_SIZE

                        ta, tb, h = obj_def.x, obj_def.y, obj_def.height

                        cx, cy = (SCALE + ta * BUFFERED_TILE, SCALE + tb * BUFFERED_TILE - (h * TILE_SIZE))
                        drawn.append(view.blit(objs, 
                                               (wx * TILE_SIZE + main_offset[0] + global_offset_x, 
                                                wy * TILE_SIZE + main_offset[1] - (h * TILE_SIZE) + global_offset_y),
                                     (cx, cy+scroll, TILE_SIZE, TILE_SIZE * (1 + h))))
                wy += 1
            wx += 1


        if self.show_fps:
            text = self.fixed_font.render('FPS=%s' % (round(self.parent.draw_fps,2)), True, COLORS_WHITE)
            rect = text.get_rect()
            rect.centerx = 100
            rect.centery = 10
            texts.append((text, rect))
        if not main_character.dead:
            summary = main_character.short_summary()
            sum_len = len(summary)
            hp_len = len(str(main_character['Vitality']))
            stam_len = len(str(main_character['Stamina']))
            hp_color = color_from_amt(float(main_character.hp) / main_character['Vitality'], html=False)
            stamina_color = color_from_amt(float(main_character.stamina) / main_character['Stamina'], html=False)
            vital_label = '  %s  HP: %s/%s  Stamina: %s/%s  ' % (summary,
                                                                 ' '*hp_len, 
                                                                 main_character['Vitality'],
                                                                 ' '*stam_len, 
                                                                 main_character['Stamina'])
            hp_label = ' '*(sum_len + 8 + (hp_len-len(str(main_character.hp)))) + '%s' % (main_character.hp)
            hp_label += ' '*(len(vital_label)-len(hp_label))
            stamina_label = ' '*(sum_len+20+hp_len*2 + (stam_len-len(str(main_character.stamina)))) + '%s' % (main_character.stamina)
            stamina_label += ' '*(len(vital_label)-len(stamina_label))

            for (label, color) in [(vital_label, (COLORS_WHITE, COLORS_BLACK)),
                                    (hp_label, (hp_color,)),
                                    (stamina_label, (stamina_color,)),
                                    ]:
                if (label,color) in self.font_renders:
                    text = self.font_renders[(label,color)]
                else:
                    text = self.fixed_font.render(label, True, *color)
                    self.font_renders[(label,color)] = text
                rect = text.get_rect()
                rect.centerx = (self.center_x)
                rect.top = 0
                texts.append((text, rect))
        else:
            time_left = int(main_character.spawn_time - main_character.dead)
            text = self.big_font.render('Respawn in: %3.f seconds' % time_left, True, COLORS_WHITE)
            rect = text.get_rect()
            rect.centerx = (self.center_x)
            rect.top = TILE_SIZE
            texts.append((text, rect))

        # draw flying chars and character labels
        for _, _, _, args in [c for c in chars_to_draw if c[0] and not c[1]]:
            drawn.append(draw_char(*args))
        for text, rect in texts:
            drawn.append(view.blit(text, rect))
        for args in wpns_to_draw:
            drawn.append(draw_wpn(*args))
            
        sx, sy = self.width, self.height
        view = pygame.transform.smoothscale(view, (sx,sy))
        #sx, sy = self.SCREEN_SIZE[0]*TILE_SIZE, self.SCREEN_SIZE[1]*TILE_SIZE
        dest.blit(view, (0,0), (0,0,sx,sy))
