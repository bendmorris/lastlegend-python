class Ability:
    def __init__(self, name):
        self.name = name
        self.attributes = []
        
    def __contains__(self, x):
        if not hasattr(self, 'attribute_set'):
            self.attribute_set = set([attribute[0] for attribute in self.attributes])
        return x in self.attribute_set
        
    def __getitem__(self, x):    
        for (name, value) in self.attributes:
            if name == x: return value
        if x == 'drop_img': return 'chest'
        if x == 'Requires': return '1'
        return '0'
        
    def types(self):
        if not hasattr(self, '_types'):
            self._types = set([s.strip() for s in self['type'].split(',')])
        return self._types
