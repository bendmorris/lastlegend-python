import math
try:
    import numpypy as np
except ImportError:
    import numpy as np
import random
import re
import uuid
from mmorpg import *
from item import Item, ItemCopy
from sprite import Sprite, sprite_values, resolve_offset_p
from funcs import *
from ability import Ability
from inspect import isgeneratorfunction


no_cache = set(['changes', 'damage_numbers'])

def ShellCharacter(data_dict):
    return Character(None,None,None,None,None,data_dict=data_dict)

class Character(object):
    def __init__(self, name, colors, race, title, defs, start_map=None,
                 xp=1000, skills=None, items=None, abilities=None, spawn=False, 
                 spawn_time=30, spent_xp=0, data_dict=None, hostile=False,
                 **kwargs):
        self.changes = {}
        self._last_values = {}
        self.defs = defs
        if self.defs: self.race = self.defs['races_dict'][race]
        self._FPS = BASE_FPS
        self._sprites = {}
        self._peers = None
        self._last_peers = set()
        
        if data_dict:
            self.__dict__ = data_dict
            return
            
        self.uid = str(uuid.uuid4().hex)[:12]
        self.uncache()
        
        self.name = name
        if colors == -1: 
            self.colors = random.randint(0,5)
            self.random_color = True
        else: 
            self.colors = colors
            self.random_color = False
        
        self.items = []
        self.music_playing = None
        self.force_msg = 0
        
        self.dead = False
        self._busy = False
        self.spawn_time = spawn_time
        
        self._marks = {}
        self.buffs = {}
        
        self.xp = xp
        self.spent_xp = spent_xp
        self.gold = 0
        self._damage_dict = {}
        self._damage_time = {}
        self._kills = set()
        
        self.poison = 0
        self.penalty = 0
        self.hostile = hostile
        
        self._title = title
        self.charclass = self.defs['titles_dict'][title]
                
        if not isinstance(skills, dict):
            self.skills = {}
            skills = {}
        else:
            self.skills = skills

        self.skill_upgrades = {key:0 for key in self.skills}
        self.skill_upgrades.update({key:0 for key in self.charclass.start_skills})
        self.skill_upgrades.update({key:0 for key in self.race.start_skills})

        self.use_skills = {}
        
        for key, value in skills.items():
            if key != 'Goodness':
                for i in xrange(value):                
                    cost = self.upgrade_cost(key)
                    self.skills[key] = value
                    if not key in self.skill_upgrades:
                        self.skill_upgrades[key] = 0
                    self.skill_upgrades[key] += 1
                    self.spent_xp += cost
                    self.uncache_stats()
            
        for skill in self.race.start_skills:
            if not skill in self.skills:
                self.skills[skill] = 0
        for key, value in self.charclass.start_skills.items():
            if not key in self.skills:
                self.skills[key] = 0
            self.skills[key] += value

        self._run_effort = 0
        self._walk_effort = 0
        self._fly_effort = 0
        self._swim_effort = 0
        self._poison_effort = 0
        self._regen = 0
        self.damage_numbers = []
        self.attacking = False
        self.attacking_with = None
        self._already_attacking_with = False
        self._want_to_attack_with = False
        self.attack_wait = 0
        self.attack_animation = []
        self.attack_progress = 0
        self._targets = []
        self._attacked_this_turn = False
        self.need_uncache = False
        self._check_for_hostility = False
        
        self.switches = {}
        self.region = None
        self.shopping = None
        self._need_to_talk_to = None
        self._talking_to = None
        self.choices = None
        
        self._path = None
        self._path_dest = None
        
        self.hp = 0
        self.stamina = 0
        self.messages = []
        
        if items is None:
            starting_items = self.charclass.starting_items + self.race.starting_items
        else:
            starting_items = items
        for item in starting_items:            
            if '*' in item:
                item, n = item.split('*')[0].strip(), int(item.split('*')[1].strip())
            else: n=1
            this_item = self.defs['items_dict'][item]
            for i in xrange(n): self.get_item(item)
            if 'Equippable' in this_item.types():
                self.equip(item)
                
        self.abilities = set()
        if abilities is None:
            starting_abilities = self.charclass.abilities
        else:
            starting_abilities = abilities
        for ability in starting_abilities:
            self.learn_ability(ability)
        
        self.hp = self['Vitality']
        self.stamina = self['Stamina']
        
        self.looking_at = self.uid
        
        self.map_name = start_map
        self.tiles = []
        self.objs = {}
        self.position = None
        self._zone = None
        self.direction = 'd'
        self._moving = None
        self._running = False
        self.flying = False
        self._need_to_fly = None
        self.in_water = False
        self.step = False
        self.offset = (0,0)
        self.invert = False
        self.set_peers([])
        
        if spawn:
            self.spawn()
            
        self.vx, self.vy = SCREEN_X/2 + 2, SCREEN_Y/2 + 2
        
        for key, value in kwargs.items():
            self.__setattr__(key, value)
        self.changes = {}
        
        self.speed = self.move_speed(False) * self.move_multiplier()

    def __getRange__(self): return self.range()
    def __getDamage__(self): return self.equipment_attr('Damage')
    def __getLevel__(self): return self.level()
    def __getVitality__(self): return max(HP_B + self['Toughness'] * HP_M, HP_MIN)
    def __getStamina__(self): return max(STAM_B + self['Endurance'] * STAM_M, STAM_MIN)
    def __getStaminaCost__(self, use_ability=False):
        return self.ability_cost(self.attacking_ability(use_ability=use_ability))
    special_gets = {
                    'Range': __getRange__,
                    'Damage': __getDamage__,
                    'Level': __getLevel__,
                    'Vitality': __getVitality__,
                    'Stamina': __getStamina__,
                    'StaminaCost': __getStaminaCost__,
                    }
    def __getitem__(self, x):
        try: return self.special_gets[x](self)
        except: pass
        stat = self.stat(x)
        stat += self.equipment_attr(x)
        aa = self.attacking_ability()
        if isinstance(aa, Ability):
            stat += self.solve_formula(aa[x])
        if x != 'Goodness': stat = max(stat, 1 if x in self.skill_upgrades else 0)
        return stat


    def ability_cost(self, ability):
        if isinstance(ability, basestring): 
            try: ability = self.defs['abilities_dict'][ability]
            except: 
                try: ability = self.defs['items_dict'][ability]
                except: ability=None
        try: cost = self.solve_formula(ability['Stamina Cost'])
        except: cost = 0
        return max(0, cost + self.equipment_attr('Attack Cost'))

    def __setattr__(self, x, value):
        object.__setattr__(self, x, value)
        if (not x in no_cache) and not x[0]=='_': self.record_changes(x)
    def setattr(self, x, value):
        return self.__setattr__(x, value)

    def __repr__(self):
        return self.summary()
        
    def sprite(self, *args, **kwargs):
        return Sprite(self, *args, **kwargs)

    def sprite_for(self, name):
        if not name in self._sprites:
            self._sprites[name] = self.sprite()
        return self._sprites[name]


    def uncache(self):
        self.uncache_stats()
        self.uncache_buffs()
    def uncache_stats(self):
        self._cached_stats = {}
        self._cached_formulas = {}
        self._cached_equip_attrs = {}
        self._cached_upgrade_costs = {}
        self._cached_base_dmg = None
        self._cached_dps = None
        self._cached_level = None
        self._cached_quality = None
        self._sorted_abilities = None
        self._sorted_skills = None
        self._learnable_skills = None
        self._cached_attacking_ability = None
        self.uncache_move_speed()
        self.need_uncache = True
        try: self.update_equipment()
        except: pass
    def uncache_move_speed(self):
        self._cached_move_speed = None
        self._cached_attack_speed = None
        self._cached_move_multiplier = None
        self.need_uncache = True
    def uncache_buffs(self):
        self._cached_buffs = {}
        self.need_uncache = True
        
    def cache_stat(self, name, value):
        self._cached_stats[name] = value
    def cache_formula(self, name, value):
        self._cached_formulas[name] = value
    def cache_equip_attr(self, name, value):
        self._cached_equip_attrs[name] = value
    def cache_buff_attr(self, name, value):
        self._cached_buffs[name] = value
    def cache_upgrade_cost(self, name, value):
        self._cached_upgrade_costs[name] = value
    def cache_base_dmg(self, value):
        self._cached_base_dmg = value
    def cache_move_speed(self, value):
        self._cached_move_speed = value
        self.record_changes('speed')
    def cache_level(self, value):
        self._cached_level = value


    def abs_distance(self, target):
        try:
            x, y = self.position
            tx, ty = target.position
            return max(abs(x-tx), abs(y-ty))
        except:
            return np.inf
            
    def after_attack(self, world):
        self._targets = []
        self._already_attacking_with = False
        if not self._want_to_attack_with is False:
            self.set_attacking_with(self._want_to_attack_with)
            self._want_to_attack_with = False

    def attack(self, target, response=False, force=True):
        self._cached_move_multiplier = None

        if (self.dead and target) or self.uid == target: target = False
        if target:
            if not target == self.attacking:
                self.attack_wait = 0
                self.attacking = target
                if not self.hostile and not force: self._check_for_hostility = True
                target = self.current_target()
        else:
            self.attacking = False
        self._attacked_this_turn = False
        self._already_attacked_with = False
        self._want_to_attack_with = False

    def attack_events(self, world):
        # attacking
        target = self.current_target(world=world)
        if self._check_for_hostility:
            self._check_for_hostility=False
            if not target.hostile and target.attacking != self.uid:
                self.attack(False)
                return
        if not target: self.attack(False); return
        if target.dead: self.attack(False); return
        if not self.can_see(target): self.attack(False); return

        wpn = self.attacking_ability()

        if self._attacked_this_turn or self.distance(target) <= self.solve_formula(self.range()):
            self.direction = self.face(world, target)
            # charge up, then attack
            self.attack_wait += self.attack_speed()
            if self.attack_wait >= ATTACK_THRESHOLD*3 / 4 and not self.attack_animation and not self._attacked_this_turn:
                self._attacked_this_turn = True
                splash = self.solve_formula(wpn['Splash'])
                if splash: self._targets = world.get_targets(self.uid, target, splash)
                else: self._targets = [target]
                self._already_attacking_with = self.attacking_with
                for target in self._targets:
                    self.weapon_animation(target)
            if self.attack_wait >= ATTACK_THRESHOLD:
                self.attack_wait = ATTACK_THRESHOLD
                if not self.attack_animation or self.attack_progress < 0.25:
                    self.attack_wait = 0
                    self._attacked_this_turn = False
                    
                    # attack
                    first_attack = True
                    for target in self._targets:
                        success = self.canhit(target)
                        
                        if success:
                            dmg = self.damage(target)
                            if isinstance(dmg, tuple): dmg, critical, counter = dmg
                            else: dmg, critical, counter = dmg, False, False
                            dmg = max(random_number(dmg), 1)
                            if not first_attack: dmg = max(dmg/2, 1)
                            
                            psn = self.canpoison(target)
                            knockback = self.canknockback(target)
                            
                            dmg_msg = []
                            if self.direction == target.direction and self['Backstabbing']:
                                dmg_msg += ["from behind "]
                            dmg_msg += ["for <b>%s %sdamage</b>" % (dmg, 'critical ' if critical else '')]
                            if psn:
                                dmg_msg += [" and <b>%s poison damage</b>" % psn]
                                
                            dmg_msg = ''.join(dmg_msg)
                            
                            if wpn.name == 'unarmed': with_txt = ''
                            else: with_txt = ' with <b>%s</b>' % wpn.name
                            self.message("<font color=GREEN>You attack %s%s %s!</font>" % (target.name, with_txt, dmg_msg))
                            target.message("<font color=RED>%s attacks you%s %s!</font>" % (self.name, with_txt, dmg_msg))

                            if counter:
                                self.message('%s counters!' % target.name)
                                target.message('You counter!')
                            
                            if knockback: target.push(world, self.direction)
                            
                            target.poison_dmg(psn, self)
                            
                            target_hp = target.hp
                            target.change_hp(dmg * -1, damager=self.uid, critical=critical)
                            self.record_damage(target.uid, max(dmg, target_hp))

                            # gain point for skill use
                            if wpn['Skill']:
                                skill = wpn['Skill'][1:-1]
                                if self[skill] < self.level():
                                    skills_changed = False
                                    
                                    if not skill in self.use_skills:
                                        self.use_skills[skill] = 0
                                    if not skill in self.skills:
                                        self.skills[skill] = 0
                                        skills_changed = True
                                        
                                    self.use_skills[skill] += 1
                                    
                                    if self.use_skills[skill] >= USE_SKILL_THRESHOLD:
                                        self.use_skills[skill] -= USE_SKILL_THRESHOLD
                                        self.skills[skill] += 1
                                        self.message('<i>** %s skill is now level %s! **</i>' % (skill, 
                                                     self.skills[skill]))
                                        skills_changed = True
                                    
                                    if skills_changed: 
                                        self.record_changes('skills')
                                        self.uncache_stats()
                                    
                                    self.record_changes('use_skills')
                                    
                        else:
                            self.message("You missed %s." % target.name)
                            target.message("%s missed you." % self.name)
                            target.damage_number('miss')
                        
                        first_attack = False
                            
                    target = self._targets[0]
                    if not 'Wild' in target.__class__.__name__ and target.region and not target.region in target._marks:
                        self._marks[target.region] = MARK_LENGTH
                        
                    cost = self.__getStaminaCost__(True) * -1
                    self.change_stamina(cost)
                    
                    self.after_attack(world)
        else:
            # move toward the target
            self.chase(world, target)

    def attack_meter(self):
        rate = float(self.attack_wait) / ATTACK_THRESHOLD
        bars = int(round(5*rate))
        return '_'*bars + ' '*(5-bars)
        #return meter(float(self.attack_wait) / ATTACK_THRESHOLD, color=False, html=False, max_length=4)

    def attack_speed(self):
        if self._cached_attack_speed: spd = self._cached_attack_speed
        else:
            spd = 1
            wpn = self.attacking_ability(use_ability=True)
            if 'Weapon' in wpn.types():
                spd += (self['Flurry'] / 100.)
            elif 'Magic' in wpn.types():
                spd += (self['Quickcast'] / 100.)
            self._cached_attack_speed = spd
        return self.move_speed() * spd
        
    def attacking_ability(self, use_ability=True):
        try: 
            if use_ability and self._cached_attacking_ability: return self._cached_attacking_ability
        except: pass
        
        wpn = self.weapon()
        if self._already_attacking_with and use_ability:
            result = self.defs['abilities_dict'][self._already_attacking_with]
        elif self.attacking_with and use_ability:
            result = self.defs['abilities_dict'][self.attacking_with]
        elif wpn: 
            result = self.defs['items_dict'][self.weapon()]
        else:
            result = self.defs['abilities_dict']['unarmed']
        if use_ability: self._cached_attacking_ability = result
        return result

    def available_xp(self):
        if self.penalty:
            return 0
        else:
            return self.xp

    def being_attacked(self, target):
        self.message(target.summary() + ' is attacking you.')

    def buff(self, name, duration):
        self.buffs[name] = duration
        self.record_changes('buffs')
        self.uncache_buffs()
        
    def buff_value(self, name):
        try: return self._cached_buffs[name]
        except KeyError:
            value = 0
            for buff in self.buffs:
                buff_def = self.defs['magic_dict'][buff]
                if name in buff_def:
                    value += buff_def[name]
            self.cache_buff_attr(name, value)
            return value
            
    def buy(self, item_name):
        self.shopping.buy(item_name, self)
    def sell(self, item_name):
        self.shopping.sell(item_name, self)

    def can_equip(self, item):
        if not 'Equippable' in item.types(): return False
        for equip_type in item.types():
            if equip_type in self.race.disabled_skills: return False
        for slot in [this_slot.strip() for this_slot in item['Space'].split(', ')]:
            if slot in self.race.disabled_skills: return False
        return self.solve_formula(item['Requires'])

    def canhit(self, target):
        magic_attack = False
        
        wpn = self.attacking_ability()
            
        precision = max(1, self.solve_formula(wpn['Skill'])) * 5
        enemy_dodge = 0
        if self['Backstabbing'] and self.direction == target.direction:
            precision += self['Backstabbing']
                
        if 'Weapon' in wpn.types():
            precision += float(self['Precision'])
            enemy_dodge += target['Evasion']
        if 'Magic' in wpn.types():
            precision += float(self['Magic'])
            enemy_dodge += target['Spell Resist']
            
        try:
            total_precision = (precision / (enemy_dodge + precision))
        except:
            total_precision = 0.5
            
        return random.random() ** 2 < total_precision
        
    def canknockback(self, target):
        if not self['Knockback']: return 0
        knockback = self['Knockback'] / 2.
        if self['Bloodlust']:
            knockback += self['Bloodlust'] * (self['Vitality'] - self.hp) / self['Vitality']
        guard = target['Defense']
        prob = knockback / (knockback + guard)
        return random.random() < prob
        
    def can_move(self, world, direction, change_direction=True, from_pos=None):
        if from_pos: x, y = from_pos
        else: x, y = self.position

        mx, my = move_directions[direction]
        dest_pos = (x+mx, y+my)
        dx, dy = dest_pos

        if self.choices: return False, dest_pos
        
        if hasattr(self, 'clipping') and self.clipping:
            if change_direction: self.direction = direction
            return dest_pos, (mx, my)
        
        if self.offset == (0,0):
            if change_direction: self.direction = direction

            if self.get_map(world).movable(dx, dy, world.get_peers(self.uid), char=self):
                return dest_pos, (mx, my)
        return False, dest_pos

    def canpoison(self, target):
        if not self['Poisoning']: return 0
        return random_number(self['Poisoning']*10./target['Defense'])

    def can_see(self, target, mult=1):
        return self.x_distance(target) <= self.vx*mult and self.y_distance(target) <= self.vy*mult

    def change_hp(self, amt, damager=None, critical=False):
        if amt:
            self.damage_number(amt, critical=critical)
            self.hp += amt
            if self.hp < 0: self.hp = 0
            if self.hp > self['Vitality']: self.hp = self['Vitality']
            if amt < 0:
                self.invert = INVERT_LENGTH * max(((1-(float(self.hp)/self['Vitality']))**2),0.25) * (2 if critical else 1)
                if damager:
                    self.record_damage(damager, amt*-1)
            if self.hp == 0:
                self.die()
                return True
            return False
        
    def change_stamina(self, amt):
        self.stamina += amt
        if self.stamina <= 0:
            self.stamina = 0
            self.run(False)
        if self.stamina > self['Stamina']: self.stamina = self['Stamina']
        if self.attacking_with:
            cost = self.__getStaminaCost__(True)
            if self.stamina < cost:
                self.set_attacking_with(None)
        self.uncache_move_speed()

    def chase(self, world, target):
        #self.face(world, target)
        if not isinstance(target, tuple): target = target.position
        path = self.find_path(world, target)
        self._path = path
        self._path_dest = target
        try: 
            move_dir = self._path.pop()
            self.move(world, move_dir)
        except: pass
                
    def clone(self):
        return self

    def current_target(self, world=None):
        if self.attacking:
            if world:
                try:
                    return self.get_map(world).characters[self.attacking]
                except Exception as e: return None
            else:
                return self.peer_by_uid(self.attacking)
        return None

    def damage(self, target=None, use_ability=True):
        guard = 0
        bonus = 0
        critical = False
        counter = False
        wpn = self.attacking_ability(use_ability=use_ability)

        if not self._cached_base_dmg is None:
            dmg = self._cached_base_dmg
        else:
            dmg = self.solve_formula(wpn['Damage'])
            skill = wpn['Skill']
            dmg += self.solve_formula(skill)
            if wpn:
                bonus += (self.solve_formula(wpn['Base']))
                if 'Weapon' in wpn.types():
                    bonus += self['Fighting']
                if 'Magic' in wpn.types():
                    bonus += self['Magic']
                    
            dmg = (dmg * bonus) ** 0.5
            
            if wpn and not wpn.name == 'unarmed' and (not self.equipped('Off Hand') 
                                                      or 'Two-Handed' in wpn['Requires']):
                dmg += min(self['Two-Handed'], dmg)
                        
            dmg = max(dmg, 1)
            
            self.cache_base_dmg(dmg)
                
        if target:
            if wpn:
                if 'Weapon' in wpn.types(): 
                    guard += target['Defense']
                    guard -= random.randint(0, self['Piercing'])
                if 'Magic' in wpn.types(): guard += target['Spell Resist']

                crit_chance = ((self['Critical'] + (self['Bloodlust'] * (self['Vitality'] - self.hp) / self['Vitality'] if self['Bloodlust'] else 1)) / (target['Defense']*target.level()))
                crit_chance = max(min(crit_chance, 0.25), 0.01)

                if random.random() < crit_chance: 
                    dmg *= 1+random.random()
                    critical = True
                else: critical = False

                if target.attacking and target['Counter']:
                    counter_chance = ((target['Counter'] + (target['Bloodlust'] * (target['Vitality'] - target.hp) / target['Vitality'] if target['Bloodlust'] else 1)) / (self['Defense']*self.level()))
                    counter_chance = max(min(counter_chance, 0.25), 0.01)

                    if random.random() < counter_chance:
                        target.attack_wait += (random.random() * ATTACK_THRESHOLD)
                        counter = True
                
            else: guard += target['Defense']
            
            if self['Bloodlust']:
                wrath = self['Bloodlust'] * float(self['Vitality'] - self.hp) / self['Vitality']
                guard -= wrath
            if self['Backstabbing'] and self.direction == target.direction:
                guard -= self['Backstabbing']                
            if self['Swarming']:
                guard -= self['Swarming'] * (len(target._damage_dict) - 1)
            if self['Justice']:
                if self.enmity(target):
                    guard -= self['Justice']
            if self['Betrayal']:
                if not self.enmity(target):
                    guard -= self['Betrayal']
                    
            element = wpn['Element']
            if element:
                resist_skill, weak_skill = '_Resist %s' % element, '_Weak to %s' % element
                if target[resist_skill]:
                    dmg /= 1 + target[resist_skill]
                if target[weak_skill]:
                    dmg *= 1 + target[resist_skill]

        dmg = max(dmg, 1)
        guard = max(guard, 1) * 2
            
        if guard:
            dmg -= dmg * (float(guard) / (dmg + guard))
            
        final = max(round((dmg+1) ** 0.9, 1), 1)
        return final if not (critical or counter) else (final, critical, counter)

    def damage_number(self, x, critical=False):
        if not x in (0, 1):
            label = x if not critical else '%s!' % x
            self.damage_numbers.append(label)
            
            self.record_changes('damage_numbers')

            for sprite in self._sprites.values():
                if not 'damage_numbers' in sprite.changes:
                    sprite.changes['damage_numbers'] = []
                sprite.changes['damage_numbers'].append(label)


    def die(self, gold=None):
        self.message("You died!")
        
        self.poison = 0
        self.hp = 0
        self.stamina = 0
        
        self.flying = False
        self.run(False)
        self.dead = self.spawn_time
        
        self._xp_to_distribute = random_number(self.xp_from())
        if gold: self._gold_to_distribute = gold
        else: self._gold_to_distribute = random_number(self.gold / 3)
        self.gold -= self._gold_to_distribute
        
        max_penalty = self.max_penalty()
        penalty = random_number(max_penalty*2./5)
        self.penalty += penalty
        if self.penalty > max_penalty:
            self.penalty = max_penalty
        msg = "Your XP penalty is now %s. Respawning in %s seconds." % (self.penalty, self.spawn_time)
        if self._gold_to_distribute:
            msg = ("You lost %s gold. " % self._gold_to_distribute) + msg            
        self.message(msg)
        
        self.uncache()
        self._attacked_this_turn = False
        self._already_attacking_with = False

        self.music = 'dead_dance'
        
    def distance(self, target):
        try:
            x, y = self.position
            tx, ty = target.position

            extra = 0
            if target.flying and not self.flying: extra += 1
            if target.swimming() and not self.swimming(): extra += 1
            return distance(x, y, tx, ty, extra)
        except:
            return np.inf
    def x_distance(self, target):
        return abs(self.position[0] - target.position[0])
    def y_distance(self, target):
        return abs(self.position[1] - target.position[1])

    def do_event(self, event):
        for action, contents in event.events:
            action = action.lower()
            if action == 'message':
                self.message(contents)
            elif action == 'exec':
                # this is unsafe
                exec(contents.strip())
                
    def dps(self):
        if self._cached_dps:
            return self._cached_dps
        else:
            self._cached_dps = round(self.damage() * self.attack_speed() * self._FPS / ATTACK_THRESHOLD, 1)
            return self._cached_dps

    def enmity(self, char):
        if self['Goodness'] == 0 or char['Goodness'] == 0: return False
        return (self['Goodness'] < 0) == (char['Goodness'] < 0)
        
    def equip(self, item_name):
        for item in self.items:
            if item.item.name == item_name:
                requirement = self.solve_formula(item.item['Requires'])
                if self.can_equip(item.item):
                    self.unequip_slot(item.item['Space'])
                    item.equipped = True
                    self.sort_items()
                    self.uncache_stats()
                    self.rebase_hp_stamina()
                    self.changes['items'] = True
                    self.message('You equipped <b>%s</b>.' % item.item.name)
                    return True
        return False

    def equipment_attr(self, attr):
        try:
            return self._cached_equip_attrs[attr]
        except KeyError:
            value = 0
            for item in self.items:
                if item.equipped:
                    value += self.solve_formula(item.item[attr], True)
            self.cache_equip_attr(attr, value)
            return value

    def equipment_slots(self):
        slots = set()
        for item in self.items:
            if item.equipped:
                for slot in [this_slot.strip() for this_slot in item.item['Space'].split(', ')]:
                    slots.add(slot)
        default_order = ["Both Hands",
                         "Main Hand", 
                         "Off Hand", 
                         "Shield",
                         "Armor",
                         "Head",
                         "Hands",
                         "Feet"]
        ordered_slots = []
        for order in default_order:
            if order == 'Both Hands':
                main_hand = self.equipped('Main Hand')
                off_hand = self.equipped('Off Hand')
                if (self['Two-Handed'] and main_hand and not off_hand) or (main_hand and main_hand == off_hand):
                    ordered_slots.append(order)
                    if 'Main Hand' in slots: slots.remove('Main Hand')
                    if 'Off Hand' in slots: slots.remove('Off Hand')
            elif order in slots:
                ordered_slots.append(order)
                slots.remove(order)
        return ordered_slots + sorted(list(slots))
                
    def equipped(self, slot):
        if slot == 'Both Hands':
            main_hand = self.equipped('Main Hand')
            off_hand = self.equipped('Off Hand')
            return main_hand if (self['Two-Handed'] and main_hand and not off_hand) or (main_hand and main_hand == off_hand) else ""
        else:    
            for item in self.items:
                if item.equipped and slot in [this_slot.strip() for this_slot in item.item['Space'].split(', ')]:
                    return item.item.name
        return ""

        
    def face(self, world, target):
        if isinstance(target, tuple):
            tx, ty = target
        else:
            tx, ty = target.position

        x, y = self.position        
        dx, dy = abs(x-tx), abs(y-ty)
        
        if dx == dy == 0:
            if target.flying and not self.flying:
                return 'u'
            else:
                return 'd'
            return
        
        if dx > dy:
            d1 = 'r' if x < tx else 'l'
            d2 = 'd' if y < ty else ('u' if y > ty else None)
            d2 = random.choice([d2, d2])
        else:
            d1 = 'd' if y < ty else 'u'
            d2 = 'r' if x < tx else ('l' if x > tx else None)
            d2 = random.choice([d2, d2])

        if dx+dy > self['Range']:
            choices = []
            if self.can_move(world, d1)[0]: choices += [d1, d1]
            if not d2 is None and self.can_move(world, d2)[0]: choices.append(d2)
            
            if choices: return random.choice(choices)
            return d1
        else:
            return d1
            
    def turn(self, target):
        x, y = self.position
        tx, ty = target.position
        if abs(x-tx) > abs(y-ty):
            if x > tx: self.direction = 'l'
            else: self.direction = 'r'
        else:
            if y > ty: self.direction = 'u'
            else: self.direction = 'd'

    def fancy_summary(self, looking=None):
        return "<b>%s</b><br>Lv%s %s %s" % (self.name, self.level(), self.race.name, self.title())
        
    def find_item(self, item_name):
        for item in self.items:
            if item.item.name == item_name: return item

    def find_path(self, world, dest):
        if not isinstance(dest, tuple): dest = dest.position
        if self._path_dest == dest and self._path: return self._path
        x, y = self.position
        dx, dy = dest
        max_d = 4
        if distance(x, y, dx, dy) > max_d*2:
            dx = x + min(max(dx - x, -max_d), max_d)
            dy = y + min(max(dy - y, -max_d), max_d)
            return self.find_path(world, (dx,dy))
        
        open_set = set()
        closed_set = set()
        
        directions = 'udlr'
        
        parents = {}
        face = {}
        def retrace_path(c):
            if parents[c] is None:
                return []
            return [face[c]] + retrace_path(parents[c])
        def metric(c):
            return distance(dx,dy,*c)
        
        open_set.add(self.position)
        parents[self.position] = None
        face[self.position] = []
        n = 0
        while len(open_set) > 0 and n < 50:
            n += 1
            current = min(open_set, 
                          key=lambda p: 
                          metric(p)
                          )
            
            if current == dest:
                return retrace_path(current)
            open_set.remove(current)
            closed_set.add(current)
            
            for direction in directions:
                c = self.can_move(world, direction, 
                                  change_direction=False, from_pos=current)
                if (c[0] or (not c[0] and c[1] == dest)) and not (c[0] in open_set or c[0] in closed_set):
                    c = c[0] if c[0] else c[1]
                    face[c] = direction
                    parents[c] = current
                    open_set.add(c)
        
        return [self.face(world, dest)]
        

    def fly(self, flying=True):
        self._need_to_fly = flying
    def real_fly(self, world, flying=True):
        if flying and self['Flying'] and self.stamina > 0:
            self.flying = True
        else:
            x, y = self.position
            if self.get_map(world).movable(x, y, self.peers.values(), char=self, flying=False):
                self.flying = False
        self._need_to_fly = None
        
    def gain_gold(self, gold):
        self.gold += gold
        
    def gain_xp(self, xp):
        #self.damage_number('%s XP' % xp)
        if self.penalty > 0:
            xp, self.penalty = (max(xp - self.penalty, 0), max(self.penalty - xp, 0))
        if xp > 0:
            old_level = self.level()
            self.xp += xp
            self.spent_xp += xp
            
            if self.level(False) != old_level:
                for level in range(old_level+1, self.level()+1):
                    extra_xp = level * 250
                    self.xp += extra_xp
                    self.message("<i>** Level up! You are now level %s. **</i>" % level)
                    self.message("<i>You gained %s extra XP.</i>" % extra_xp)
                    self.damage_number('Lv UP!', 1.5)
                self.set_values_for_sprites('short_summary', self.short_summary())
                    
        self.uncache_stats()

    def getattr(self, x):
        return getattr(self, x)

    def get_item(self, item_name):
        wanted_item = self.defs['items_dict'][item_name]
        
        for item in self.items:
            if item.item.name == item_name:
                if item.quantity < 100:
                    item.quantity += 1
                    self.changes['items'] = True
                    return True
                else:
                    return False
        new_item = ItemCopy(wanted_item)
                
        self.items.append(new_item)
        self.sort_items()
        self.uncache_move_speed()
        return True
        
    def get_map(self, world):
        return world.maps[self.map_name]

    def god_mode(self):
        self.clipping = True
        self.skills['Toughness'] = 1000
        self.skills['Endurance'] = 1000
        self.skills['Regeneration'] = 1000
        self.skills['Strength'] = 1000
        self.skills['Speed'] = 1000
        self.skills['Defense'] = 1000
        self.skills['Intelligence'] = 1000
        self.skills['Flying'] = 1000
        self.skills['Swimming'] = 1000
        self.skills['Knockback'] = 1000
        self.skills['Precision'] = 1000
        self.skills['Evasion'] = 1000
        self.skills['Spell Resist'] = 1000
        self.xp += 100000
        self.spent_xp = 10000000
        self.uncache_stats()
        self.restore_hp(), self.restore_stamina()
        self.record_changes('skills')
        #def f(a, b, c=True, d=None):
        #    if c: self.direction = b
        #    return True
        #self.can_move = lambda a, b, c=True, d=None: f(a,b,c,d)

    def goodness_summary(self):
        v = self['Goodness']
        desc = "<font color='%s'><b>" % ('#%02x%02x%02x' % self.rgb() ,)
        if abs(v) > 4: desc += "Very "
        desc += ('Good' if v > 0 else ('Bad' if v < 0 else 'Neutral'))
        desc += '</b></font>'
        return desc
        
    def good_target(self, char):
        return (1 if char.attacking == self.uid else 0, char)

    def learn_ability(self, ability):
        self.abilities.add(ability)
        self.record_changes('abilities')
    
    def learnable_skills(self):
        if not self._learnable_skills is None: return self._learnable_skills
        result = []
        for skill in self.defs['all_skills']:
            if not skill in self.race.disabled_skills and not skill.startswith('_') and not ':' in skill and not skill in self.skills:
                result.append((skill, self.upgrade_cost(skill), skill in self.race.potential_skills
                                                                or skill in self.charclass.potential_skills))
        self._learnable_skills = result
        return result
        
    def level(self, use_cache=True):
        if self._cached_level and use_cache: return self._cached_level
        self.cache_level(level_from(self.spent_xp))
        return self._cached_level
        
    def look(self, target, talk_to = False):
        self.looking_at = target
        if talk_to and not (hasattr(self, 'dead') and self.dead):
            self._need_to_talk_to = target

    def look_summary(self, looking=None):
        try:
            desc = ['<p>%s</p>' % self.fancy_summary(looking)]
            #desc += ['<br><br>XP: %s' % self.spent_xp]
            #desc += ['<br>Gold: %s' % self.gold]
            #desc += ['<br><br>' + self.vitals_summary()]
            desc += ['<br><br>' + self.main_skills_summary()]
            desc = ''.join(desc)
            
            return desc
        except: return ''

    def lose_item(self, item_name):
        item = self.find_item(item_name)
        if item:
            item.quantity -= 1
            if item.quantity < 1:
                self.items.remove(item)
                self.sort_items()
            self.changes['items'] = True
        return item.item

    def main_skills_summary(self):
        main_skills = ['Strength', 'Defense', 'Speed', 'Intelligence', 'Damage', 'Range']
        desc = '%s (%s)<br>' % (self.goodness_summary(), self['Goodness'])
        for skill in main_skills:
            desc += '<br>%s: %s' % (skill, self[skill])
        return desc

    def max_penalty(self):
        return min(max(xp_from(self.spent_xp)*2, 100), 10000)

    def max_stat_value(self, x):
        if x in MAX_STATS: return MAX_STATS[x]
        class_skill = x in self.charclass.start_skills or x in self.charclass.potential_skills
        race_skill = x in self.race.start_skills or x in self.race.potential_skills
        if class_skill: multiplier = 1.2
        elif race_skill: multiplier = 1.1
        else: multiplier = 1
        try: return max(self.race.start_skills[x] * 5 * multiplier, 50)
        except: return 50

    def message(self, txt):
        self.messages.append(txt)
        self.msg_prolong(min(max(4, len(txt) / 10), 15))

    def move(self, world, direction, not_if_attacking=True, change_direction=True, dontswim=False):
        if self.dead: return False
        if self._busy: return False
        if not_if_attacking:
            if self._attacked_this_turn: return False

        can = self.can_move(world, direction, change_direction=change_direction)
        if can[0]:
            dest_pos, (mx, my) = can
            if dontswim and not self.in_water and self.get_map(world).in_water(dest_pos):
                return False
            self.position = dest_pos
            #self._zone = zone(self.position)
            self.offset = (mx, my)
            self.step = True
        return can

    def move_events(self, world, fps):
        # this is a serious performance bottleneck
        self._peers = None
        current_map = self.get_map(world)
        '''try:
            if self.offset == (0,0) and (
                not self.uid in current_map.characters_by_zone[self._zone]):
                print 'WARNING', self.uid, self._zone, current_map.characters_by_zone
        except KeyError:
            print "keyerror:",self._zone, current_map.characters_by_zone.keys()'''
        if fps: self._FPS = fps
        fps = self._FPS
        ifps = 1. / fps
        #if self.need_to_spawn:
        #    world.spawn(self)
        #    return
        
        
        if self.invert:
            self.invert -= (BASE_FPS / fps)
            if self.invert <= 0: self.invert = 0
        if self.attack_animation:
            self.attack_progress -= (ATTACK_ANIMATION_SPEED / fps * BASE_FPS)
            if self.attack_progress <= 0:
                self.attack_progress = 0
                self.step = False
                self.attack_animation = []
                self.record_changes('attack_animation')
        
        for countdown in '_marks', 'buffs', '_damage_time':
            attr = getattr(self, countdown)
            if attr:
                try:
                    for key in attr:
                        attr[key] -= ifps
                        if attr[key] <= 0:
                            del attr[key]
                    if not countdown[0] == '_':
                        self.record_changes(countdown)
                except RuntimeError:
                    # if dictionary changes during iteration, stop and try again later
                    pass
        if self.force_msg:
            self.force_msg -= ifps
            if self.force_msg <= 0: self.force_msg = 0
                    
        if self._need_to_talk_to:
            target = world.get_char_by_uid(self._need_to_talk_to)
            if self.distance(target) > TALK_DISTANCE or self.offset != (0,0):
                self.chase(world, target)
            else:
                self.direction = self.face(world, target)
                target.talk_to(self)
                self._need_to_talk_to = None

        if self.dead:
            self.resolve_offset(world)
            if self._damage_dict:
                damage = self._damage_dict
                self._damage_dict = {}
                self._damage_time = {}
                total_damage = float(sum(damage.values()))
                xp = self._xp_to_distribute
                gold = self._gold_to_distribute
                self._xp_to_distribute = 0
                self._gold_to_distribute = 0
                for uid in damage:
                    ratio = (damage[uid] / total_damage) ** 1.5
                    xp_earned = int(max(1, xp * ratio)+.5)
                    gold_earned = int(max(1 if gold else 0, gold * ratio)+.5)
                    world.death_msg(uid, self.name)
                    world.give_xp(uid, self.level(), xp_earned, xp)
                    if gold_earned: world.give_gold(uid, gold_earned, gold)
                                
            self.unhostile(world)
            self.dead -= ifps
            if self.dead <= 0.5:
                self.respawn(world)
            return
            
        # poison damage, standing
        if self.poison:
            self._poison_effort -= 1./fps
            if self._poison_effort <= 0:
                self._poison_effort += POISON_THRESHOLD
                poison_dmg = max(1, self.poison/5 + 1)
                self.poison -= poison_dmg
                self.change_hp(-poison_dmg)
                if self.poison <= 0: self._poison_effort = 0

        if (not self.flying and hasattr(self, 'flyer') and self.flyer):
            self._need_to_fly = True
        if not self._need_to_fly is None:
            self.real_fly(world, self._need_to_fly)
                
        defs = self.defs
    
        if self.flying:
            self._fly_effort += (ifps) * BASE_FPS
            if self._fly_effort >= self['Flying'] * FLY_THRESHOLD:
                self._fly_effort -= self['Flying'] * FLY_THRESHOLD
                if hasattr(self, 'flyer') and self.flyer: pass
                else: self.change_stamina(-1)
            if self.stamina <= 0:
                self.fly(False)
        elif self.swimming() and not (hasattr(self, 'swimmer') and self.swimmer):
            self._swim_effort += (ifps) * BASE_FPS
            swim_ability = max(self['Swimming'], 1) * SWIM_THRESHOLD
            if not self.stamina: swim_ability = max(swim_ability/2, SWIM_THRESHOLD)
            if self._swim_effort >= swim_ability:
                self._swim_effort -= swim_ability
                if self.stamina:
                    self.change_stamina(-1)
                else:
                    self.change_hp(-1)
        
        if self.offset == (0,0):
            # events
            if self.position in current_map.events:
                for event in current_map.events[self.position]:
                    if self.solve_formula(event.condition, cache=False):
                        self.do_event(event)
            
            # if character is done moving into a tile but still moving, move to the next tile
            if self._moving and not self._attacked_this_turn:
                # moving
                self.move(world, self._moving[0])
                if len(self._moving) > 1:
                    self._moving = self._moving[1:] + self._moving[0]
            elif self.attacking:
                self.attack_events(world)
            else:
                # standing still: recover HP and stamina
                if self.stamina < self['Stamina'] or self._walk_effort > 1:
                    self._walk_effort -= 1 + (self['Endurance'] + self['Regeneration'])
                    if self.stamina == self['Stamina']:
                        if self._walk_effort < 1: self._walk_effort = 1
                    if self._walk_effort <= 0:
                        self.change_stamina(1)
                        self._walk_effort += (WALK_THRESHOLD - self['Regeneration'])
                if self.hp < self['Vitality']:
                    self._regen += 1 + (self['Toughness'] + self['Regeneration'])
                    threshold = REGEN_THRESHOLD - self['Regeneration']
                    if self._regen >= threshold:
                        self._regen -= threshold
                        self.change_hp(1)
        else:
            self.resolve_offset(world)

    def move_speed(self, fps_adjust=True):
        if self._cached_move_speed is not None:
            spd = self._cached_move_speed
        else:
            x = max(self['Speed'] - 5, 1)
            p0 = BASE_MOVE_SPEED
            k = p0 + p0
            r = 0.01
            spd = (k*p0*math.e**(r*x)/(k + p0*(math.e**(r*x) - 1)))
            
            self.cache_move_speed(spd)
        if fps_adjust: return spd / self._FPS * BASE_FPS
        else: return spd

    def msg_prolong(self, time):
        self.force_msg = max(self.force_msg, time)

    def msg_done(self):
        self.force_msg = 0

    def next_level(self):
        if self.level() >= MAX_LEVEL: return False
        goal = self.level() + 1
        return int(500 * (goal ** 2) - self.spent_xp)

    def next_selection(self, reverse=False):
        peers = [c for c in (self.peers.values()) if not (hasattr(c, 'dead') and c.dead)]
        peers += [self]
        sorted_peers = [c.uid for c in sorted(peers, key = lambda c: c.position)]        
        if not sorted_peers: return
        if (self.looking_at and (self.looking_at in sorted_peers)) and (self.looking_at != self.uid):
            sorted_peers.remove(self.uid)
            l = sorted_peers.index(self.looking_at)
        else:
            l = sorted_peers.index(self.uid)
        #self.message(str((sorted_peers, str(l))))
        if reverse:
            self.look(sorted_peers[l-1])
        else:
            try: self.look(sorted_peers[l+1])
            except IndexError: self.look(sorted_peers[0])

    def next_target(self, world):
        peers = world.get_peers(self.uid)
        targets = [self.good_target(p) for p in peers if not (hasattr(p, 'dead') and p.dead)]
        if not targets: return
        best_aggr, best_target = max(targets)
        if best_aggr > 0:
            if best_target.uid != self.attacking:
                self.attack(best_target.uid)
                self.look(self.attacking)
        else:
            self.attack(False)

    def ordered_skills(self):
        return [skill for skill in self.defs['all_skills'] if skill in self.skills]

    def peer_by_uid(self, uid):
        for c in self.peers:
            if c == uid and (not hasattr(c, 'dead') or not c.dead):
                return c
        return None
        
    def perceived_level(self):
        return self.level() * (float(self.hp)/self['Vitality'])**0.9
    
    def poison_dmg(self, amt, by):
        self.poison += amt
        if not self._poison_effort: self._poison_effort = POISON_THRESHOLD
        self.record_damage(by.uid, amt)
        
    def push(self, world, direction):
        self.move(world, direction, not_if_attacking=False, change_direction=False)

        if self.attack_wait:
            self.attack_wait = max(self.attack_wait - ATTACK_THRESHOLD/4, 0)

    def quality(self):
        try:
            if not self._cached_quality: raise Exception()
            return self._cached_quality
        except:
            potentials = set([s for s in set.union(self.race.potential_skills, self.charclass.potential_skills)
                              if not s in self.skills])
            q = int(
                    sum([max(c-10, 1)**0.9 
                         for (n, c) in self.skills.items()
                         if not n.startswith('_')
                         ])                                                 # value of all skills/attribute levels
                    + self['Vitality']**0.9 + self['Stamina']**0.9          # vitals
                    + self.dps()**0.9                                       # dps
                    + (len(potentials)**0.9)*2                              # potential skills
                    )
            self._cached_quality = q
            return q

    def range(self):
        wpn = self.attacking_ability()
        if wpn:
            return max(self.solve_formula(wpn['Range']), 1)
        else: return 1

    def read_messages(self):
        to_send = []
        for i in range(len(self.messages)):
            to_send.append(self.messages.pop())
        return to_send
        
    def read_damage_numbers(self):
        to_send = []
        for i in range(len(self.damage_numbers)):
            to_send.append(self.damage_numbers.pop())
        return to_send

    def real_offset(self):
        x, y = self.offset
        return (TILE_SIZE*(x+smoothstep(x))/2, TILE_SIZE*(y+smoothstep(y))/2)

    def bin_offset(self):
        return tuple([1 if n > 0 else -1 if n < 0 else 0 for n in self.offset])

    def rebase_hp_stamina(self):
        if self.hp > self['Vitality']: self.hp = self['Vitality']
        if self.stamina > self['Stamina']: self.stamina = self['Stamina']

    special_changes = {
                       'offset': lambda self: self.bin_offset(),
                       'hp': lambda self: float(self.hp) / self['Vitality'],
                       'stamina': lambda self: float(self.stamina) / self['Stamina'],
                       'map': lambda self: self.map.name,
                       'speed': lambda self: round(self.move_speed(fps_adjust=False) * 
                                self.move_multiplier(), 4),
                       }
    bools = set(['force_msg', 'invert', 'poison', 'attack_progress'])
    rounds = set([])
    ints = set(['dead', 'attack_wait'])
    nocheck = set(['need_uncache'])
    
    def record_changes(self, x):
        if x == 'offset': 
            value = self.bin_offset()
        elif x == 'speed':
            value = self.special_changes['speed'](self)
        elif x in self.bools:
            value = 1 if getattr(self, x) else 0
        elif x in self.rounds:
            value = round(getattr(self, x), 3)
        elif x in self.ints:
            value = int(getattr(self, x)+0.5)
        else: 
            value = getattr(self, x)
            
        if (not x in self._last_values) or value != self._last_values[x]:
            self.changes[x] = value
            if isinstance(value, dict) or isinstance(value, set):
                self.changes['need_uncache'] = True
        if not (isinstance(value, dict) or isinstance(value, list) or isinstance(value, set)
                or x in self.nocheck):
            self._last_values[x] = value
        
        if x in sprite_values:
            for sprite in self._sprites.values():
                try: sprite.record_change(x, self.special_changes[x](self))
                except KeyError: sprite.record_change(x, value)

    def record_damage(self, uid, amt):
        if not uid in self._damage_dict:
            self._damage_dict[uid] = 0
        self._damage_dict[uid] -= amt
        self._damage_time[uid] = 300
        
    def move_multiplier(self):
        if self._cached_move_multiplier is None:
            move_speed = 1
            
            if self.swimming(): 
                move_speed *= 0.5 + min(self['Swimming'], 100)/100.
            if self.flying:
                move_speed *= 1 + min(self['Flying'], 100)/500.
            if self._running: 
                # character is running, which costs a lot of stamina
                move_speed *= 1.67
            else:
                # character is walking
                if self.stamina == 0:
                    move_speed *= 0.75

            if hasattr(self, '_patrol') and not self.attacking: 
                if self._patrol == 0:
                    if self.position == self.original_position and self.offset == (0,0): return 0
                else:
                    move_speed *= self._patrol if not self.attacking else 1
            
            self._cached_move_multiplier = move_speed
        
        return self._cached_move_multiplier
        
    def resolve_offset(self, world, speed_adjust=1):
        if self.offset != (0,0):
            fps = self._FPS
            ox, oy = self.offset
            move_speed = self.move_speed() * self.move_multiplier()
            
            if self._running:
                self._run_effort += (1. / fps) * BASE_FPS
                run = self['Running'] * RUN_THRESHOLD
                if self._run_effort > run:
                    self.change_stamina(-1)
                    self._run_effort -= run
            # poison damage, moving
            if self.poison:
                self._poison_effort -= POISON_MOVE_PENALTY/fps
                if self._poison_effort <= 0:
                    self._poison_effort += POISON_THRESHOLD
                    poison_dmg = max(1, self.poison/3 + 1)
                    self.poison -= poison_dmg
                    self.change_hp(-poison_dmg)
                    if self.poison <= 0: self._poison_effort = 0
            
            if ox < 0: ox += move_speed
            if ox > 0: ox -= move_speed
            if oy < 0: oy += move_speed
            if oy > 0: oy -= move_speed
            if abs(ox) < move_speed: ox = 0
            if abs(oy) < move_speed: oy = 0
            if self.step and abs(ox) < 0.5 and abs(oy) < 0.5:
                self.step = False
            self.offset = (ox, oy)
            
            if self.offset == (0,0):
                self.get_map(world).done_moving(self, world)
                
    def resolve_offset_p(self, fps):
        resolve_offset_p(self, fps)
            
    def respawn(self, world):
        world.spawn(self)
        self.offset = (0,0)
        self.restore_hp()
        self.restore_stamina()

        self.look(self.uid)
        
        if self.random_color: self.colors = random.randint(0,5)

    def restore_hp(self):
        self.hp = self['Vitality']
        self.poison = 0
        self.dead = False
        self._marks = {}
        
    def restore_stamina(self):
        self.stamina = self['Stamina']
                
    def rgb(self):
        min_value = 48
        r = max(min(min_value + (255-min_value) * (self['Goodness']/-6.), 255), min_value)
        g = min_value
        b = max(min(min_value + (255-min_value) * (self['Goodness']/6.), 255), min_value)
        return (r,g,b)
        
    def run(self, running=True):
        if running:
            if self.stamina > 0:
                self._running = True
        else:
            self._running = False
        self._cached_move_multiplier = None
        self.record_changes('speed')
            
    def set_attacking_with(self, ability):
        if self.attacking_with != ability:
            cost = self.ability_cost(ability)
            if self.stamina >= cost:
                if not (self._already_attacking_with is False):
                    self._want_to_attack_with = ability
                else:
                    self.attacking_with = ability
                    self.attack_wait = 0
                    self.uncache_stats()
            
    def set_moving(self, direction, remove=False):
        if remove:
            if self._moving:
                self._moving = self._moving.replace(direction, '')
            if not self._moving: self._moving = False
        else:
            self._moving = direction
            
    def set_peers(self, peers):
        self.peers = {}
        for peer in filter(lambda p: not p.uid == self.uid, peers):
            self.peers[peer.uid] = peer

    def set_switch(self, switch_name, value=True):
        self.switches[switch_name] = value
        
    def set_values_for_sprites(self, value, x):
        for sprite in self._sprites.values():
            sprite.record_change(x, value)

    def shop(self, shop):
        self.shopping = shop

    def short_summary(self):
        return "%s L%s" % (self.name, self.level())

    def skill_summary(self):
        result = '%s (%s)<br>' % (self.goodness_summary(), self['Goodness'])
        for skill in self.defs['all_skills']:
            if not skill in self.race.disabled_skills and not skill == 'Goodness' and self[skill] and not skill.startswith('_'):
                result += '<br><i>%s</i>%s' % (skill, (': %s' % self[skill]) if self[skill] else '')
        return result

    def solve_formula(self, formula, negative_ok=False, cache=True):
        try:
            return self._cached_formulas[formula]
        except KeyError:
            new_formula = ''.join(str(formula))
            replacements = re.findall("<[^<>]*>", new_formula)
            for replacement in replacements:
                skill_name = replacement[1:-1]
                value = 0
                if not skill_name in self.skills and ':' in skill_name:
                    command, arg = skill_name.split(':')
                    command = command.lower()
                    if command == 'equipped':
                        if arg == 'No Weapon':
                            for item in self.items:
                                if item.equipped and 'Weapon' in item.item.types(): value = 1
                        else:
                            for item in self.items:
                                if item.equipped and arg in item.item.types(): value = 1
                    elif command == 'race':
                        if self.race.name in arg: value = 1
                else:
                    value = self[skill_name]
                
                new_formula = new_formula.replace(replacement, str(value))
            try:
                value = int(eval(new_formula)) if new_formula else 0
                if negative_ok: return value
                else: value = max(value, 0)
            except: value = 0; raise
            if cache: self.cache_formula(formula, value)
            return value
            
    def sorted_abilities(self):
        if not self._sorted_abilities is None: return self._sorted_abilities
        result = [('Attack', None, self.ability_cost(self.weapon()))]
        for ability in self.defs['all_abilities']:
            ability = self.defs['abilities_dict'][ability]
            if ability.name in self.abilities: 
                result.append((ability.name, ability.name, self.ability_cost(ability)))
        self._sorted_abilities = result
        return result

    def sorted_skills(self):
        if not self._sorted_skills is None: return self._sorted_skills
        result = []
        for skill in self.defs['all_skills']:
            if ((skill in self.skills or skill in self.use_skills) 
                and not skill in self.race.disabled_skills 
                and not skill.startswith('_') 
                and not (self[skill] is False or self[skill] is None)):
                if skill in self.use_skills: exp = self.use_skills[skill]
                else: exp = 0 if ':' in skill else None
                result.append((skill, self[skill], exp))
        self._sorted_skills = result
        return result

    def sort_items(self):
        self.items.sort(key=lambda item: (0 if item.equipped else 1, 
                                          0 if 'Usable' in item.item.types() else 1, 
                                          0 if 'Equippable' in item.item.types() else 1, 
                                          self.solve_formula(item.item['Value']) * -1,
                                          item.item.name
                                          ))
        self.changes['items'] = True

    def spawn(self):
        self.need_to_spawn = True
        
    def splash(self):
        wpn = self.attacking_ability()
        if wpn:
            return self.solve_formula(wpn['Splash'])
        else: return 1
        
    def stat(self, name):
        try:
            value = self._cached_stats[name]
        except KeyError:
            if name in self.race.disabled_skills:
                return 0
            if not name in self.skills:
                return 0
            if self.skills[name] is True: return True
            value = min(self.skills[name], self.max_stat_value(name))
            if name in self.race.start_skills:
                value += self.race.start_skills[name] * ((1 + (self.level()-1)/20.) if name != 'Goodness' else 1)
            if name in self.defs['skills_dict']:
                value += self.solve_formula(self.defs['skills_dict'][name].formula, negative_ok=(name=='Goodness'))
            value = int(value)
            self.cache_stat(name, value)

        return value + self.buff_value(name)
        
    def stop_actions(self):
        self.run(False)
        self.attack(False)
        self._need_to_talk_to = None

    def summary(self):
        return "%s Level %s %s %s" % (self.name, self.level(), self.race.name, self.title())

    def swimming(self):
        return (self.in_water and not self.flying)

    def switch(self, switch_name):
        if not switch_name in self.switches: return None
        return self.switches[switch_name]
        
    def talk_to(self, char):
        if self.attacking:
            char.message('%s  HP: %s/%s  Stamina: %s/%s' % (
                         self.short_summary(), 
                         self.hp, self['Vitality'], 
                         self.stamina, self['Stamina']))
        elif hasattr(self, '_talk_to') and self._talk_to:
            self.turn(char)
            if isgeneratorfunction(self._talk_to):
                self._busy = True
                event = self._talk_to(self, char)
                char._talking_to = (event, self)
                event.next()
            else:
                self._talk_to(self, char)
                
    def make_choice(self, choice):
        if choice in self.choices:
            self.choices = None
            if self._talking_to:
                event, char = self._talking_to
                try:
                    self.message('%s: %s' % (self.name.upper(), choice))
                    event.send(choice)
                except StopIteration:
                    self._talking_to = None
                    char._busy = False
            
    def give_choice(self, *choices):
        self.choices = choices
    
    def title(self):
        return self._title

    def trained_skills(self):
        return set([skill for skill in self.skills])
        
    def unequip(self, item_name):
        result = False
        for item in self.items:
            if item.item.name == item_name:
                item.equipped = False
                self.message('You unequipped <b>%s</b>.' % item.item.name)
                result = True
        self.update_equipment()
        if result:
            self.sort_items()
            self.uncache_stats()
            self.changes['items'] = True
            self.rebase_hp_stamina()
        return result
    
    def unequip_slot(self, slot_name):
        success = False
        slots = [slot.strip() for slot in slot_name.split(', ')]
        for item in self.items:
            for this_slot in [slot.strip() for slot in item.item['Space'].split(', ')]:
                if this_slot in slots:
                    item.equipped = False
                    success = True
        for item in self.items:
            if item.equipped and not self.solve_formula(item.item['Requires']):
                item.equipped = False
        if success: self.sort_items()
        return success

    def unhostile(self, world):
        self.attack(False)
        try:
            chars = world.get_peers(self.uid)
            for char in chars:
                if char.attacking == self.uid:
                    char.attack(False)
                if char.looking_at == self.uid:
                    char.looking_at = None
        except AttributeError: pass
        except KeyError: pass

    def untrained_skills(self):
        return set([skill for skill in self.defs['skills_dict'] 
                     if not skill in self.skills 
                     and not skill in self.race.disabled_skills])

    def update_equipment(self):
        for item in self.items:
            if item.equipped and not self.can_equip(item.item):
                item.equipped = False
                self.changes['items'] = self.items

    def upgrade(self, name):
        title = self.summary()
        
        success = False
        learned = name in self.skills
        
        cost = self.upgrade_cost(name)
        if cost and self.available_xp() >= cost:
            self.xp -= cost
            
            if name in self.skills:
                self.skills[name] += 1
            else:
                self.skills[name] = 1
                
            if name in self.skill_upgrades:
                self.skill_upgrades[name] += 1
            else:
                self.skill_upgrades[name] = 1
                
            success = cost
            
            self.record_changes('skills')
            self.record_changes('skill_upgrades')
        else:
            success = False
            
        self.uncache_stats()
        if success:
            if not learned:
                self.message("You spent <b>%s XP</b> to learn <b>%s</b>." % 
                             (success, name))
            else:
                self.message("You spent <b>%s XP</b> to upgrade <b>%s</b> to %s." % 
                             (success, name, self[name]))
        else:
            self.message("Failed to upgrade skill %s." % name)
         
        return success

    def upgrade_cost(self, name, test=False):
        if self.dead: return False
        if name in ('Goodness', 'Vitality', 'Stamina'): return False
        if ':' in name: return False
        try: 
            return self._cached_upgrade_costs[name]
        except KeyError:
            if name in self.skills and self.skills[name] >= self.max_stat_value(name):
                return False
            if name in self.race.disabled_skills or ((not test) and not name in self.defs['skills_dict']):
                return False

            class_skill = name in self.charclass.start_skills or name in self.charclass.potential_skills
            race_skill = name in self.race.start_skills or name in self.race.potential_skills
            
            if class_skill: multiplier = 0.8
            elif race_skill: multiplier = 1
            else: multiplier = 1.2
            
            
            if name in self.skill_upgrades:
                if self.skill_upgrades[name] >= 200:
                    return False
                l = self.skill_upgrades[name] + 1
            else:
                l = ((len([k for k in self.skill_upgrades
                           if not k in self.race.start_skills
                           and (not k in self.race.potential_skills if not race_skill else k in self.race.potential_skills)
                           and (not k in self.charclass.potential_skills if not class_skill else k in self.charclass.potential_skills)
                           and not k in self.charclass.start_skills])) + 1) * 4
            cost = int(round((160 * l) ** 0.9, -1) * multiplier)
            self.cache_upgrade_cost(name, cost)
            return cost
            
    def learn_cost(self):
        return self.upgrade_cost('test', True)

    def use_item(self, item):
        item = self.find_item(item)
        if item:
            item.item.use_on(self)


    def vision(self):
        v = VISION
        try:
            if self.flying: v += 1
        except: pass
        return v

    def vitals_summary(self, main=False):
        hp_meter = meter(float(self.hp) / self['Vitality'])
        stamina_meter = meter(float(self.stamina) / self['Stamina'])
        desc = "%s %s%s/%s HP<br>%s %s/%s Stamina" % (
                hp_meter, self.hp, 
                '' if not self.poison else '<font color=GREEN>-%s</font>' % (self.poison),
                self['Vitality'],
                stamina_meter, self.stamina, self['Stamina'])
        return desc
                
    def weapon(self):
        return self.equipped('Main Hand')
        
    def weapon_animation(self, target):
        #if self.attacking: target = self.current_target(world=world)
        #else: target = self
        wpn = self.attacking_ability()
        
        self.step = True        
        animation = {}
        if wpn:
            animation['image'] = wpn['image']
            animation['animation_type'] = wpn['animation_type']
            animation['direction'] = self.direction
            if animation['animation_type']:
                animation['src_pos'] = self.position
                animation['src_fly'] = self.flying if hasattr(self, 'flying') else False
                animation['dest_pos'] = target.position
                animation['dest_fly'] = target.flying if hasattr(target, 'flying') else False
                if 'animation_height' in wpn: animation['elevate'] = float(wpn['animation_height'])
                if 'animation_trace' in wpn: animation['trace'] = wpn['animation_trace']
        self.attack_animation.append(animation)
        self.attack_progress = 1
        self.record_changes('attack_animation')

    def xp_from(self):
        return (xp_from(self.spent_xp) + 
                 sum([self[x] for x in [
                      'Strength',
                      'Defense',
                      'Speed',
                      'Intelligence',
                      'Toughness',
                      'Endurance',
                      ]])/40)
