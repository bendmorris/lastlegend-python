import collections

class CharClass(collections.namedtuple('CharClass', ['name', 'description', 'start_skills', 'potential_skills', 'starting_items', 'abilities'])):
    def __new__(self, name, description, start_skills, potential_skills, starting_items, abilities):
        return super(CharClass, self).__new__(self, name, description, start_skills, potential_skills, starting_items, abilities)
