from mmorpg import *
import math
import re
import random


def random_number(x):
    return int(x + x*(random.random()-1)/4)

def log4(n):
    return int(math.log(max(n, 1), 4))

def zone(position):
    x, y = position
    return (x*2/(SCREEN_X), y*2/(SCREEN_Y))

def meter(amt, max_length=15, color=True, html=True):
    if not color: color='WHITE'
    else: color = color_from_amt(amt)
    bars = 0 if amt == 0 else min(int(amt * max_length) + 1, max_length)
    the_meter = ('='*bars + '&nbsp;'*(max_length-bars))
    if html:
        return "[<code><font color='%s'>%s</font></code>]" % (color, the_meter)
    else:
        return "[%s]" % the_meter.replace('&nbsp;', ' ')        
        
html_colors = {
               'WHITE':COLORS_WHITE,
               'RED': COLORS_RED,
               'YELLOW': COLORS_YELLOW,
               'GREEN': COLORS_GREEN}
               
def color_from_amt(amt, html=True):
    if amt < 0.33: result = 'RED'
    elif amt < 0.67: result = 'YELLOW'
    else: result = 'GREEN'
    if not html: return html_colors[result]
    return result
        
def distance(x1, y1, x2, y2, extra=0):
    return abs(x1-x2) + abs(y1-y2) + extra
    #return int(round(((x1-x2)**2 + (y1-y2)**2 + extra**2) ** 0.5))
    
def xp_from(x):
    return 20 + 10*level_from(x) + int((x / 8) ** 0.6)
    
def level_from(x):
    if x > 0:
        return min(max(int((x / 500) ** 0.5), 1), MAX_LEVEL)
    else:
        return 1

def smoothstep(n):
    return ((1 if n > 0 else -1) * (abs(n)**2 * (3-2*abs(n))))#/2. + n/2.
def smoothprojectile(n):
    return n ** 0.5
    
def onscreen(target, center, screen_size):
    if isinstance(target, tuple):
        px, py = target
        offset = (0,0)
    else:
        px, py = target.position
        offset = target.offset if hasattr(target, 'offset') else (0,0)
    x, y = center.position
    main_offset = real_offset(*center.offset_p)
    
    dx = (((screen_size[0] / 2) + (px - x)) * TILE_SIZE) - (offset[0] - main_offset[0])
    dy = (((screen_size[1] / 2) + (py - y)) * TILE_SIZE) - (offset[1] - main_offset[1])
    
    if hasattr(target, 'flying') and target.flying:
        dy -= (1 + smoothstep(target.fly_offset)) * TILE_SIZE
    
    return (dx, dy)
    
    
def html_format(text):
    formatted_text = text
    replacements = re.findall("<[^<>=]*>", formatted_text)
    for replacement in replacements:
        formatted_text = formatted_text.replace(replacement, replacement[1:-1])
    return formatted_text


def real_offset(x, y):
    return (TILE_SIZE*(x+smoothstep(x))/2, TILE_SIZE*(y+smoothstep(y))/2)
