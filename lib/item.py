class Item:
    def __init__(self, name, description):
        self.name = name
        self.description = description
        self.attributes = []
        
    def __contains__(self, x):
        if not hasattr(self, 'attribute_set'):
            self.attribute_set = set([attribute[0] for attribute in self.attributes])
        return x in self.attribute_set
        
    def __getitem__(self, x):
        if not hasattr(self, 'attribute_dict'):
            self.attribute_dict = {}
            for (name, value) in self.attributes:
                self.attribute_dict[name] = value
        if x in self.attribute_dict: return self.attribute_dict[x]
        if x == 'drop_img': return 'chest'
        if x == 'Requires': return '1'
        return '0'

    def types(self):
        if not hasattr(self, '_types'):
            self._types = set([s.strip() for s in self['type'].split(',')])
        return self._types

    def use_on(self, char):
        if char.dead: return False
        target = self['Target']
        if target == 'Self':
            hp = char.solve_formula(self['HP'], True)
            stamina = char.solve_formula(self['Stamina'], True)

            msg = ["You used %s." % self.name]
            verbs = []
            if      hp > 0:     verbs.append('restored %s HP' % hp)
            elif    hp < 0:     verbs.append('reduced your HP by %s' % abs(hp))
            if      stamina > 0:verbs.append('restored %s stamina' % stamina)
            elif    stamina < 0:verbs.append('reduced your stamina by %s' % abs(stamina))
            if verbs:
                msg += [" It %s." % ' and '.join(verbs)]
            msg = ''.join(msg)
            char.change_hp(hp)
            char.change_stamina(stamina)
            char.message(msg)
            char.lose_item(self.name)
        
        
class ItemCopy:
    def __init__(self, item, equipped=False, quantity=1):
        self.item = item
        self.equipped = equipped
        self.quantity = quantity

    def verbs(self, char):
        verbs = []
        if char.dead: return verbs
        if 'Usable' in self.item.types(): verbs.append('use')
        if 'Equippable' in self.item.types():
            if self.equipped: verbs.append('unequip')
            elif char.can_equip(self.item): verbs.append('equip')
        verbs.append('drop')
        return set(verbs)

        
'''class ItemCopy(collections.namedtuple('ItemCopy', ['item', 'equipped', 'quantity'])):
    def __new__(self, item, equipped=False, quantity=1):
        return super(ItemCopy, self).__new__(self, item, equipped, quantity)'''
