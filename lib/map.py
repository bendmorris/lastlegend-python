import collections
import random
import uuid
import shlex
from mmorpg.tiles.tiles import *
from mmorpg.lib.npc import NPC
from mmorpg.lib.shop import Shop
from npc import *
import numpy as np


class Portal(collections.namedtuple('Portal', ['dest_map', 'dest_entry'])):
    def __new__(self, dest_map, dest_entry):
        return super(Portal, self).__new__(self, dest_map, dest_entry)
        
        
class Event(collections.namedtuple('Event', ['position', 'condition', 'events'])):
    def __new__(self, position, condition, events=None):
        return super(Event, self).__new__(self, position, condition, events if events else [])
        

class Obj(collections.namedtuple('Obj', ['type', 'contents', 'empty_type', 'respawn'])):
    def __new__(self, obj_type, contents=None, empty_type=None, 
                 respawn=120):
        return super(Obj, self).__new__(self, obj_type, contents, empty_type if empty_type else obj_type, respawn)
        
        
class GetOutOfLoop(Exception): pass
        

class Map(object):
    def __init__(self, map_file, world, defs):
        self.changes = {}
        self.uid = str(uuid.uuid4().hex)
        self.uncache()
        
        self.tiles = []
        self.markers = []
        self.portals = {}
        self.entries = {}
        self.entry_locations = set()
        self.NPCargs = []
        self.NPCs = set()
        self.objs = {}
        self.obj_chars = {}
        self.shops = {}
        self.events = {}
        self.regions = {}
        self.region_music = {}
        self.default_music = None
        
        self.defs = defs
        
        # key=zone, value=set of characters
        self.characters = {}
        self.characters_by_zone = {}
        
        self.special_chars = {}
        
        input_file = open(map_file, 'r')
        self.name = input_file.readline().strip()
        line = True
        _talk_to_count = 0
        # read in map tiles
        while line:
            line = input_file.readline().rstrip()
            if line:
                line_markers = range(0, len(line), 2)
                line_tiles = range(1, len(line), 2)
                self.tiles.append(''.join([line[i] for i in line_tiles]))
                self.markers.append(''.join([line[i] for i in line_markers]))
        
        # parse map description for objs, NPCs, portals/entries, etc.
        line = input_file.readline()
        while line:
            nextline = True
            
            if line: line = line.strip()
            try:
                if line and not line[0]=='#':
                    s = line.split()
                    command = s[0].lower().strip()
                    arg = line[len(command):].strip()
                    if command == 'portal':
                        args = arg.split()
                        dest_map, dest_entry = args
                        
                        x1, x2, y1, y2 = self.regions[last_region]
                        try:
                            for b in range(y1, y2+1):
                                for a in range(x1, x2+1):
                                    if self.markers[b][a] == '>':
                                        self.markers[b] = self.markers[b][:a] + ' ' + self.markers[b][a+1:]
                                        self.portals[(a,b)] = Portal(dest_map, dest_entry)
                                        raise GetOutOfLoop
                            raise Exception('Not found')
                        except GetOutOfLoop: pass
                        
                    elif command == 'entry':
                        entry_name = arg
                        
                        x1, x2, y1, y2 = self.regions[last_region]
                        try:
                            for b in range(y1, y2+1):
                                for a in range(x1, x2+1):
                                    if self.markers[b][a] == '<':
                                        self.markers[b] = self.markers[b][:a] + ' ' + self.markers[b][a+1:]
                                        self.entries[entry_name] = (a,b)
                                        raise GetOutOfLoop
                            raise Exception('Not found')
                        except GetOutOfLoop: pass
                        
                    elif command == 'obj':
                        args = arg.split()
                        tile, new_tile, obj_type = args
                        
                        self.special_chars[tile] = (new_tile, obj_type)
                        
                    elif command == 'tile':
                        pass
                        #args = arg.split()
                        #tile, new_tile, old_type = args
                        
                        #self.special_chars[tile] = (new_tile, old_tile)
                        
                    elif 'npc' in command or 'wild' in command:
                        if 'x' in command:
                            command, num_NPCs = command.split('x')
                            num_NPCs = int(num_NPCs)
                            npc_args = arg.strip()
                            src_pos = None
                        else:
                            num_NPCs = 1
                            npc_args = arg.strip()
                            x1, x2, y1, y2 = self.regions[last_region]
                            try:
                                for b in range(y1, y2+1):
                                    for a in range(x1, x2+1):
                                        if self.markers[b][a] == '*':
                                            self.markers[b] = self.markers[b][:a] + ' ' + self.markers[b][a+1:]
                                            src_pos = (a,b)
                                            raise GetOutOfLoop
                                raise Exception('Not found')
                            except GetOutOfLoop: pass
                            npc_args += ', patrol=False'
                        
                        npc_args += ', world=world, defs=self.defs, position=%s, start_map="%s", map_name="%s", home_region="%s"' % (src_pos, self.name, self.name, region_name)
                        
                        try: 
                            line2 = input_file.readline()
                            line = line2
                            nextline = False
                        except: line2 = None
                        
                        if line2:
                            if line2.strip().startswith('talk:'):
                                _talk_to_lines = []
                                line2 = input_file.readline()
                                while line2.strip() != 'end':
                                    _talk_to_lines.append(line2[4:])
                                    line2 = input_file.readline()
                                _talk_to_count += 1
                                exec(('def _talk_to%s(self, char):\n' % _talk_to_count) + ''.join(_talk_to_lines))
                                exec('self._talk_to%s = _talk_to%s' % (_talk_to_count, _talk_to_count))
                                npc_args += ', _talk_to=self._talk_to%s' % _talk_to_count
                                nextline = True
                        
                        npc_class = {
                                     'npc': 'NPC', 
                                     'wild': 'WildNPC'
                                     }[command]
                        self.NPCargs += ["%s(%s)" % (npc_class, npc_args)] * num_NPCs
                        
                    elif command == 'event':
                        source, condition = [s.strip() for s in arg.split(':')] if ':' in arg else (arg, 'True')
                        src_pos = tuple([int(n) for n in source.split(',')])
                        new_event = Event(src_pos, condition)
                        try: line2 = input_file.readline()
                        except: line2 = None
                        while line2 and line2[0] in (' ', '\t'):
                            line2 = line2.strip()
                            event_segs = line2.split(':')
                            event_type = event_segs[0]
                            event_content = ':'.join(event_segs[1:])
                            new_event.events.append((event_type, event_content))
                            line2 = input_file.readline()
                        line = line2
                        nextline = False
                        
                        if not src_pos in self.events:
                            self.events[src_pos] = []
                        self.events[src_pos].append(new_event)
                    
                    elif command == 'region':
                        args = arg.split()
                        region_number, region_name = args[:2]
                        if len(args) > 2: music = args[2]
                        else: music = self.music
                        
                        points = []
                        for b in range(len(self.markers)):
                            for a in range(len(self.markers[b])):
                                if self.markers[b][a] == region_number:
                                    points.append((a,b))
                        
                        a = [point[0] for point in points]
                        b = [point[1] for point in points]
                        self.regions[region_name] = (min(a),max(a),min(b),max(b))
                        self.region_music[region_name] = music
                        last_region = region_name
                            
                    elif command == 'music':
                        self.music = arg.strip()
                        
                    elif command == 'shop':
                        args = shlex.split(arg)
                        shop_name, items = args[0], args[1:]
                        
                        x1, x2, y1, y2 = self.regions[last_region]
                        try:
                            for b in range(y1, y2+1):
                                for a in range(x1, x2+1):
                                    if self.markers[b][a] == '$':
                                        self.markers[b] = self.markers[b][:a] + ' ' + self.markers[b][a+1:]
                                        src_pos = (a,b)
                                        raise GetOutOfLoop
                            raise Exception('Not found')
                        except GetOutOfLoop: pass
                        
                        self.shops[src_pos] = Shop(shop_name, [defs['items_dict'][item] for item in items])
                        
            except:
                print command, args
                raise                        
                    
            if nextline: line = input_file.readline()
            
        for b in range(len(self.tiles)):
            for a in range(len(self.tiles[b])):
                t = self.tiles[b][a]
                last_t = None
                while not last_t == t:
                    last_t = t
                    new_t = None
                    if t in self.special_chars:
                        obj = self.special_chars[t]
                        if isinstance(obj, tuple):
                            new_t, obj_type = obj
                            if not (a,b) in self.objs:
                                self.objs[(a,b)] = []
                            self.objs[(a,b)] = [(Obj(obj_type))] + self.objs[(a,b)]
                        else:
                            new_t = obj        
                    if new_t:
                        t = new_t
                        
        self.entry_locations = set(self.entries.values())
        
        self.real_tiles = ([''.join([self.get_tile(x, y) 
                            for x in range(len(self.tiles[y]))])
                            for y in xrange(len(self.tiles))])

        world.maps[self.name] = self

    def spawn_NPCs(self, world):
        for arg in self.NPCargs:
            new_npc = eval(arg)
            if new_npc.need_to_spawn:
                world.spawn(new_npc, self.name)
                new_npc.need_to_spawn = False
            self.NPCs.add(new_npc)
            
        # add character references to world characters dictionary        
        for npc in self.NPCs:
            world.spawn(npc, map_name=self.name)
            
        self.NPCs = None
        
    def add_to_map(self, char):
        self.characters[char.uid] = char
        self.add_to_zone(char, char._zone)
        char.map_name = self.name
    def remove_from_map(self, char):
        try:        
            del self.characters[char.uid]
            self.characters_by_zone[char._zone].discard(char.uid)
        except: pass
                                        
    def uncache(self):
        self._cached_tiles = {}
    def cache_tile(self, pos, t):
        self._cached_tiles[pos] = t

    def get_tile(self, x, y):
        try: 
            return self._cached_tiles[(x,y)]
        except KeyError:
            result = '0'
            if not (x < 0 or y < 0):
                try:
                    t = self.tiles[y][x]
                    last_t = None
                    while t in self.special_chars and not last_t == t:
                        last_t = t
                        t = self.special_chars[t]
                        if isinstance(t, tuple): t = t[0]
                    result = t
                except IndexError:
                    pass
                except TypeError:
                    pass
            self.cache_tile((x,y), result)
            return result
            
    def add_obj(self, position, obj):
        if not position in self.objs:
            self.objs[position] = []
        self.objs[position] = [obj] + self.objs[position]
            
    def get_objs(self, x, y):
        try:
            return self.objs[(x,y)]
        except:
            return []
            
    def movable(self, x, y, peers, char=None, flying=None):
        if char and char.position in self.entry_locations: return True
    
        occupied = False
        this_tile = self.get_tile(x, y)
        current_tile = 'none'
        if char:
            a, b = char.position
            current_tile = self.get_tile(a, b)
        
        if flying is None:
            try:
                flying = char.flying if char else False
            except:
                flying = False
                
        if not (this_tile in TILES_MOVABLE or (flying and this_tile in TILES_FLYABLE)):
            return False
        
        chars = peers
        for c in chars:
            try:
                cflying = c.flying if hasattr(c, 'flying') else False
                if c and c.position == (x, y) and (not hasattr(c, 'dead') or not c.dead) and not (cflying != flying):
                    if char and ((hasattr(c, 'attacking') and c.attacking) or (hasattr(char, 'attacking') and char.attacking == c.uid)):
                        return False
                    else:
                        pass
            except Exception as e: print e; raise
            
        water = False
        if (not occupied) and (x,y) in self.objs:
            movable = OBJS_MOVABLE + (OBJS_FLYABLE if flying else [])
            objs = self.objs[(x,y)]
            for obj in objs:
                if not obj.type in movable:
                    if obj.type == 'water':
                        water = True
                    else:                
                        return False
                        
        can_enter_water = (flying or this_tile in SWIMMING_ENTRANCES or current_tile in SWIMMING_ENTRANCES)
        swimming = char.swimming() if char else False
        in_water = char.in_water if char else False
        if char and water:
            if (not (char.swimming())) and (not ((in_water or can_enter_water))):
                return False
        elif char and char.swimming() and not can_enter_water:
            return False
                
        return True
        
    def add_to_zone(self, char, z):
        if z:
            if not z in self.characters_by_zone:
                self.characters_by_zone[z] = set()
            self.characters_by_zone[z].add(char.uid)
            char._zone = z            
    def remove_from_zone(self, char, z):
        if z:
            if not z in self.characters_by_zone:
                self.characters_by_zone[z] = set()
            self.characters_by_zone[z].discard(char.uid)
        
    def done_moving(self, char, world):
        char.in_water = self.in_water(char.position)
        '''for key in self.characters_by_zone:
            if char.uid in self.characters_by_zone[key]:
                char.damage_number(key)'''
        
        if zone(char.position) != char._zone:
            self.remove_from_zone(char, char._zone)
            self.add_to_zone(char, zone(char.position))
        
        if char.position in self.portals and not isinstance(char, NPC):
            portal = self.portals[char.position]
            world.move_to(char, portal.dest_map)
            char.position = world.get_map(portal.dest_map).entries[portal.dest_entry]
            
        if char.position in self.shops and not isinstance(char, NPC):
            shop = self.shops[char.position]
            char.shop(shop)
        elif char.shopping: char.shop(None)
        
        region = False
        x, y = char.position
        for region_name, (x1, x2, y1, y2) in self.regions.items():
            if x1 <= x <= x2 and y1 <= y <= y2:
                if region_name != char.region: char.region = region_name
                if region_name in self.region_music and self.region_music[region_name] != char.music:
                    char.music = self.region_music[region_name]
                region = True
                break
        if not region: 
            if not char.region is None: char.region = None
            if not char.music == self.music: char.music = self.music
        
    def in_water(self, position):
        try:
            for o in self.get_objs(position[0], position[1]):
                if o and o.type == 'water': return True
        except: return False
        return False
