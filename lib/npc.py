from mmorpg import *
from character import Character
from sprite import Sprite
from funcs import *
import random
import numpy as np


class NPC(Character):
    def __init__(self, name, colors, race, title, skills, position, world,
                 start_map=None, spawn=False, direction='d', 
                 items=None, enforcer=False, 
                 patrol=0, march=True, walk=2, wait=4, antagonism=False, persistence=1,
                 move_limit=None, defs=None, map_name=None, spawn_time=60, xp=0, spent_xp=0, gold=0, 
                 hostile=False, **kwargs):
        Character.__init__(self, name, colors, race, title, defs, skills=skills, items=items, spawn=False,
                           spawn_time=spawn_time, xp=xp, spent_xp=spent_xp, hostile=hostile, **kwargs)
        self.world = world
        
        if not position:
            self.map_name = map_name
            self.spawn()            
        else:
            self.direction = direction
            self.position = position
            self._zone = zone(position)
            # store original position, in case NPC moves
            self.original_position = self.position
            self.original_direction = direction
        self._FPS = FPS
        self.spawn_map = map_name
        self.need_to_spawn = spawn
        
        self.restore_hp(), self.restore_stamina()
        
        # enforcers will attack when you attack or steal from NPCs
        self._enforcer = enforcer
            
        self._antagonism = antagonism
        self._persistence = persistence
        self._patrol = patrol
        self._walk = walk
        self._wait = wait
        self._march = march
        self._move_cycle = self._walk
        self.gold = gold


    def max_stat_value(self, x):
        return Character.max_stat_value(self, x) * 2

    def after_attack(self, world):
        Character.after_attack(self, world)
        self.next_target(world)       
        
    def good_target(self, char):
        can_see = self.can_see(char)
        if self._enforcer and self.home_region in char._marks and can_see:
            return (np.inf, char)
        try:            
            if self._antagonism == False: return (0, None)
            if (self['Goodness'] < 0) == (char['Goodness'] < 0): return (0, None)
            antagonism = (self.level() - char.perceived_level()) + self._antagonism
            if antagonism < 0: return (0, None)
            if not (can_see) and not (char.attacking == self): return (0, None)
            antagonism -= self.distance(char)
            antagonism += char.damage(self)            
            return (antagonism ** 2, char)
        except:
            return (0, None)
            
    def message(self, txt):
        pass
            
    def being_attacked(self, by, to_help=None):
        if (not self.map_name in self._marks) and not isinstance(by, NPC):
            by._marks[self.map_name] = MARK_LENGTH
            by.changes['marks'] = by._marks
    
        #if not self.attacking: self.attack(by.uid, response=True)
        #chars = self.peers[:]
        #for c in chars:
        #    if not c.attacking:                
        #        if hasattr(c, 'enforcer') and (c._enforcer and c.abs_distance(self) <= c._enforcer or c.distance(by) <= c._enforcer):
        #            if (not to_help) or (to_help and not self.enmity(to_help)):
        #                c.attack(by.uid, response=True)
        #                c.being_attacked(by, to_help=self)
        
    def can_move(self, world, direction, change_direction=True, from_pos=None):
        if from_pos: x, y = from_pos
        else: x, y = self.position
        
        mx, my = move_directions[direction]
        dest_pos = (x+mx, y+my)
        dx, dy = dest_pos

        if self.offset == (0,0):
            if change_direction: self.direction = direction
            current_map = self.get_map(world)
            if (not (dx, dy) in current_map.portals and not (dx, dy) in current_map.entry_locations and
                current_map.movable(dx, dy, world.get_peers(self.uid), char=self)):
                return dest_pos, (mx, my)
        return False, dest_pos
        
    def can_see(self, target, mult=1):
        return ((self.region == target.region or not (self.region and target.region) or target.attacking) 
                 and Character.can_see(self, target, mult))
        
    def gain_xp(self, xp):
        pass
        
    def gain_gold(self, gold):
        pass

    def move_events(self, world, fps=None):
        if fps: self._FPS = fps
        #self.set_peers(world.get_peers(self.uid))
        
        if self.need_to_spawn:
            world.spawn(self, self.map_name)
                    
        if not self.dead:
            defs = self.defs
            
            self.run(False)
            
            if self.offset == (0,0):
                current_map = self.get_map(world)
                
                if self.attacking:
                    target = world.get_char_by_uid(self.attacking)
                    if self.can_see(target, self._persistence):
                        self.run()
                    else:
                        self.next_target(world)
                elif not self._patrol:
                    if hasattr(self, 'original_position'):
                        if self.position != self.original_position:
                            self.chase(world, self.original_position)
                        else:
                            self.direction = self.original_direction
                elif self.stamina >= self['Stamina'] / 4:
                    if self._move_cycle >= 0:
                        x, y = self.position
                        choices = [self.direction] * 2 + ['u','d','l','r']
                        if self.home_region:
                            x1, x2, y1, y2 = current_map.regions[self.home_region]
                            xlim, ylim = (x1,x2), (y1,y2)
                        else:
                            ylim = (0, len(current_map.tiles)-1)
                            xlim = (0, len(current_map.tiles[0])-1)
                        
                        if not (xlim[0] <= x <= xlim[1] and ylim[0] <= y <= ylim[1]):
                            self.chase(world, ((x1+x2)/2, (y1+y2)/2))
                        else:
                            direction = random.choice(choices)
                            self.move(world, direction, dontswim=(not hasattr(self, 'flyer') or not self.flyer))
                            self._move_cycle -= 1
                            if self._move_cycle <= 0:
                                if self._wait:
                                    self._move_cycle = self._wait * -1
                                else: self._move_cycle = self._walk
                    if self._move_cycle < 0:
                        self._move_cycle += self.move_speed()
                        if self._move_cycle >= 0:
                            self._move_cycle = self._walk
                
                if not self._patrol and not self.attacking and self._march and self.offset == (0,0):
                    self._move_cycle -= 1
                    
                    if self._move_cycle <= 0:
                        self.step = not self.step
                        self._move_cycle = 1. / self.move_speed()
                        
                if not self.attacking:
                    self.next_target(world)
        
        Character.move_events(self, world, fps)
        
    def run(self, running=True):
        if running and self.stamina < self['Stamina'] / 2:
            running = False
        Character.run(self, running)
        
    def die(self):
        gold = random_number(self.gold)
        self.gold += gold
        result = Character.die(self, gold=gold)
        self.penalty = 0
        return result
        
    def next_target(self, world):
        Character.next_target(self, world)


class WildNPC(NPC):
    def __init__(self, name, colors, race, title, skills, position, start_map=None,
                 items=None, enforcer=5, patrol=1, walk=1, wait=1, 
                 antagonism=5, move_limit=None, defs=None, map_name=None, hostile=True,
                 spawn_time=60, spent_xp=1000, **kwargs):
        NPC.__init__(self, name, colors, race, title, skills, position, start_map=start_map,
                     items=items, enforcer=enforcer, patrol=patrol,
                     walk=walk, wait=wait, antagonism=antagonism,
                     move_limit=move_limit, defs=defs, map_name=map_name,
                     spawn_time=spawn_time, spawn=True, spent_xp=spent_xp, hostile=hostile,
                     **kwargs)
                     
    def good_target(self, char):
        if isinstance(char, NPC): return (0, None)
        antagonism = (self.level() - 
                      (0 if hasattr(char, 'attacking') and char.attacking and char.attacking == self.uid else char.perceived_level()) + 
                      self._antagonism) if self.can_see(char) else 0
        return (antagonism, char)
