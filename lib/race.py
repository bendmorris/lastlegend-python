import collections

class Race(collections.namedtuple('Race', ['name', 'description', 'start_skills', 'potential_skills', 'disabled_skills', 'starting_items'])):
    def __new__(self, name, description, start_skills, potential_skills, disabled_skills, starting_items):
        return super(Race, self).__new__(self, name, description, start_skills, potential_skills, disabled_skills, starting_items)
