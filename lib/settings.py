class Settings:
    def __init__(self, graphics=2):
        import pygame.constants as pc

        self.joy_btns = {0: (ord('f'),''),
                         1: (pc.K_DELETE,''),
                         2: (pc.K_LSHIFT,''),
                         3: (pc.K_SPACE,''),
                         4: (pc.K_TAB,'',True),
                         5: (pc.K_TAB,''),
                         8: (pc.K_INSERT,'')
                         }
        self.GRAPHICS_QUALITY = graphics# 0 = retro, 1 = lores, 2 = hires
        self.MUSIC = True

        
    def get_doubles(self):
        return {0:0, 1:1, 2:4}[self.GRAPHICS_QUALITY]
    def get_smooth(self):
        return True if self.GRAPHICS_QUALITY >= 2 else False
        
    DOUBLES = property(get_doubles)
    SMOOTH = property(get_smooth)
