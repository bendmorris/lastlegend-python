import collections

class Shop(collections.namedtuple('Shop', ['name', 'for_sale', 'markup'])):
    def __new__(self, name, for_sale, markup=0):
        return super(Shop, self).__new__(self, name, for_sale, markup=0)
        
    def purchase_price(self, item, char):
        return int(int(char.solve_formula(item['Value'])) * (1 + self.markup))
        
    def buyback_price(self, item, char):
        return int(char.solve_formula(item['Value']) / 2)
        
    def buy_summary(self, char):
        return [(item, self.purchase_price(item, char)) for item in self.for_sale]
        
    def buy(self, item_name, char):
        for item in self.for_sale:
            if item.name == item_name:
                price = self.purchase_price(item, char)
                if char.gold >= price:
                    char.gold -= price
                    char.get_item(item_name)
                    char.message('You purchased <b>%s</b>.' % item_name)
                    return
    
    def sell(self, item_name, char):
        item = char.lose_item(item_name)
        if item:
            price = self.buyback_price(item, char)
            char.gold += price
            char.message('You sold your <b>%s</b>.' % item_name)
