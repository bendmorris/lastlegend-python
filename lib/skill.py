import collections

class Skill(collections.namedtuple('Skill', ['name', 'formula', 'description'])):
    def __new__(self, name, formula, description):
        return super(Skill, self).__new__(self, name, formula, description)
