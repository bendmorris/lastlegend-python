from funcs import *


sprite_values = set(['position',
                     'direction',
                     'dead',
                     'step',
                     'poison',
                     'hp',
                     'stamina',
                     'offset',
                     'invert',
                     'flying',
                     'attacking',
                     'attacking_with',
                     'attack_wait',
                     'attack_animation',
                     'attack_progress',
                     'short_summary',
                     'rgb',
                     'boss',
                     'hostile',
                     'speed',
                     ])
binary_vars = set(['dead', 'poison', 'invert'])

class Sprite(object):
    def __init__(self, char, explicit=False, data_dict=None):
        self.changes = {}
        if data_dict:
            self.__dict__ = data_dict
            return
        self.uid = char.uid
        self.pic = (char.race.name, char.colors)
        self.position = char.position
        self.direction = char.direction
        if char.dead: self.dead = char.dead if explicit else True
        if char.step: self.step = char.step
        if char.poison: self.poison = char.poison if explicit else True
        if char.hp: self.hp = round(float(char.hp) / char['Vitality'], 1)
        if char.stamina: self.stamina = round(float(char.stamina) / char['Stamina'], 1)
        ox, oy = char.bin_offset()
        if (ox,oy) != (0,0):
            self.offset_p = char.bin_offset()
            #self.offset = char.real_offset()
        if char.invert: self.invert = char.invert if explicit else True
        if char.flying: 
            self.flying = char.flying
            self.fly_offset = 1
        if char.attacking: self.attacking = char.attacking
        if char.attacking: self.attack_wait = round(char.attack_wait, 2)
        if char.attacking_with: self.attacking_with = char.attacking_with
        if char.attack_animation: self.attack_animation = char.attack_animation
        if char.attack_progress: self.attack_progress = char.attack_progress
        if char.hostile: self.hostile = 1
        self.speed = round(char.move_speed(False) * char.move_multiplier(), 4)
        self.short_summary = char.short_summary()
        self.rgb = char.rgb()
        if hasattr(char, 'boss') and char.boss: self.boss = char.boss
        self.damage_numbers = []
    def abs_offset(self):
        x, y = self.offset if hasattr(self, 'offset') else (0,0)
        if hasattr(self, 'flying') and self.flying:
            y -= (1 + smoothstep(self.fly_offset)) * TILE_SIZE
        return (x,y)
    def current_target(self, peers):
        if self.attacking:
            for c in peers:
                if c.uid == self.attacking:
                    return c
        return None
    def record_change(self, x, value):
        if x in binary_vars and value: value = True
        if x == 'offset': 
            value = tuple([1 if n > 0 else -1 if n < 0 else 0 for n in value])
            x = 'offset_p'
        if x in ('hp', 'stamina', 'attack_wait', 'speed') and value: value = round(value,2)
        if not hasattr(self, x) or getattr(self, x) != value or (value and isinstance(value, dict) or isinstance(value, list)):
            setattr(self, x, value)
            self.changes[x] = value
    def resolve_offset_p(self, fps):
        resolve_offset_p(self, fps)


def resolve_offset_p(self, fps):
    ifps = 1./fps
    
    if hasattr(self, 'offset_p') and self.offset_p and self.offset_p != (0,0):
        move_speed = self.speed / fps * BASE_FPS
        ox, oy = self.offset_p
        if ox < 0: ox += move_speed
        if ox > 0: ox -= move_speed
        if oy < 0: oy += move_speed
        if oy > 0: oy -= move_speed
        if abs(ox) < move_speed: ox = 0
        if abs(oy) < move_speed: oy = 0
        self.offset_p = (ox, oy)
        self.offset = real_offset(ox, oy)
    else:
        self.offset_p = (0,0)
        self.offset = (0,0)
    
    if hasattr(self, 'flying') and self.flying:
        if not hasattr(self, 'fly_direction') or not self.fly_direction:
            self.fly_direction = 1
            self.fly_offset = 0
            
        self.fly_offset = max(min(1, self.fly_offset + 1./(fps) * self.fly_direction), 0)
        if self.fly_offset >= 1 or self.fly_offset <= 0:
            self.fly_direction *= -1
    else:
        self.fly_offset = 0
        self.fly_direction = 0
        
    if hasattr(self, 'damage_numbers'):
        self.damage_numbers = [(n, 1) if not isinstance(n, tuple) else n
                                for n in self.damage_numbers]
        self.damage_numbers = [(n, duration - ifps)
                               for n, duration in self.damage_numbers
                               if duration - ifps >= 0]
    
    if hasattr(self, 'attack_progress') and self.attack_progress > 0:
        self.attack_progress -= ifps
        if self.attack_progress < 0: self.attack_progress = 0