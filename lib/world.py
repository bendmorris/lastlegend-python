try: 
    import numpy as np
except ImportError:
    import numpypy as np
from sprite import Sprite
from character import Character
from npc import NPC
from funcs import *
from mmorpg import *
import random
import os
import shutil
import cPickle as pickle


class World:
    writable_attributes = ['characters_by_uid', 'characters_by_name']

    def __init__(self):
        self.maps = {}
        self.characters_by_uid = {}
        self.characters_by_name = {}
        
        self.new_characters = []
        
        
    def clone(self):
        return self
    def get_char_by_uid(self, uid, sprite=False):
        result = self.characters_by_uid[uid]
        if sprite: return result.sprite()
        return result
    def get_char_by_name(self, name, sprite=False):
        result = self.characters_by_name[name]
        if sprite: return result.sprite()
        return result
    def do(self, uid, f, *args):
        if not f[0] == '_':
            return getattr(self.get_char_by_uid(uid), f)(*args)
    def execute(self, f, *args):
        if not f[0] == '_':
            return getattr(self, f)(*args)
    def get_map(self, map_name):
        return self.maps[map_name]
    def sprites(self, chars):
        return [c.sprite() for c in chars
                 if not c.dead]
    def get_peers(self, uid):        
        char = self.get_char_by_uid(uid)
        if not char._peers is None: return char._peers
        this_map = self.get_map(char.map_name)
        zx, zy = char._zone
        zones_to_look_at = [(x, y) for x in xrange(zx-1, zx+2)
                                   for y in xrange(zy-1, zy+2)]
        map_chars = this_map.characters_by_zone
        chars_to_look_at = [map_chars[zone] 
                            for zone in zones_to_look_at
                            if zone in map_chars]
        if not chars_to_look_at: return []
        chars_to_look_at = set.union(*chars_to_look_at)
        chars_to_look_at.discard(uid)
        result = [self.get_char_by_uid(u) for u in list(chars_to_look_at)]
        char._peers = result
        return result
    def get_targets(self, uid, target, distance):
        peers = self.get_peers(target.uid)
        return [target] + [p for p in peers 
                            if not (hasattr(p, 'dead') and p.dead) 
                            and not p.uid == uid
                            and target.distance(p) <= distance
                            ]
    def get_objs(self, uid):
        char = self.get_char_by_uid(uid)
        x, y = char.position
        v = VISION
        result = self.maps_dict[char.map_name].objs
        return {(a,b):v for (a,b),v in result.items() if distance(a,b,x,y) <= v}
    def add_to_map(self, map_name, char):
        this_map = self.get_map(map_name)
        this_map.add_to_map(char)
        if not char.uid in self.characters_by_uid:
            self.characters_by_uid[char.uid] = char
            print 'New %s: %s, %s' % ('NPC' if isinstance(char, NPC) else 'character', char, char.uid)
        if not char.name.lower() in self.characters_by_name:
            self.characters_by_name[char.name.lower()] = char
    def death_msg(self, uid, target_name):
        char = self.get_char_by_uid(uid)
        char.message("%s died." % target_name)
        char._kills.add(target_name)
    def give_xp(self, uid, lv, xp, max_xp):
        char = self.get_char_by_uid(uid)
        if char.level() > lv:
            diff = max(char.level() - lv - 1, 1)
            r = (1. / diff)
            xp = int(xp * r)
            max_xp = int(max_xp * r)
        if char['Loyalty']:
            loyalty = min(char['Loyalty'], 100) / 1000.
            xp = int(max(xp + max_xp*loyalty, max_xp))
        char.message("You gained <b>%s XP</b>." % (xp))
        char.gain_xp(xp)
        if char['Vampirism'] and not (hasattr(char, 'dead') and char.dead):
            vamp = random_number((((char['Vampirism']/3.) * char['Vitality']) ** 0.5) * (float(xp)/max_xp))
            char.change_hp(vamp)
            char.message("You absorbed <b>%s HP</b>." % vamp)

    def give_gold(self, uid, gold, max_gold):
        char = self.get_char_by_uid(uid)
        if char['Loyalty']:
            loyalty = min(char['Loyalty'], 100) / 100.
            gold = int(max(gold + max_gold*loyalty, max_gold))
        char.message("You gained <b>%s gold</b>." % (gold))
        char.gain_gold(gold)
        
    def remove_from_map(self, map_name, char):
        if map_name:
            self.get_map(map_name).remove_from_map(char)
        
    def spawn(self, char, map_name=None):
        if not map_name:
            if hasattr(char, 'spawn_map'):
                map_name = char.spawn_map
            else:
                map_name = 'cornell'
                
        spawn_map = self.maps_dict[map_name]
        #char.set_map(spawn_map)
        #char.set_peers([])
        #char.set_peers([Sprite(c) for c in self.characters[spawn_map.name].values()])
                
        if hasattr(char, 'original_position'):
            x, y = char.original_position
        else:
            if char.home_region:
                x1, x2, y1, y2 = spawn_map.regions[char.home_region]
                xlim, ylim = (x1,x2), (y1,y2)
            else:
                ylim = (0, len(spawn_map.tiles)-1)
                xlim = (0, len(spawn_map.tiles[0])-1)

            if hasattr(char, 'must_swim'): swim = None if not char.must_swim else True
            elif hasattr(char, 'swimmer'): swim = char.swimmer
            else: swim = False
            
            x, y = random.randint(xlim[0], xlim[1]), random.randint(ylim[0], ylim[1])
            while (not spawn_map.movable(x, y, char.peers.values())) or ((not swim is None) and spawn_map.in_water((x,y)) != swim):
                x, y = random.randint(xlim[0], xlim[1]), random.randint(ylim[0], ylim[1])
                #print char.name, char.home_region, x, y, spawn_map.in_water((x,y)), swimmer, spawn_map.movable(x, y, char.peers.values())
        
        char.tiles = []
        char.position = (x,y)
        self.move_to(char, map_name)
        
        char._zone = zone(char.position)
        spawn_map.add_to_map(char)
        spawn_map.done_moving(char, self)
        
        char.need_to_spawn = False
    
    def move_to(self, char, map_name):
        char.unhostile(self)

        self.remove_from_map(char.map_name, char)
        self.add_to_map(map_name, char)
        
        char.map_name = map_name
        this_map = self.maps_dict[map_name]
        char.music = this_map.music
        #if not char.tiles == this_map.real_tiles: char.tiles = this_map.real_tiles
        #if not char.objs == this_map.objs: char.objs = this_map.objs
            
    def new_character(self, *args, **kwargs):
        c = Character(*args, **kwargs)
        self.spawn(c)
        return c.uid

    def broadcast(self, sender, txt):
        char = self.get_char_by_uid(sender)
        # TO DO: sanitize output
        if txt[0] == '@':
            txt = txt[1:]
            try:
                sender, receiver, txt = sender, txt.split()[0], ' '.join(txt.split()[1:])
                self.tell(sender, receiver, txt)
                return
            except: pass
        elif txt[0] == ':': char.message('<i>%s %s</i>' % (char.name, txt[1:]))
        else: char.message('<i>You said, "%s"</i>' % txt)
        peers = self.get_peers(char.uid)
        if txt[0] == ':': msg = "<i>%s %s</i>" % (char.name, txt[1:])
        else: msg = '<i>%s said, "%s"</i>' % (char.name, txt)
        [peer.message(msg) for peer in peers if not peer.uid == sender]
        
    def tell(self, sender, receiver, txt):
        char = self.get_char_by_uid(sender)        
        try:
            receiver = self.characters_by_name[receiver.lower()]
            char.message('<i>You tell %s, "%s"</i>' % (receiver.name, txt))
            receiver.message('<i>%s tells you, "%s"</i>' % (char.name, txt))
        except KeyError:
            char.message("@%s isn't logged in." % receiver)
            
    def logout(self, uid):
        char = self.get_char_by_uid(uid)
        char.peers = {}
        char._last_peers = set()
        
    def save(self):
        #pickle.dump(self, open('world.new', 'w'), -1)
        #shutil.move('world.new', 'world.pkl')
        pass

    def load(self):
        pass
