#import pyximport; pyximport.install()
from mmorpg.app.main import launch_app
from mmorpg.lib.models import *
from mmorpg.scripts.parse_scripts import *
#import psyco
#psyco.full()


def main():
    launch_app()

if __name__ == '__main__':    
    main()
