map_script = '''%s
%s
'''

import sys
import os

if len(sys.argv) == 1:
    print '''Usage: python new_map.py map_name size fill_tile
example: python new_map.py meadow 20x20 .'''
    sys.exit()

map_name, size = sys.argv[1], sys.argv[2]
if len(sys.argv) > 3: empty_tile = sys.argv[3]
else: empty_tile = '0'
width, height = [int(x) for x in size.split('x')]

map = '\n'.join([(' %s' % empty_tile) * width for y in range(height)])

map = map_script % (map_name, map)
map_dir = os.path.dirname(__file__)

output_file = os.path.join(map_dir, map_name + '.map')
f = open(output_file, 'w')
f.write(map)
f.close()
