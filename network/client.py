import time
from mmorpg import *
from mmorpg.lib.models import *
from twisted.internet.protocol import ClientFactory, Protocol
from twisted.protocols.basic import LineOnlyReceiver as LineReceiver
from twisted.internet import reactor, task
from data import parse, dump, encode, decode


class GameClient(LineReceiver):
    delimiter=MSG_END
    MAX_LENGTH=MAX_MSG_LENGTH
    def __init__(self, character):
        self.character = character
        
    def connectionMade(self):
        self.update_task = task.LoopingCall(self.update_request)
        self.update_task.start(1. / UPDATE_FPS)

        self.draw_task = task.LoopingCall(self.draw)
        self.draw_task.start(1. / FPS)

    def draw(self):
        reactor.callLater(0, self.controller.update)
        
    def update_request(self):
        if self.controller.done:     
            reactor.stop()
            import pygame; pygame.quit()
            return
        reactor.callLater(0, self.controller.update)
        self.send_data(MSG_UPDATE, self.character.uid)
        
    def lineReceived(self, data):
        cmd = data[0]
        data = decode(data[1:])
        if cmd == MSG_CHAR:
            self.character.__dict__ = parse(data)

        elif cmd == MSG_UPDATE:
            changes = parse(data)
            if 'messages' in changes:
                self.controller.message(changes['messages'])
                del changes['messages']
            if 'damage_numbers' in changes:
                self.character.damage_numbers += [(n, 1) for n in changes['damage_numbers']]
                del changes['damage_numbers']
            if 'items' in changes:
                self.character.items = [ItemCopy(self.character.defs['items_dict'][item], equipped, quantity)
                                        for item, equipped, quantity in changes['items']]
                del changes['items']
            if 'tiles' in changes:
                self.character.tiles = changes['tiles'].split('\n') if isinstance(changes['tiles'], str) else changes['tiles']
                del changes['tiles']
            if 'peer_updates' in changes:
                for uid, value in changes['peer_updates'].items():
                    if uid in self.character.peers:
                        peer = self.character.peers[uid]
                        if 'damage_numbers' in value:
                            peer.damage_numbers += [(n, 1) for n in value['damage_numbers']]
                            del value['damage_numbers']
                        peer.__dict__.update(value)
                for uid in set.difference(set(self.character.peers), set(changes['peer_updates'])):
                    del self.character.peers[uid]
            if 'old_peers' in changes:
                for peer in changes['old_peers']:
                    if peer in self.character.peers:
                        del self.character.peers[peer]
            if 'new_peers' in changes:
                for peer in changes['new_peers']:
                    self.character.peers[peer['uid']] = Sprite(None, data_dict=peer)
            if 'need_uncache' in changes:
                self.character.uncache()
            if 'offset' in changes:
                self.character.offset_p = changes['offset']
                del changes['offset']
            self.character.__dict__.update(changes)

                
        elif cmd == MSG_OBJS:
            self.character.map.objs = parse(data)
        
    def send_data(self, cmd, data):
        self.sendLine(cmd + encode(data, compress=False))


class GameClientFactory(ClientFactory):
    def __init__(self, controller):
        self.controller = controller

    def buildProtocol(self, addr):
        self.protocol = GameClient(self.character)
        self.protocol.controller = self.controller
        return self.protocol
        
        
class Client:
    def __init__(self, controller):
        self.factory = GameClientFactory(controller)
        
    def start(self):
        self.factory.character = self.character
    
        reactor.connectTCP(HOST, PORT, self.factory)

    def do(self, f, *args):
        p = self.factory.protocol
        if p: reactor.callLater(0, p.send_data, MSG_ACT, dump((p.character.uid, f, args)))
    def execute(self, f, *args):
        p = self.factory.protocol
        if p: p.send_data(MSG_WORLD, dump((f, args)))
