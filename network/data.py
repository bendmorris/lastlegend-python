import cPickle as pickle
from pickletools import optimize
import zlib
import base64
import StringIO
import sys
from mmorpg.lib.models import *
import numpy as np

SAFE = True


def parse(data):
    return Unpickler.loads(data)
def dump(data):
    return optimize(pickle.dumps(data, -1))
def encode(data, compress=True):
    if compress: return zlib.compress(data,9)
    return (data)
def decode(data, compress=True):
    if compress: return zlib.decompress(data)
    return (data)
    
    
    
class SafeUnpickler(object):
    @classmethod
    def find_class(cls, module, name):
        if name == 'eval':
            raise pickle.UnpicklingError('No eval allowed!')
        elif '__builtin__' in module or '__builtins__' in module:
            if not name in set(['object', 'set', 'property']):
                raise pickle.UnpicklingError(
                    'Unsafe class: %s' % name
                )
        elif module.startswith('mmorpg') or module.startswith('numpy') and not '_' in module:
            pass
        else:
            raise pickle.UnpicklingError(
                'Unsafe module: %s' % module
            )
        #__import__(module)
        return getattr(sys.modules[module], name)

    @classmethod
    def loads(cls, pickle_string):
        pickle_obj = pickle.Unpickler(StringIO.StringIO(pickle_string))
        pickle_obj.find_global = cls.find_class
        return pickle_obj.load()
        
        
Unpickler = SafeUnpickler if SAFE else pickle
