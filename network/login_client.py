from twisted.internet.protocol import ClientFactory, Protocol
from twisted.protocols.basic import LineReceiver
from twisted.internet import reactor, task
from data import parse, dump, encode, decode
from mmorpg import *
from mmorpg.lib.models import ShellCharacter
from mmorpg.app.controller import Controller
from mmorpg.network.client import Client
from mmorpg.scripts.parse_scripts import defs
import random


class LoginClient(LineReceiver):
    delimiter=MSG_END
    MAX_LENGTH=MAX_MSG_LENGTH
    def __init__(self, msg, menu):
        self.msg = msg
        self.character = None
        self.menu = menu
        
    def connectionMade(self):
        print "Logging in..."
        self.log_in()
        
    def lineReceived(self, data):
        cmd = data[0]
        data = decode(data[1:])
        settings = self.menu.settings
        tilesets = self.menu.tilesets
        screen = self.menu.screen
        if cmd == MSG_CHAR:
            character = ShellCharacter(parse(data))
            print "Login complete."
            
            controller = Controller(settings)
            controller.character = character
            

            client = Client(controller)
            client.character = character
            controller.client = client
            controller.tilesets = tilesets
            controller.screen = screen
            controller.start()
            client.start()
            
        
    def send_data(self, cmd, data):
        self.sendLine(cmd + encode(data, compress=False))
        #self.transport.flush()
        #self.transport.doWrite()
        
    def log_in(self):
        msg = self.msg
        self.send_data(MSG_NEW, str(msg))


class LoginClientFactory(ClientFactory):
    def buildProtocol(self, addr):
        self.protocol = LoginClient(self.msg, self.menu)
        return self.protocol
        
        
def login():
    #app = wx.PySimpleApp()

    #new_character = NewCharacterDialog(None, "New Character", defs)
    #new_character.ShowModal()
    
    #app.Exit()
    
    from mmorpg.app.main_menu import main
    result = main()
    
    if not result: return
    msg, menu = result

    factory = LoginClientFactory()
    factory.msg = msg
    factory.menu = menu
    del menu
    reactor.connectTCP(HOST, PORT, factory)
    #reactor.registerWxApp(wx.GetApp())
    reactor.run()
