from mmorpg import *
from mmorpg.lib.map import *
from mmorpg.lib.models import *
from mmorpg.scripts.parse_scripts import get_world, defs
import os
import cPickle as pickle
import shelve
if os.path.exists('world.dump'):
    print "Loading server state...",
    world = pickle.load(open('world.dump', 'rb'))
    print "done."
else:
    world = get_world()
from data import parse, dump, encode, decode
from twisted.internet.protocol import Protocol, Factory
from twisted.protocols.basic import LineOnlyReceiver as LineReceiver
from twisted.internet.endpoints import TCP4ServerEndpoint
from twisted.internet import reactor, task, threads
import time
import sys
reload(sys)
sys.setdefaultencoding('latin1')
import random
#from guppy import hpy
#h = hpy()


class Server(LineReceiver):
    delimiter=MSG_END
    MAX_LENGTH=MAX_MSG_LENGTH
    def __init__(self, factory, world, defs):
        self.factory = factory
        self.world = world
        self.defs = defs
        
    def connectionMade(self):
        self.factory.numProtocols += 1

    def connectionLost(self, reason):
        self.factory.numProtocols -= 1

    def lineReceived(self, data):
        data = data.strip()
        if data:
            cmd = data[0]
            if len(data) > 1:
                data = decode(data[1:], compress=False)
            else: data = ''
                
            if cmd == MSG_NEW:
                data = data.split(',')
                new_char = int(data[0])
                if new_char:
                    new_char, email, password, name, colors, race, title, tutorial = data
                    tutorial = int(tutorial)
                    colors = int(colors)
                    if new_char:
                        new_uid = self.world.new_character(name, colors, race, title, self.defs, 
                                                           password=password, email=email,
                                                           spawn_map='tutorial' if tutorial else 'cornell',
                                                           home_region='start_room' if tutorial else 'cornell_town')
                        char = self.world.get_char_by_uid(new_uid)
                        char.gold = 600
                else:
                    new_char, name, password = data
                    char = self.world.get_char_by_name(name.lower())
                    if not char.password == password: return
                    
                char_dict = char.__dict__.copy()
                char_dict.update({'changes': {}})
                response = dump(char_dict)
                pickle.dump(char_dict, open('char_dump.txt', 'wb'), -1)
                pickle.dump(char.sprite(), open('sprite_dump.txt', 'wb'), -1)
                self.send_data(MSG_CHAR, response)
            elif cmd == MSG_CHAR:
                uid = data
                char = self.world.get_char_by_uid(uid)
                
                response = dump(char.__dict__)
                self.send_data(cmd, response)
            elif cmd == MSG_UPDATE:
                uid = data
                
                char = self.world.get_char_by_uid(uid)
                changes = char.changes
                #if changes: print changes
                
                peers = self.world.get_peers(uid)
                peer_uids = set([p.uid for p in peers])
                intersection = set.intersection(char._last_peers, peer_uids)
                new_sprites = set.difference(peer_uids, char._last_peers)
                old_sprites = set.difference(char._last_peers, peer_uids)
                changes['peer_updates'] = {}
                for sprite in list(intersection):
                    watching_char = self.world.get_char_by_uid(sprite)
                    this_sprite = watching_char.sprite_for(uid)
                    these_changes = this_sprite.changes
                    changes['peer_updates'][sprite] = these_changes
                    this_sprite.changes = {}
                new_peers = [self.world.get_char_by_uid(sprite).sprite_for(uid).__dict__ 
                             for sprite in list(new_sprites)]
                if new_peers: changes['new_peers'] = new_peers
                for sprite in list(old_sprites):
                    this_char = self.world.get_char_by_uid(sprite)
                    if this_char and uid in this_char._sprites:
                        del this_char._sprites[uid]
                
                char._last_peers = peer_uids
                
                if 'items' in changes: 
                    changes['items'] = [(item.item.name, item.equipped, item.quantity) for item in char.items]
                msgs = char.read_messages()
                if msgs: changes['messages'] = msgs
                dmgs = char.read_damage_numbers()
                if dmgs: changes['damage_numbers'] = dmgs
                char.changes = {}
                #if changes: print changes
                
                response = dump(changes)
                self.send_data(cmd, response)
            elif cmd == MSG_OBJS:
                uid = data
                objs = self.world.get_objs(uid)
                response = dump(objs)
                self.send_data(cmd, response)
            elif cmd == MSG_ACT:
                uid, f, args = parse(data)
                try:
                    self.world.do(uid, f, *args)
                except Exception as e:
                    print "EXCEPTION", e
            elif cmd == MSG_WORLD:
                f, args = parse(data)
                try:
                    self.world.execute(f, *args)
                except Exception as e:
                    print "EXCEPTION", e

    
    def send_data(self, cmd, data):
        #print data
        to_send = str(cmd) + encode(data)
        self.factory.sent_data += len(to_send)
        #print 'Sent data, size:', len(to_send)
        self.sendLine(to_send)


class ServerFactory(Factory):
    def __init__(self, world, defs):
        self.world = world
        self.defs = defs
        self.numProtocols = 0
        self.sent_data = 0
        
        self.last_move_events = time.time()
        self.move_events_task = task.LoopingCall(self.moveEvents)
        self.move_events_task.start(1. / UPDATE_FPS)

        self.write_task = task.LoopingCall(self.start_write)
        self.write_task.start(WRITE_FREQUENCY)
        
        self.update_task = task.LoopingCall(self.sent_data_update)
        self.last_updated = time.time()
        self.update_task.start(10)
        
    def moveEvents(self):
        try:
            fps = 1. / (time.time() - self.last_move_events)
            self.last_move_events = time.time()
        
            #print 'move events: fps = %s' % fps
            self.last_time = time.time()
            chars = self.world.characters_by_uid.values()
            for c in chars: c.move_events(self.world, fps)
        except ZeroDivisionError:
            pass

    def buildProtocol(self, addr):
        return Server(self, self.world, self.defs)

    def start_write(self):
        threads.deferToThread(self.write_state)

    def write_state(self):
        print "Writing state...",
        t = time.time()
        self.world.save()
        print "took %s seconds." % (time.time() - t)
        
    def sent_data_update(self):
        t = time.time() - self.last_updated
        self.last_updated += t
        sent = self.sent_data
        self.sent_data -= sent
        if t > 0:
            rate = sent / t
            if rate > 0:
                print 'Data rate: %s kb/s' % round(rate/1000.,2)
        #print h.heap()

    def stopFactory(self):
        pass
        
        
def run_server():
    endpoint = TCP4ServerEndpoint(reactor, PORT)
    endpoint.listen(ServerFactory(world, defs))
    print "Running server"
    reactor.run()
