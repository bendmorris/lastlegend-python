Provisions: Basic supplies to restore stamina.
    type: Usable
    Target: Self
    Stamina: 20 + <Cooking>*5
    Value: 5
Ale: Restores all stamina; can affect HP.
    type: Usable
    Target: Self
    HP: -10 + <Cooking>*2
    Stamina: <Stamina>
    Value: 20
Chicken: Restores HP.
    type: Usable
    Target: Self
    HP: 30 + <Cooking>*5
    Value: 25
Mutton: Restores HP.
    type: Usable
    Target: Self
    HP: 50 + <Cooking>*5
    Value: 40
Raw Meat: Restores all of your HP and stamina. Must be cooked first.
    type: Usable
    Target: Self
    HP: <Vitality> if <Cooking> >= 5 else -<Vitality>/2
    Stamina: <Stamina>
    Value: 50
Fruit: Restores HP and stamina.
    type: Usable
    Target: Self
    HP: 5 + <Cooking>
    Stamina: 15 + <Cooking>
    Value: 20
Vegetables: Restores HP and stamina.
    type: Usable
    Target: Self
    HP: 5 + <Cooking>
    Stamina: 20 + <Cooking>
    Value: 25
Medical Kit: Heal HP of self or ally.
    type: Usable
    Target: Any
    Requires: <Healing>
    HP: 25 + <Healing>*5
    Value: 50
Medicinal Potion: Heal HP of self or ally.
    type: Usable
    Target: Any
    Requires: <Healing> >= 10
    HP: 50 + <Healing>*5
    Value: 100
Energy Potion: Heal stamina of self or ally.
    type: Usable
    Target: Any
    Requires: <Healing> >= 10
    Stamina: 25 + <Healing>*5
    Value: 100
Rejuvenating Potion: Heal HP and stamina of self or ally.
    type: Usable
    Target: Any
    Requires: <Healing> >= 20
    HP: 25 + <Healing>*5
    Stamina: 25 + <Healing>*5
    Value: 150
Pine Staff
    type: Equippable, Weapon, Staff
    Space: Main Hand, Off Hand
    weight: 1
    Damage: 6
    Base: <Strength>
    Skill: <Weapon:Staff>
    Magic: 4
    Range: 1
    image: staff
    Value: 150
Oak Staff
    type: Equippable, Weapon, Staff
    Space: Main Hand, Off Hand
    weight: 1
    Damage: 10
    Base: <Strength>
    Skill: <Weapon:Staff>
    Magic: 6
    Range: 1
    image: staff
    Value: 800
Elemental Staff
    type: Equippable, Weapon, Staff
    Space: Main Hand, Off Hand
    weight: 1
    Damage: 14
    Base: <Strength>
    Skill: <Weapon:Staff>
    Magic: -4
    Sorcery: 4
    Flurry: 10
    Quickcast: 10
    Range: 2
    Attack Cost: -1
    image: staff
    Value: 2400
Initiate's Orb
    type: Equippable
    Space: Orb
    Requires: <Magic> >= 20 and <Equipped:Staff>
    Attack Cost: -2
    Magic: 2
    Endurance: 2
    Quickcast: 2
    Value: 500
Novice's Orb
    type: Equippable
    Space: Orb
    Requires: <Magic> >= 30 and <Equipped:Staff>
    Attack Cost: -4
    Magic: 4
    Endurance: 4
    Quickcast: 4
    Value: 1500
Mage's Orb
    type: Equippable
    Space: Orb
    Requires: <Magic> >= 50 and <Equipped:Staff>
    Attack Cost: -6
    Magic: 6
    Endurance: 6
    Quickcast: 6
    Value: 3500
Master's Orb
    type: Equippable
    Space: Orb
    Requires: <Magic> >= 80 and <Equipped:Staff>
    Attack Cost: -8
    Magic: 10
    Endurance: 10
    Quickcast: 10
    Value: 6500
Shortsword
    type: Equippable, Weapon, Sword
    Space: Main Hand
    weight: 3
    Damage: 8
    Base: <Strength>
    Skill: <Weapon:Sword>
    Range: 1
    Value: 200
    image: sword
Venom Shortsword
    type: Equippable, Weapon, Sword
    Space: Main Hand
    weight: 3
    Damage: 8
    Base: <Strength>
    Skill: <Weapon:Sword>
    Range: 1
    Poisoning: 20
    Value: 400
    image: sword
Rapier
    type: Equippable, Weapon, Sword
    Space: Main Hand
    weight: 3
    Damage: 12
    Base: (<Strength> + <Speed>)/2
    Skill: <Weapon:Sword>
    Range: 1
    Precision: 2
    Evasion: 2
    Value: 650
    image: sword
Scimitar
    type: Equippable, Weapon, Sword
    Space: Main Hand
    weight: 3
    Damage: 14
    Endurance: -2
    Base: <Strength>
    Skill: <Weapon:Sword>
    Range: 1
    Value: 1300
    image: sword
Longsword
    type: Equippable, Weapon, Sword
    Space: Main Hand
    weight: 3
    Damage: 18
    Endurance: -2
    Base: <Strength>
    Skill: <Weapon:Sword>
    Range: 1
    Value: 1700
    image: sword
Greatsword
    type: Equippable, Weapon, Sword
    Space: Main Hand, Off Hand
    Requires: <Two-Handed>
    weight: 3
    Damage: 20
    Endurance: -4
    Base: <Strength>
    Skill: <Weapon:Sword>
    Range: 1
    Piercing: 4
    Knockback: 4
    Value: 2200
    image: longsword
Excaliber
    type: Equippable, Weapon, Sword
    Space: Main Hand
    Requires: <Race:Human>
    weight: 3
    Damage: 24
    Endurance: -2
    Base: <Strength>
    Skill: <Weapon:Sword>
    Range: 1
    Defense: 10
    Spell Resist: 10
    Bloodlust: 10
    Piercing: 10
    Knockback: 10
    Value: 5000
    image: longsword
Katana
    type: Equippable, Weapon, Sword, Katana
    Space: Main Hand
    weight: 3
    Damage: 9
    Endurance: -1
    Base: (<Strength> + <Speed>)/2
    Skill: <Weapon:Sword>
    Range: 1
    Value: 700
    image: sword
Wakizashi
    type: Equippable, Weapon, Sword
    Space: Off Hand
    Requires: <Equipped:Katana>
    weight: 3
    Damage: 9
    Endurance: -1
    Base: (<Strength> + <Speed>)/2
    Skill: <Weapon:Sword>
    Range: 1
    Precision: 6
    Evasion: 10
    Flurry: 10
    Value: 800
    image: sword
Leather Scabbard
    type: Equippable
    Space: Scabbard
    Requires: <Equipped:Sword>
    weight: 3
    Damage: 1
    Defense: 1
    Evasion: 4
    Flurry: 4
Short Dagger
    type: Equippable, Weapon, Knife
    Space: Main Hand
    weight: 1
    Damage: 6
    Base: (<Strength> + <Speed> + <Cunning>)/2
    Skill: <Weapon:Knife>
    Range: 1
    Piercing: 2
    Value: 180
    image: dagger
Long Dagger
    type: Equippable, Weapon, Knife
    Space: Main Hand
    weight: 2
    Damage: 8
    Base: (<Strength> + <Speed> + <Cunning>)/2
    Skill: <Weapon:Knife>
    Range: 1
    Piercing: 4
    Value: 350
    image: dagger
Concealed Knife
    type: Equippable
    Space: Off Hand
    Requires: <Cunning> and <Equipped:Knife>
    weight: 2
    Damage: 2
    Critical: 10
    Piercing: 2
    Cunning: 8
    Value: 250
Throwing Knives
    type: Equippable, Weapon
    Space: Main Hand, Off Hand
    weight: 2
    Damage: 4 + <Cunning>/2
    Base: <Speed>
    Skill: <Weapon:Knife>
    Range: 4
    Piercing: 6
    Value: 500
    image: dagger
    animation_type: projectile
Short Spear
    type: Equippable, Weapon, Spear
    Space: Main Hand
    weight: 3
    Damage: 7
    Base: <Strength>
    Skill: <Weapon:Spear>
    Range: 2
    Piercing: 2
    Value: 200
    image: spear
Barbed Spear
    type: Equippable, Weapon, Spear
    Space: Main Hand
    weight: 3
    Damage: 11
    Base: <Strength>
    Skill: <Weapon:Spear>
    Range: 2
    Piercing: 5
    Bloodlust: 5
    Value: 600
    image: spear
Iron Lance
    type: Equippable, Weapon, Spear
    Space: Main Hand
    weight: 3
    Damage: 9
    Base: <Strength>
    Skill: <Weapon:Spear>
    Range: 2
    Piercing: 8
    Value: 200
    image: spear
Steel Lance
    type: Equippable, Weapon, Spear
    Space: Main Hand
    weight: 3
    Damage: 13
    Endurance: -2
    Base: <Strength>
    Skill: <Weapon:Spear>
    Range: 2
    Piercing: 10
    Value: 200
    image: spear
Heavy Lance
    type: Equippable, Weapon, Spear
    Space: Main Hand
    Requires: <Two-Handed>
    weight: 3
    Damage: 19
    Endurance: -4
    Base: <Strength>
    Skill: <Weapon:Spear>
    Range: 2
    Piercing: 16
    Knockback: 4
    Value: 2200
    image: spear
Pine Javelin
    type: Equippable, Weapon, Javelin
    Space: Main Hand, Off Hand
    weight: 2
    Damage: 6
    Base: <Strength>
    Skill: <Weapon:Javelin>
    Range: 4
    Piercing: 1
    Value: 250
    image: spear
    animation_type: projectile
Iron Javelin
    type: Equippable, Weapon, Javelin
    Space: Main Hand, Off Hand
    weight: 2
    Damage: 14
    Base: <Strength>
    Skill: <Weapon:Javelin>
    Range: 4
    Piercing: 6
    Value: 800
    image: spear
    animation_type: projectile
Steel Javelin
    type: Equippable, Weapon, Javelin
    Space: Main Hand, Off Hand
    weight: 2
    Damage: 18
    Base: <Strength>
    Skill: <Weapon:Javelin>
    Range: 4
    Piercing: 12
    Value: 1150
    image: spear
    animation_type: projectile
Small Axe
    type: Equippable, Weapon, Axe
    Space: Main Hand
    weight: 3
    Damage: 9
    Base: <Strength>
    Skill: <Weapon:Axe>
    Range: 1
    Value: 220
    image: axe
Stone Axe
    type: Equippable, Weapon, Axe
    Space: Main Hand
    weight: 3
    Damage: 12
    Base: <Strength>
    Skill: <Weapon:Axe>
    Range: 1
    Value: 700
    image: axe
Mattock
    type: Equippable, Weapon, Axe
    Space: Main Hand
    Requires: <Race:Dwarf,Goblin>
    weight: 3
    Damage: 11
    Base: <Strength>
    Skill: <Weapon:Axe>
    Range: 2
    Piercing: 20
    Critical: 10
    Bloodlust: 5
    Loyalty: 5
    Swarming: 5
    Two-Handed: 10
    Flurry: 10
    Value: 750
    image: mattock
    animation_type: projectile
Battle Axe
    type: Equippable, Weapon, Axe
    Space: Main Hand
    weight: 3
    Damage: 16
    Endurance: -2
    Base: <Strength>
    Skill: <Weapon:Axe>
    Range: 1
    Value: 1200
    image: axe
Lode Axe
    type: Equippable, Weapon, Axe
    Space: Main Hand
    Requires: <Two-Handed>
    weight: 3
    Damage: 21
    Endurance: -4
    Base: <Strength>
    Skill: <Weapon:Axe>
    Range: 1
    Knockback: 6
    Two-Handed: 6
    Piercing: 2
    Value: 2400
    image: axe
Small Club
    type: Equippable, Weapon, Club
    Space: Main Hand, Off Hand
    Requires: <Two-Handed>
    weight: 3
    Stamina Cost: 1
    Damage: 16
    Endurance: -1
    Base: <Strength>
    Skill: <Weapon:Club>
    Range: 1
    Knockback: 2
    Flurry: -6
    Evasion: -6
    Value: 250
    image: club
Pine Club
    type: Equippable, Weapon, Club
    Space: Main Hand, Off Hand
    Requires: <Two-Handed>
    weight: 3
    Stamina Cost: 2
    Damage: 21
    Endurance: -2
    Base: <Strength>
    Skill: <Weapon:Club>
    Range: 1
    Knockback: 4
    Flurry: -8
    Evasion: -8
    Value: 750
    image: club
Spruce Club
    type: Equippable, Weapon, Club
    Space: Main Hand, Off Hand
    Requires: <Two-Handed>
    weight: 3
    Stamina Cost: 3
    Damage: 26
    Endurance: -3
    Base: <Strength>
    Skill: <Weapon:Club>
    Range: 1
    Knockback: 6
    Flurry: -10
    Evasion: -10
    Value: 1250
    image: club
Oak Club
    type: Equippable, Weapon, Club
    Space: Main Hand, Off Hand
    Requires: <Two-Handed>
    weight: 3
    Stamina Cost: 4
    Damage: 31
    Endurance: -4
    Base: <Strength>
    Skill: <Weapon:Club>
    Range: 1
    Knockback: 10
    Flurry: -10
    Evasion: -10
    Value: 2250
    image: club
Shortbow
    type: Equippable, Weapon, Bow
    Space: Main Hand, Off Hand
    weight: 2
    Damage: 6
    Base: <Speed>
    Skill: <Weapon:Bow>
    Range: 5
    Piercing: 4
    Flurry: 4
    Value: 200
    image: arrow
    animation_type: projectile
Pine Longbow
    type: Equippable, Weapon, Bow
    Space: Main Hand, Off Hand
    weight: 2
    Damage: 9
    Base: <Speed>
    Skill: <Weapon:Bow>
    Range: 5
    Piercing: 4
    Flurry: 4
    Value: 500
    image: arrow
    animation_type: projectile
Stealthbow
    type: Equippable, Weapon, Bow
    Space: Main Hand, Off Hand
    Requires: <Cunning> and <Race:Elf>
    weight: 2
    Damage: 10
    Base: <Speed>
    Skill: <Weapon:Bow>
    Range: 5
    Piercing: 4
    Cunning: 10
    Evasion: 10
    Flurry: 4
    Value: 200
    image: arrow
    animation_type: projectile
    animation_elevate: 0.5
    animation_trace: smokeball
Spruce Longbow
    type: Equippable, Weapon, Bow
    Space: Main Hand, Off Hand
    weight: 2
    Damage: 11
    Base: <Speed>
    Skill: <Weapon:Bow>
    Range: 6
    Piercing: 6
    Flurry: 4
    Value: 1200
    image: arrow
    animation_type: projectile
Speedy Longbow
    type: Equippable, Weapon, Bow
    Space: Main Hand, Off Hand
    weight: 2
    Damage: 13
    Base: <Speed>
    Skill: <Weapon:Bow>
    Range: 6
    Piercing: 8
    Speed: 4
    Flurry: 4
    Value: 2000
    image: arrow
    animation_type: projectile
Takedown Recurve
    type: Equippable, Weapon, Bow
    Space: Main Hand, Off Hand
    weight: 2
    Damage: 15
    Base: <Speed>
    Skill: <Weapon:Bow>
    Range: 7
    Piercing: 10
    Flurry: 4
    Speed: 2
    Value: 2500
    image: arrow
    animation_type: projectile
Crossbow
    type: Equippable, Weapon, Crossbow
    Space: Main Hand, Off Hand
    weight: 2
    Stamina Cost: 1
    Damage: 18
    Base: <Speed>
    Skill: <Weapon:Bow>
    Range: 6
    Speed: 4
    Endurance: 4
    Precision: 4
    Piercing: 20
    Flurry: 10
    Value: 4500
    image: arrow
    animation_type: projectile
Leather Armguard
    type: Equippable
    Space: Forearm
    Requires: <Equipped:Bow>
    weight: 2
    Defense: 1
    Precision: 4
    Piercing: 4
    Flurry: 4
    Value: 500
Bowstring Sight
    type: Equippable
    Space: Bowstring
    Requires: <Equipped:Bow>
    weight: 2
    Range: 1
    Precision: 20
    Piercing: <Range>
    Critical: <Range>
    Flurry: -4
    Value: 500
Light Arrows
    type: Equippable, Arrow
    Space: Arrow
    Requires: <Flurry> and <Equipped:Bow>
    weight: 2
    Attack Cost: 1
    Damage: -2
    Piercing: -5
    Flurry: 25
    Value: 400
Elven Arrows
    type: Equippable, Arrow
    Space: Arrow
    Requires: <Flurry> and <Equipped:Bow> and <Race:Elf,Centaur>
    weight: 2
    Attack Cost: 1
    Damage: 1
    Precision: 10
    Piercing: 10
    Flurry: 10
    Value: 500
Centaur Arrows
    type: Equippable, Arrow
    Space: Arrow
    Requires: <Flurry> and <Equipped:Bow> and <Race:Centaur>
    weight: 2
    Attack Cost: 1
    Damage: 2
    Precision: 15
    Piercing: 10
    Flurry: 15
    Running: 10
    Value: 600
Blunt Arrows
    type: Equippable, Arrow
    Space: Arrow
    Requires: <Knockback> and <Equipped:Bow>
    weight: 2
    Attack Cost: 2
    Damage: 2
    Knockback: 20
    Value: 600
Poison Arrows
    type: Equippable, Arrow
    Space: Arrow
    Requires: <Poisoning> and <Equipped:Bow>
    weight: 2
    Attack Cost: 2
    Damage: 3
    Poisoning: 20
    Value: 750
Piercing Arrows
    type: Equippable, Arrow
    Space: Arrow
    Requires: <Piercing> and <Equipped:Bow>
    weight: 2
    Attack Cost: 3
    Damage: 5
    Precision: 5
    Piercing: 20
    Value: 900
Quiver
    type: Equippable
    Space: Quiver
    Requires: <Equipped:Arrow>
    Endurance: 2
    Flurry: 5
    Value: 250
Fletching Kit
    type: Equippable
    Space: Tool
    Requires: <Equipped:Arrow>
    Attack Cost: -1
    Value: 250
Slingshot
    type: Equippable, Weapon
    Space: Main Hand, Off Hand
    weight: 1
    Damage: 4
    Base: <Speed>
    Skill: <Precision>
    Range: 4
    Flurry: 5
    Value: 300
    image: pebble
    animation_type: projectile
Boulder
    type: Equippable, Weapon
    Space: Main Hand, Off Hand
    Requires: <Two-Handed>
    weight: 3
    Stamina Cost: 4
    Endurance: -4
    Damage: 25
    Base: <Strength>
    Skill: <Two-Handed>
    Range: 4
    Piercing: 10
    Knockback: 40
    Speed: -30    
    Flying: -20
    Swimming: -20
    Flurry: -20
    Value: 1000
    image: boulder
    animation_type: projectile
Ball and Chain
    type: Equippable, Weapon
    Space: Main Hand, Off Hand
    Requires: <Two-Handed>
    weight: 3
    Stamina Cost: 3
    Endurance: -5
    Damage: 20
    Base: <Strength>
    Skill: <Weapon:Whip>
    Range: 2
    Piercing: 10
    Knockback: 20
    Speed: -10
    Endurance: -2
    Flying: -10
    Swimming: -10
    Flurry: -10
    Value: 800
    image: boulder
    animation_type: projectile
    animation_elevate: 0.5
    animation_trace: chain
Leather Whip
    type: Equippable, Weapon
    Space: Main Hand
    weight: 3
    Damage: 6
    Base: <Strength>
    Skill: <Weapon:Whip>
    Range: 2
    Piercing: 4
    Knockback: 2
    Speed: 4
    Value: 200
    image: chain
Chain Whip
    type: Equippable, Weapon
    Space: Main Hand
    Requires: <Flurry>
    weight: 3
    Stamina Cost: 1
    Damage: 19
    Base: <Strength>
    Skill: <Weapon:Whip>
    Range: 2
    Piercing: 10
    Defense: 4
    Evasion: 5
    Knockback: 5
    Speed: 4
    Value: 700
    image: chain
Cloth Robe
    type: Equippable, Armor
    Space: Armor
    weight: 1
    Defense: 2
    Speed: 2
    Value: 150
Leather Armor
    type: Equippable, Armor
    Space: Armor
    weight: 2
    Defense: 8
    Endurance: -1
    Value: 200
Chainmail
    type: Equippable, Armor
    Space: Armor
    weight: 3
    Defense: 16
    Endurance: -2
    Value: 800
Platemail
    type: Equippable, Armor
    Space: Armor
    weight: 3
    Defense: 28
    Endurance: -3
    Value: 1400
Siege Armor
    type: Equippable, Armor
    Space: Armor
    weight: 3
    Defense: 60
    Endurance: -6
    Speed: -8
    Evasion: -8
    Value: 3500
Leather Helmet
    type: Equippable, Armor
    Space: Head
    weight: 2
    Defense: 3
    Value: 150
Small Shield
    type: Equippable, Shield
    Space: Off Hand
    weight: 2
    Defense: 4
    Evasion: 30
    Value: 150
Round Shield
    type: Equippable, Shield
    Space: Off Hand
    weight: 2
    Defense: 6
    Evasion: 50
    Value: 600
Tower Shield
    type: Equippable, Shield
    Space: Off Hand
    weight: 3
    Defense: 10
    Endurance: -3
    Evasion: 65
    Value: 1200
Mirror Shield
    type: Equippable, Shield
    Space: Off Hand
    weight: 3
    Defense: 20
    Endurance: -2
    Evasion: 90
    Spell Resist: 20
    _Resist Fire: 1
    _Resist Water: 1
    _Resist Thunder: 1
    Value: 4000
Siege Shield
    type: Equippable, Shield
    Space: Main Hand, Off Hand
    Requires: <Two-Handed>
    weight: 3
    Defense: 40
    Endurance: -4
    Evasion: 150
    Damage: 1
    Attack Cost: 8
    Speed: -10
    Value: 2800
Running Shoes
    type: Equippable, Armor
    Space: Feet
    Requires: <Running>
    weight: 1
    Speed: 6
    Endurance: 8
    Running: 20
    Value: 750
Leather Boots
    type: Equippable, Armor
    Space: Feet
    weight: 1
    Defense: 2
    Value: 100
Pegasus Boots
    type: Equippable, Armor
    Space: Feet
    weight: 1
    Speed: 2
    Endurance: 4
    Running: 10
    Flying: 10
    Value: 1200
Leather Gloves
    type: Equippable, Armor
    Space: Hands
    weight: 1
    Defense: 2
    Value: 100
Chain Gloves
    type: Equippable, Armor
    Space: Hands
    weight: 3
    Defense: 4
    Endurance: -1
    Value: 400
Wrestling Gloves
    type: Equippable, Armor
    Space: Hands
    Requires: <Grappling>
    weight: 2
    Defense: 2
    Endurance: -1
    Speed: 2
    Evasion: 2
    Flurry: 5
    Grappling: 10
Fire Ring
    type: Equippable
    Space: Ring
    Requires: <Magic>
    Sorcery: 10
    _Resist Fire: 1
    Value: 750
Water Ring
    type: Equippable
    Space: Ring
    Requires: <Magic>
    Sorcery: 10
    _Resist Water: 1
    Value: 750
Lightning Ring
    type: Equippable
    Space: Ring
    Requires: <Magic>
    Sorcery: 10
    _Resist Thunder: 1
    Value: 750
Magic Ring
    type: Equippable
    Space: Ring
    Requires: <Magic>
    Magic: 2
    Sorcery: 10
    Value: 1000
Shield Ring
    type: Equippable
    Space: Ring
    Requires: <Magic> and <Spell Resist>
    Defense: 4
    Evasion: 4
    Spell Resist: 4
    Value: 1000
Thief Gloves
    type: Equippable
    Space: Hands
    Requires: <Stealing>
    Defense: 2
    Stealing: 10
    Value: 800
Aqualung
    type: Equippable
    Space: Head
    weight: 1
    Requires: <Swimming>
    Defense: 5
    Speed: 5
    Endurance: 10
    Swimming: 30
    Value: 500
Hero Medal
    type: Equippable
    Space: Medal
    Requires: <Goodness> >= -4
    Goodness: 4
    Value: 1500
Villain Medal
    type: Equippable
    Space: Medal
    Requires: <Goodness> <= 4
    Goodness: -4
    Value: 2000
Magister's Amulet
    type: Equippable
    Space: Medal
    Requires: <Magic> >= 10
    Magic: 10
    Sorcery: 10
    Spell Resist: 4
    Value: 4500
Necromancer's Amulet
    type: Equippable
    Space: Medal
    Requires: <Magic> >= 10 and <Vampirism> and <Goodness> < 0
    Magic: 20
    Vampirism: 20
    Sorcery: 20
Beta Tester's Amulet
    type: Equippable
    Space: Tester's Medal
    Vitality: 10
    Stamina: 10
    Intelligence: 2
    Cunning: 2
    Creativity: 2
    Precision: 2
    Evasion: 2
    Flying: 2
    Vampirism: 2
Supporter's Armguard
    type: Equippable
    Space: Arms
    Defense: 5
    Strength: 5
    Speed: 5
    Toughness: 5
    Endurance: 5
    Damage: 5
    Precision: 15
    Evasion: 15
    Loyalty: 20
    Bloodlust: 2
    Knockback: 2
