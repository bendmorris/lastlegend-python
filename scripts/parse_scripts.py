from mmorpg.lib.models import *
from mmorpg.lib.map import *
import os

skills_file = open("scripts/skills", "r")
skills = []
default_skills = []
line = True
while line:
    line = skills_file.readline().strip()    
    if line:
        if line[0] == '*':
            default = True
            line = line[1:]
        else: default=False
    
        if ': ' in line:
            name, formula = [s.strip() for s in line.split(': ')]
        else:
            name = line
            formula = ""
        
        description = skills_file.readline().strip()
        skills.append(Skill(name, formula, description))
        if default: default_skills.append(name)
skills_file.close()
all_skills = [skill.name for skill in skills]
skills_dict = {}
for skill in skills:
    skills_dict[skill.name] = skill
    
    
races_file = open("scripts/races", "r")
last_race = None
line = races_file.readline()
races = []
playable_races = []
while line:
    if line:
        if line[0] == ' ':
            line = line.strip()
            if line[0] == '-':
                last_race[4] = [s.strip() for s in line[1:].split(',')]
            else:
                last_race[5] = [s.strip() for s in line.split(',')]
        else:
            if last_race: races.append(last_race)
            line = line.strip()
            name, talents = line.split(':')[0].strip(), ':'.join(line.split(':')[1:]).strip()
            if name[0] == '*':
                tier = len([c for c in name if c == '*'])
                name = name.strip('*')
                if tier <= TIER: playable_races.append(name)
            talents = talents.split(', ')
            talents_dict = {}
            potential_skills = set()
            for talent in talents:
                if talent.startswith('_') and not talent.startswith('__'):
                    try:
                        talent_name, talent_value = ' '.join(talent.split(' ')[:-1]), int(talent.split(' ')[-1])
                    except:
                        talent_name, talent_value = talent, True
                else:
                    if talent.split(' ')[-1] == '+':
                        potential_skills.add(' '.join(talent.split(' ')[:-1]))
                    else:
                        talent_name = ' '.join(talent.split(' ')[:-1])
                        talent_value = int(talent.split(' ')[-1])
                talents_dict[talent_name] = talent_value
                for default_skill in default_skills:
                    if not default_skill in talents_dict:
                        talents_dict[default_skill] = 1
            description = races_file.readline().strip()
            last_race = [name, description, talents_dict, potential_skills, [], []]
    line = races_file.readline()
races_file.close()
races.append(last_race)
races = [Race(*race) for race in races]
all_races = [race.name for race in races]
races_dict = {}
for race in races:
    races_dict[race.name] = race
    
    
titles_file = open("scripts/titles", "r")
all_titles = []
playable_titles = []
titles_dict = {}
line = titles_file.readline()
while line:
    line2 = titles_file.readline()
    name, stuff = [s.strip() for s in line.strip().split(': ')]
    description = line2.strip()

    if name[0] == '*':
        tier = len([c for c in name if c == '*'])
        name = name.strip('*')
        playable = tier <= TIER
    else: playable = True
    
    if ';' in stuff:
        sections = [[s.strip() for s in a.split(',')] for a in stuff.split(';')]
        if len(sections) == 3:
            skills, items, abilities = sections
        elif len(sections) == 2:
            skills, items = sections
            abilities = []
    else:
        skills = [s.strip() for s in stuff.split(',')]
        items = []
        abilities = []
    start_skills = {' '.join(skill.split(' ')[:-1]):int(skill.split(' ')[-1]) for skill in skills
                    if skill.split(' ')[-1] != '+'}
    potential_skills = set([' '.join(skill.split(' ')[:-1]) for skill in skills
                            if skill.split(' ')[-1] == '+'])
    items_list = []
    for item in items:
        if item.strip():
            if '*' in item:
                item, qty = [s.strip() for s in item.split('*')]
                qty = int(qty)
            else: qty = 1
            items_list += [item] * qty
    title = CharClass(name, description, start_skills, potential_skills, items_list, abilities)
    titles_dict[name] = title
    all_titles.append(name)
    if playable: playable_titles.append(name)
    
    line = titles_file.readline()
titles_file.close()
    
    
items_file = open("scripts/items", "r")
last_item = None
line = items_file.readline()
items = []
item_skills = set()
while line:
    if line:
        if line[0] == ' ':
            line = line.strip()
            attr_name, attr_value = line.split(': ')
            last_item.attributes.append((attr_name, attr_value))
            if attr_name == 'Skill' and not attr_value[1:-1] in all_skills: item_skills.add(attr_value[1:-1])
        else:
            if last_item: items.append(last_item)
            line = line.strip()
            if ': ' in line:
                name, description = line.split(': ')
            else:
                name = line
                description = ""
            last_item = Item(name, description)
    line = items_file.readline()
items_file.close()
items.append(last_item)
all_items = [item.name for item in items]
items_dict = {}
for item in items:
    items_dict[item.name] = item


abilities_file = open("scripts/abilities", "r")
last_ability = None
line = abilities_file.readline()
abilities = []
while line:
    if line:
        if line[0] == ' ':
            line = line.strip()
            attr_name, attr_value = line.split(': ')
            last_ability.attributes.append((attr_name, attr_value))
            if attr_name == 'Skill' and not attr_value[1:-1] in all_skills: item_skills.add(attr_value[1:-1])
        else:
            if last_ability: abilities.append(last_ability)
            name = line.strip()
            last_ability = Ability(name)
    line = abilities_file.readline()
abilities_file.close()
abilities.append(last_ability)
all_abilities = [ability.name for ability in abilities]
abilities_dict = {}
for ability in abilities:
    abilities_dict[ability.name] = ability


all_skills += sorted(list(item_skills))
    
defs = {key: eval(key) for key in
        [
         'all_skills',
         'skills_dict',
         'all_races',
         'playable_races',
         'races_dict',
         'all_items',
         'items_dict',
         'all_abilities',
         'abilities_dict',
         'all_titles',
         'playable_titles',
         'titles_dict',
         ]}


def get_maps(world=None):
    if not world: world = World()
    maps_dict = {}
    for map_file in filter(lambda f: f.endswith('.map'), os.listdir('maps')):
        this_map = Map('maps/' + map_file, world, defs)
        print this_map.name
        maps_dict[this_map.name] = this_map
    return maps_dict

def get_world():
    world = World()
    world.maps_dict = get_maps(world)
    for this_map in world.maps_dict.values():
        this_map.spawn_NPCs(world)
    return world
