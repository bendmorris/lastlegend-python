from parse_scripts import *

"""for race in all_races:
    race = races_dict[race]
    me = Character("Ben", race, skills_dict, titles)
    title_scores = me.titles(titles)
    print '***', race.name, sorted(title_scores, key = lambda t:t[1], reverse=True)
    print me.trained_skills(), me.untrained_skills()
    n = 0
    for skill in all_skills:
        if not skill in me.race.disabled_skills:
            if not n % 5: print
            msg = '%s: %s' % (skill, me[skill])
            print msg, ' ' * (22-len(msg)),
            n += 1
    print '\n'"""
race = races_dict["Lizardman"]
me = Character("Ben", "M", race, skills_dict, titles)
print me.upgrade("Cooking")
print me.upgrade("Cooking")
print me.upgrade("Cooking")
print me.upgrade("Cooking")
print me.upgrade("Creativity")
print me.upgrade("Cunning")
print me.upgrade("Cunning")
print me.upgrade("Bargaining")
print me.upgrade("Bargaining")
print me.upgrade("Bargaining")
print me.upgrade("Bargaining")
print me.summary()
print me.skill_summary(all_skills)
print me.titles()
print me.xp, me.spent_xp
