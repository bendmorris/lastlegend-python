from setuptools import setup


packages = ['mmorpg.lib', 'mmorpg.scripts', 'mmorpg.app', 
            'mmorpg.assets', 'mmorpg.tiles', 'mmorpg.network',
            'mmorpg']

setup(name='mmorpg',
      packages=packages,
      package_dir = {'mmorpg':''},
      entry_points={
        'console_scripts': [
            'mmorpg = mmorpg.main:main',
        ],
      },
      )