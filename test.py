from mmorpg.lib.models import Character
from mmorpg.scripts.parse_scripts import defs

c = Character('Ben',0,'Human','Duelist',defs)

s = c.sprite()

import zlib as z; import cPickle as p
print 'Character',len(z.compress(p.dumps(c,-1),9))
print 'Sprite',len(z.compress(p.dumps(s,-1),9))