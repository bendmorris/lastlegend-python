from mmorpg import *
from mmorpg.lib.funcs import *


class TileDef:
    def __init__(self, x, y, scroll=None, scroll_spd=1,
        height=0):
        self.x = x
        self.y = y
        self.scroll = scroll
        self.scroll_spd = scroll_spd
        self.height = height
        
t = TileDef

# define where to get the tile images from in the tiles source image
TILESET = {
    '0': t(0,0),
    '.': t(1,0),                    # grass
    '=': t(4,1),                    # road
    ':': t(4,2),                    # stairs
    '<': t(4,3),                    # vertical bridge
    '>': t(5,3),                    # horizontal bridge
    ';': t(5,2),                    # fancy tile
    '-': t(4,0),                    # wood
    'f': t(1,1),                    # flowers
    'd': t(5,0),                    # sand
    'M': t(5,1),                    # land edge
    'b': t(6,0),  'B': t(6,1),      # brick wall
    'e': t(7,0),  'E': t(7,1),      # nice brick wall
    'h': t(6,2),  'H': t(6,3),      # brick wall gate
    'o': t(8,2),                    # snow
    'p': t(8,0),  'P': t(8,1),      # snow tree
    #'c': (9,0),  'C': (9,3),       # ice cave
    'I': t(9,1),  'i': t(9,2),
    'c': t(10,0), 'C': t(10,1),     # cave wall
    'Q': t(8,1),                    # cave opening
    'q': t(10,2),                   # cave floor
    'y': t(3,0),  'Y': t(3,1),      # log building
    'w': t(2,1),                    # window
    '[': t(2,0), ']': t(2,0),       # door, walkable and not walkable
    '+': t(6,2),                    # sign
}
TILES_MOVABLE = ".g=fsod-Hq:<>;["
TILES_FLYABLE = "M"
SWIMMING_ENTRANCES = ":"

CHARSET = {
    'Human': 0,
    'Elf': 1,
    'Dwarf': 2,
    'Centaur': 3,
    'Merfolk': 4,
    'Lizardman': 5,
    'Goblin': 6,
    'Troll': 7,
    'Ogre': 8,
    'Imp': 9,
    'Beast': 10,
    'Vampire': 11,
    'Undead': 12,
    'Dragon': 13,
    'Golem': 14,
    'Bird': 15,
    'Spider': 16,
    'Wolf': 17,
    'Bear': 18,
    'Fish': 19,
    'Squid': 20,
}

OBJSET = {
    'shadow': t(0,0),
    'smshadow': t(0,1),
    'darkness': t(0,2),
    'close': t(0,3),
    'restore': t(0,4),
    'msgbtn': t(2,4),
    'equipbtn': t(2,5),
    'skillsbtn': t(2,6),
    'uparrow': t(0,5),
    'downarrow': t(0,6),
    'selection': t(0,7),
    'attacking': t(0,8),
    'grass': t(1,5),
    'water': t(1,0, scroll=1, scroll_spd=2),
    'slow_water': t(1,0, scroll=1),
    'deep_water': t(1,0, scroll=1, scroll_spd=2),
    'waterfall': t(2,0, scroll=1),
    'tree': t(1,3,height=1),
    'stump': t(1,4),
    'tree2': t(2,3,height=1),
    'gate': t(3,0),
    'window': t(3,2),
    'sword': t(4,0),
    'longsword': t(5,0),
    'dagger': t(6,0),
    'spear': t(7,0),
    'arrow': t(8,0),
    'axe': t(9,0),
    'mattock': t(9,1),
    'chain': t(8,1),
    'club': t(10,0),
    'bow': t(4,1),
    'boulder': t(5,1),
    'staff': t(6,1),
    'shield': t(11,0),
    'armor': t(12,0),
    'helmet': t(13,0),
    'chest': t(4,2),
    'chest-open': t(5,2),
    'barrel': t(6,2),
    'pot': t(7,2),
    'fireball': t(4,3),
    'smokeball': t(5,3),
    'ice': t(4,4),
    'acid': t(5,4),
    'bolt': t(6,4),
    'lightbolt': t(7,4),
    'darkbolt': t(8,4),
    'waterball': t(4,5),
    'waterdrop': t(5,5),
}
OBJS_MOVABLE = ['gate', 'grass']
OBJS_FLYABLE = ['chest', 'chest-open', 'barrel', 'pot',]

CHAR_DIR = {
            'u': 0,
            'd': 1,
            'l': 2,
            'r': 3
            }

dead_chars = {}
def draw_char(view, chars, char, pos, direction, step):
    import pygame

    char_id, chars = chars
    if direction == 'dead':
        key = str(char_id) + str(char)
        if not key in dead_chars:
            subsurface = chars.subsurface((char[0] * BUFFERED_TILE * 2 + SCALE, 
                                           char[1] * BUFFERED_TILE * 4 + SCALE, 
                                           TILE_SIZE, TILE_SIZE)).copy()
            subsurface = pygame.transform.rotate(subsurface, -90)
            #subsurface = pygame.transform.scale(subsurface, (TILE_SIZE, TILE_SIZE*3/4))
            dead_chars[key] = subsurface
        return view.blit(dead_chars[key], pos, (0,0,int(TILE_SIZE),int(TILE_SIZE)))
    else:
        src = (int(char[0] * BUFFERED_TILE * 2 + (BUFFERED_TILE if step else 0) + SCALE), 
               int(char[1] * BUFFERED_TILE * 4 + CHAR_DIR[direction] * BUFFERED_TILE + SCALE),
               int(TILE_SIZE), int(TILE_SIZE))
        return view.blit(chars, pos, src)
        
rotation = {'r':-45, 'u':45, 'l':135, 'd':-135}
saved_subsurfaces = {}
def draw_wpn(view, objs, image, pos, direction):
    import pygame

    obj = OBJSET[image] if image in OBJSET else OBJSET['sword']
    x, y = (obj.x * BUFFERED_TILE + 1, obj.y * BUFFERED_TILE + 1)
    if image+direction in saved_subsurfaces:
        subsurface = saved_subsurfaces[image+direction]
    else:
        subsurface = objs.subsurface((x, y, TILE_SIZE, TILE_SIZE)).copy()
        subsurface = pygame.transform.rotate(subsurface, rotation[direction])
        rect = subsurface.get_rect()
        _, _, w, h = rect
        subsurface = pygame.transform.scale2x(pygame.transform.scale2x(subsurface))
        subsurface = pygame.transform.smoothscale(subsurface, (w, h))
        saved_subsurfaces[image+direction] = subsurface
    rect = subsurface.get_rect()
    _, _, w, h = rect
    if w > TILE_SIZE or h > TILE_SIZE:
        x, y = pos
        pos = (x - (w-TILE_SIZE)/2, y - (h-TILE_SIZE)/2)
    return view.blit(subsurface, pos, 
                    (1,1, w, h))
                    
def draw_obj(view, objs, image, pos):
    obj = OBJSET[image] if image in OBJSET else OBJSET['sword']
    return view.blit(objs, pos,
                     (obj.x * BUFFERED_TILE + SCALE, obj.y * BUFFERED_TILE + SCALE, TILE_SIZE, TILE_SIZE))
def draw_icon(view, objs, image, pos):
    obj = OBJSET[image] if image in OBJSET else OBJSET['sword']
    scale = BLOCK_SIZE/ORIG_TILE_SIZE
    buffer = BLOCK_SIZE + ORIG_TILE_BUFFER * scale
    return view.blit(objs, pos,
                     (obj.x * buffer + scale, obj.y * buffer + scale, BLOCK_SIZE, BLOCK_SIZE))
                     

def invert(src, c=0):
    import pygame

    src = src.copy()
    arr = pygame.surfarray.pixels3d(src)
    #vmax = np.vectorize(lambda n: min(n, 255))
    r,g,b = arr[:,:,0], arr[:,:,1], arr[:,:,2]
    abs_c = (r + g + b) + 128
    arr[:,:,c] = abs_c
    if not c == 0: arr[:,:,0] = 24
    if not c == 1: arr[:,:,1] = 24
    if not c == 2: arr[:,:,2] = 24
    return src
    
created_tilesets = {}
def get_tilesets(settings):
    key = settings.SMOOTH, settings.DOUBLES
    if not key in created_tilesets:
        created_tilesets[key] = Tilesets(settings)
    return created_tilesets[key]
                         
class Tilesets:
    def __init__(self, settings):
        import pygame

        tiles = 'tiles/tiles.png'
        chars = 'tiles/chars.png'
        objs = 'tiles/objs.png'
        x2 = pygame.transform.scale2x
        def smoothscale(surface, scale): 
            t = pygame.transform.smoothscale if settings.SMOOTH else pygame.transform.scale
            return pygame.transform.scale(surface, 
                    (int(surface.get_width() * scale), int(surface.get_height() * scale)))
        def smooth(surface):
            return smoothscale(x2(surface), 0.5)
                    
        self.tiles = pygame.image.load(tiles).convert()
        self.chars = pygame.image.load(chars).convert_alpha()
        self.objs = pygame.image.load(objs).convert_alpha()

        self.inv_tiles = invert(self.tiles)
        self.inv_chars = invert(self.chars)
        self.inv_objs = invert(self.objs)
        self.psn_chars = invert(self.chars, c=1)

        tilesets = ['tiles', 'chars', 'objs', 
                    'inv_tiles', 'inv_chars', 'inv_objs',
                    'psn_chars',]
        
        need_to_scale = float(SCALE)
        smoothed = False
        doubles = settings.DOUBLES
        while need_to_scale != 1:
            if need_to_scale > 1 and doubles > 0:
                for t in tilesets:
                    setattr(self, t, x2(getattr(self, t)))
                need_to_scale /= 2
                doubles -= 1
            else:
                for t in tilesets:
                    setattr(self, t, smoothscale(getattr(self, t), need_to_scale))
                need_to_scale = 1
        
        while doubles > 0: 
            for t in tilesets:
                setattr(self, t, smooth(getattr(self, t)))
            doubles -= 1
            
        
        for s in ['chars', 'inv_chars', 'psn_chars']:
            setattr(self, s, (s, getattr(self, s)))
            
        obj = OBJSET['darkness']
        self.darkness = self.objs.subsurface((obj.x * BUFFERED_TILE + SCALE, obj.y * BUFFERED_TILE + SCALE, TILE_SIZE, TILE_SIZE))

        self.icons = pygame.image.load(objs).convert_alpha()
        self.icons = x2(x2(self.icons))
